package com.desktopapp.jsonobject;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.Expose;

public class UISettingsJSON {
    
    	@Expose
    	private Map<String,String> uiSettingMap = new HashMap<String, String>();

	public Map<String,String> getUiSettingMap() {
	    return uiSettingMap;
	}

	public void setUiSettingMap(Map<String, String> uiSettingMap) {
	    this.uiSettingMap = uiSettingMap;
	}

	
	
}
