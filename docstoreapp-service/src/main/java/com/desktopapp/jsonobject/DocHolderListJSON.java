package com.desktopapp.jsonobject;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class DocHolderListJSON {
	
	
	@Expose
	private List<DocHolderJSON> docHolderJSONList = new ArrayList<DocHolderJSON>();

	public List<DocHolderJSON> getDocHolderJSONList() {
		return docHolderJSONList;
	}

	public void setDocHolderJSONList(List<DocHolderJSON> docHolderJSONList) {
		this.docHolderJSONList = docHolderJSONList;
	}

	
}
