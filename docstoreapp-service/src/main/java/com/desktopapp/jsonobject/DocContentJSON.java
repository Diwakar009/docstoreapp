package com.desktopapp.jsonobject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.desktopapp.framework.IEntity;
import com.desktopapp.model.DocContent;
import com.desktopapp.model.DocContentBlob;
import com.google.gson.annotations.Expose;

public class DocContentJSON extends BaseObjectJSON{
	
	private static final long serialVersionUID = -8529903988099710658L;
	
	public DocContentJSON() {
		this(new HashMap<String,Object>(),null);
	}


	public DocContentJSON(Map<String,Object> map,IEntity entity) {
		super(map,entity);
		
		if(entity != null){
			copyEntityToJSON();
		}else{
			this.entity  = new DocContent();
		}
	}
	
	public DocContentJSON(Map<String,Object> argumentMap,IEntity entity,String documentStoreDirPath) {
		super(argumentMap,entity);
		
		this.documentStoreDirPath = documentStoreDirPath;
		this.docPersist = "Y";
		
		if(entity != null){
			copyEntityToJSON();
		}else{
			this.entity  = new DocContent();
		}
		
		
	}
	
	public DocContentJSON(Map<String,Object> argumentMap,IEntity entity,String documentStoreDirPath,String docPersist) {
		super(argumentMap,entity);
		
		this.documentStoreDirPath = documentStoreDirPath;
		this.docPersist = docPersist;
		
		if(entity != null){
			copyEntityToJSON();
		}else{
			this.entity  = new DocContent();
		}
		
		
	}
	

	@Expose
	private Long id;

	@Expose
	private String docContentKeywords;

	@Expose
	private String docContentName;

	@Expose
	private String docFileExtension;
	
	@Expose
	private String docFileName;

	@Expose
	private String docDirPath;
	
	@Expose
	private Date docLastModifiedTime;
	
	@Expose
	private List<DocContentBlobJSON> docContentBlobJsons;
	
	private String documentStoreDirPath;
	
	private String docPersist;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocContentKeywords() {
		return docContentKeywords;
	}

	public void setDocContentKeywords(String docContentKeywords) {
	    
	    
		this.docContentKeywords = docContentKeywords;
	}

	public String getDocContentName() {
		return docContentName;
	}

	public void setDocContentName(String docContentName) {
		this.docContentName = docContentName;
	}

	public String getDocFileExtension() {
		return docFileExtension;
	}

	public void setDocFileExtension(String docFileExtension) {
		this.docFileExtension = docFileExtension;
	}

	public String getDocFileName() {
		return docFileName;
	}

	public void setDocFileName(String docFileName) {
		this.docFileName = docFileName;
	}

	public String getDocDirPath() {
		return docDirPath;
	}

	public void setDocDirPath(String docDirPath) {
		this.docDirPath = docDirPath;
	}

	public List<DocContentBlobJSON> getDocContentBlobs() {
		return docContentBlobJsons;
	}

	public void setDocContentBlobs(List<DocContentBlobJSON> docContentBlobs) {
		this.docContentBlobJsons = docContentBlobs;
	}
	
	
	public Date getDocLastModifiedTime() {
	    return docLastModifiedTime;
	}


	public void setDocLastModifiedTime(Date docLastModifiedTime) {
	    this.docLastModifiedTime = docLastModifiedTime;
	}


	public String getDocumentStoreDirPath() {
		return documentStoreDirPath;
	}


	public void setDocumentStoreDirPath(String documentStoreDirPath) {
		this.documentStoreDirPath = documentStoreDirPath;
	}


	


	public String getDocPersist() {
	    return docPersist;
	}


	public void setDocPersist(String docPersist) {
	    this.docPersist = docPersist;
	}


	@Override
	public void copyEntityToJSON() {
		
		super.copyEntityToJSON();
		
		DocContent docContent = (DocContent) entity;
		
		setId(docContent.getId());
		setDocContentName(docContent.getDocContentName());
		setDocDirPath(docContent.getDocDirPath());
		setDocFileExtension(docContent.getDocFileExtension());
		setDocFileName(docContent.getDocFileName());
		setDocContentKeywords(docContent.getDocContentKeywords());
		
		if(docContent.getDocLastModifiedTime() != null){
		    setDocLastModifiedTime(docContent.getDocLastModifiedTime());
		}
		
		if("N".equals(getDocPersist())){
		    
		}else{
		    docContentBlobJsons = new ArrayList<DocContentBlobJSON>();
                		
                	for(DocContentBlob contentBlob : docContent.getDocContentBlobs() ){
                		DocContentBlobJSON contentBlobJSON =  new DocContentBlobJSON(getArgumentMap(),contentBlob,getDocumentStoreDirPath());
                		docContentBlobJsons.add(contentBlobJSON);
                	}
		}
		
	}

	@Override
	public void copyJSONToEntity() {
		super.copyJSONToEntity();
		
		DocContent docContent = (DocContent)entity;
		//docContent.setId(getId());
		docContent.setDocContentKeywords(getDocContentKeywords());
		docContent.setDocContentName(getDocContentName());
		docContent.setDocDirPath(getDocDirPath());
		docContent.setDocFileName(getDocFileName());
		docContent.setDocFileExtension(getDocFileExtension());
		
		if(getDocLastModifiedTime() != null){
		    docContent.setDocLastModifiedTime(getDocLastModifiedTime());
		}
		
		if("N".equals(getDocPersist())){
		    
		}else{
		 
    		    if(docContentBlobJsons != null){
		    for(DocContentBlobJSON contentBlobJson : docContentBlobJsons){
    		    
    		    if(contentBlobJson != null){
    			contentBlobJson.setDocumentStoreDirPath(documentStoreDirPath);
    			contentBlobJson.copyJSONToEntity();
    			DocContentBlob contentBlob = (DocContentBlob)contentBlobJson.getEntity();
    			contentBlob.setDocContent(docContent);
    			docContent.addDocContentBlob(contentBlob);
    		    }
		    }
    		}
		
		}
	}
	
	
	
}
