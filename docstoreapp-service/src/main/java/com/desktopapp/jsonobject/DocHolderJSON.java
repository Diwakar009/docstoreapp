package com.desktopapp.jsonobject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.desktopapp.constants.ArgumentConstants;
import com.desktopapp.framework.IEntity;
import com.desktopapp.model.DocContent;
import com.desktopapp.model.DocHolder;
import com.google.gson.annotations.Expose;


public class DocHolderJSON extends BaseObjectJSON{
	
	private static final long serialVersionUID = -3665363292829488982L;

	public DocHolderJSON() {
		this(new HashMap<String,Object>(),null);
		getArgumentMap().put(ArgumentConstants.ARG_MASKDOCMENTS, "Y");
	}

	public DocHolderJSON(Map<String,Object> argumentMap,IEntity entity) {
		super(argumentMap,entity);
		
		if(entity != null){
			copyEntityToJSON();
		}else{
			this.entity = new DocHolder();
		}
	}
	
	public DocHolderJSON(Map<String,Object> map,IEntity entity,String documentStoreDirPath) {
		super(map,entity);
		
		this.documentStoreDirPath = documentStoreDirPath;
		
		if(entity != null){
			copyEntityToJSON();
		}else{
			this.entity = new DocHolder();
		}
		
		
		
	}

	@Expose
	private Long id;
	
	@Expose
	private String docCoowner;

	@Expose
	private String docKeywords;

	@Expose
	private String docName;

	@Expose
	private String docOwner;

	@Expose
	private String docType;

	@Expose
	private String docTypeOthers;
	
	@Expose
	private String docAccess;
	
	@Expose
	private String docPersist;
	
	
	@Expose
	private List<DocContentJSON> docContentJsons;
	
	private String documentStoreDirPath;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocCoowner() {
		return docCoowner;
	}

	public void setDocCoowner(String docCoowner) {
		this.docCoowner = docCoowner;
	}

	public String getDocKeywords() {
		return docKeywords;
	}

	public void setDocKeywords(String docKeywords) {
		this.docKeywords = docKeywords;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDocOwner() {
		return docOwner;
	}

	public void setDocOwner(String docOwner) {
		this.docOwner = docOwner;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getDocTypeOthers() {
		return docTypeOthers;
	}

	public void setDocTypeOthers(String docTypeOthers) {
		this.docTypeOthers = docTypeOthers;
	}

	public List<DocContentJSON> getDocContents() {
		return docContentJsons;
	}
	
	
	public String getDocPersist() {
	    return docPersist;
	}

	public void setDocPersist(String docPersist) {
	    this.docPersist = docPersist;
	}

	public String getDocAccess() {
	    return docAccess;
	}

	public void setDocAccess(String docAccess) {
	    this.docAccess = docAccess;
	}

	public void setDocContents(List<DocContentJSON> docContents) {
		this.docContentJsons = docContents;
	}
	
	public String getDocumentStoreDirPath() {
		return documentStoreDirPath;
	}

	public void setDocumentStoreDirPath(String documentStoreDirPath) {
		this.documentStoreDirPath = documentStoreDirPath;
	}

	@Override
	public void copyEntityToJSON() {
		
		super.copyEntityToJSON();
		
		DocHolder docHolder = (DocHolder)entity;
		setId(docHolder.getId());
		setDocName(docHolder.getDocName());
		setDocCoowner(docHolder.getDocCoowner());
		setDocKeywords(docHolder.getDocKeywords());
		setDocOwner(docHolder.getDocOwner());
		setDocType(docHolder.getDocType());
		setDocTypeOthers(docHolder.getDocTypeOthers());
		setDocAccess(docHolder.getDocAccess());
		setDocPersist(docHolder.getDocPersist() == null ? "Y" : docHolder.getDocPersist());
		
		docContentJsons = new ArrayList<DocContentJSON>();
		
		for(DocContent content : docHolder.getDocContents()){
			DocContentJSON contentJSON = new DocContentJSON(getArgumentMap(),content,getDocumentStoreDirPath(),getDocPersist());
			docContentJsons.add(contentJSON);
		}
		
	}

	@Override
	public void copyJSONToEntity() {
		
		super.copyJSONToEntity();
		
		DocHolder docHolder = (DocHolder)entity;
		
		//docHolder.setId(getId());
		docHolder.setDocName(getDocName());
		docHolder.setDocCoowner(getDocCoowner());
		docHolder.setDocKeywords(getDocKeywords());
		docHolder.setDocOwner(getDocOwner());
		docHolder.setDocType(getDocType());
		docHolder.setDocTypeOthers(getDocTypeOthers());
		docHolder.setDocPersist(getDocPersist() == null ? "Y" :  getDocPersist() );
		docHolder.setDocAccess(getDocAccess() == null ? "*" :  getDocAccess() );		
		for(DocContentJSON contentJson : docContentJsons){
			contentJson.setDocumentStoreDirPath(getDocumentStoreDirPath());
			contentJson.copyJSONToEntity();
			DocContent content = (DocContent)contentJson.getEntity();
			content.setDocHolder(docHolder);
			docHolder.addDocContent(content);
		}
		
	}


}
