package com.desktopapp.jsonobject;

import java.util.HashMap;
import java.util.Map;

import com.desktopapp.framework.IEntity;
import com.desktopapp.model.StaticCodeDecode;
import com.google.gson.annotations.Expose;




public class StaticCodeDecodeJSON extends BaseObjectJSON{
	
	private static final long serialVersionUID = -5314334704622314815L;
	
	public StaticCodeDecodeJSON() {
		this(new HashMap<String,Object>(),null);
	}

	public StaticCodeDecodeJSON(Map<String,Object> argumentMap,IEntity entity) {
		super(argumentMap,entity);
		
		if(entity != null){
			copyEntityToJSON();
		}else{
			this.entity = new StaticCodeDecode();
		}
		
	}

	@Expose
	private String codeName;
	
	@Expose
	private String language;

	@Expose
	private String codeValue;
	
	@Expose
	private String codeDesc;

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getCodeValue() {
		return codeValue;
	}

	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public String getCodeDesc() {
		return codeDesc;
	}

	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}



	@Override
	public void copyEntityToJSON() {

		super.copyEntityToJSON();

		StaticCodeDecode codeDecode = (StaticCodeDecode) entity;
		
		setCodeName(codeDecode.getCodeName());
		setCodeValue(codeDecode.getCodeValue());
		setLanguage(codeDecode.getLanguage());
		setCodeDesc(codeDecode.getCodeDesc());
		
	}

	@Override
	public void copyJSONToEntity() {
		
		super.copyJSONToEntity();
		
		StaticCodeDecode codeDecode = (StaticCodeDecode) entity;
		codeDecode.setCodeName(getCodeName());
		codeDecode.setCodeValue(getCodeValue());
		codeDecode.setLanguage(getLanguage());
		codeDecode.setCodeDesc(getCodeDesc());
	}
	
	
	
}
