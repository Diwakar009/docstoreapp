package com.desktopapp.jsonobject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.Expose;

public class FunctionAccessJSON {
    
    	@Expose
	private List<String> fullFunctionList = new ArrayList<String>();
    	
    	@Expose
    	private Map<String,List<String>> functionAccessMap = new HashMap<String, List<String>>();


	public List<String> getFullFunctionList() {
	    return fullFunctionList;
	}

	public void setFullFunctionList(List<String> fullFunctionList) {
	    this.fullFunctionList = fullFunctionList;
	}

	public Map<String, List<String>> getFunctionAccessMap() {
	    return functionAccessMap;
	}

	public void setFunctionAccessMap(Map<String, List<String>> functionAccessMap) {
	    this.functionAccessMap = functionAccessMap;
	}
}
