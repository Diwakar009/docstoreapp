package com.desktopapp.jsonobject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.desktopapp.component.constants.DocStoreConstants;
import com.desktopapp.constants.ArgumentConstants;
import com.desktopapp.framework.IEntity;
import com.desktopapp.model.DocContentBlob;
import com.desktopapp.util.CommonFileUtil;
import com.google.gson.annotations.Expose;

public class DocContentBlobJSON extends BaseObjectJSON {

	private static final long serialVersionUID = 1L;
	
	
	public DocContentBlobJSON() {
		this(new HashMap<String, Object>(),null);
		getArgumentMap().put(ArgumentConstants.ARG_MASKDOCMENTS, "Y");
	}

	public DocContentBlobJSON(Map<String,Object> argumentMap,IEntity entity) {

		super(argumentMap,entity);

		if (entity != null) {
			copyEntityToJSON();
		}else{
			this.entity = new DocContentBlob();
		}
	}
	
	public DocContentBlobJSON(Map<String,Object> map,IEntity entity,String documentStoreDirPath) {

		super(map,entity);
		
		this.documentStoreDirPath = documentStoreDirPath;
		
		if (entity != null) {
			copyEntityToJSON();
		}else{
			this.entity = new DocContentBlob();
		}
		
	}
	

	@Expose
	private Long id;

	private byte[] docContentBlob;

	@Expose
	private Long docContentId;
	
	private String documentStoreDirPath;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getDocContentBlob() {
		return docContentBlob;
	}

	public void setDocContentBlob(byte[] docContentBlob) {
		this.docContentBlob = docContentBlob;
	}

	public Long getDocContentId() {
		return docContentId;
	}

	public void setDocContentId(Long docContentId) {
		this.docContentId = docContentId;
	}
	
	public String getDocumentStoreDirPath() {
		return documentStoreDirPath;
	}

	public void setDocumentStoreDirPath(String documentStoreDirPath) {
		this.documentStoreDirPath = documentStoreDirPath;
	}

	@Override
	public void copyEntityToJSON() {
		super.copyEntityToJSON();
	
		DocContentBlob docContentBlob = (DocContentBlob)entity;
		setId(docContentBlob.getId());
		//setDocContentBlob(CommonFileUtil.getByteArrayFromBlob(docContentBlob.getDocContentBlob()));
		setDocContentBlob(docContentBlob.getDocContentBlob());
		setDocContentId(docContentBlob.getDocContent().getId());
	
		File file = null;
		
		boolean documentMaskApplicable = ("Y".equals((String)getArgumentMap().get(ArgumentConstants.ARG_MASKDOCMENTS))) ? true:false;
		
		
		if(documentMaskApplicable){
			file = new File(documentStoreDirPath + File.separator+ DocStoreConstants.MAIN_BACKUP_DOC_FOLDER   + File.separator + docContentBlob.getId() + " - " + docContentBlob.getDocContent().getId() + "");
		}else{
			file = new File(documentStoreDirPath + File.separator + docContentBlob.getDocContent().getDocHolder().getDocName()  + File.separator + docContentBlob.getDocContent().getDocFileName() + "");
		}
		
		
		try {
		    	//FileUtils.writeByteArrayToFile(file, CommonFileUtil.getByteArrayFromBlob(docContentBlob.getDocContentBlob()));
		    	FileUtils.writeByteArrayToFile(file, docContentBlob.getDocContentBlob());
			if(docContentBlob.getDocContent().getDocLastModifiedTime() != null){
		    	    file.setLastModified(docContentBlob.getDocContent().getDocLastModifiedTime().getTime());
		    	}
		} catch (IOException e) {
			// TODO Auto-generated catch blocks
			e.printStackTrace();
		}
		
	}

	@Override
	public void copyJSONToEntity() {
		super.copyJSONToEntity();
		DocContentBlob docContentBlob = (DocContentBlob)entity;
		
		//docContentBlob.setid(getId());
		
		boolean documentMaskApplicable = ("Y".equals((String)getArgumentMap().get(ArgumentConstants.ARG_MASKDOCMENTS))) ? true:false;
		
		File file = null;
		
		if(documentMaskApplicable){
			file = new File(documentStoreDirPath + File.separator+ DocStoreConstants.MAIN_BACKUP_DOC_FOLDER   + File.separator+ getId() + " - " + getDocContentId() + "");
		}else{
			file = new File(documentStoreDirPath + File.separator + docContentBlob.getDocContent().getDocHolder().getDocName()  + File.separator + docContentBlob.getDocContent().getDocFileName() + "");
		}
		
		try {
			if(file != null){
				setDocContentBlob(FileUtils.readFileToByteArray(file));
				if(docContentBlob.getDocContent() != null){
				    if(docContentBlob.getDocContent().getDocLastModifiedTime() != null){
			    	  	docContentBlob.getDocContent().setDocLastModifiedTime(docContentBlob.getDocContent().getDocLastModifiedTime());
				    }
			    	}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//docContentBlob.setDocContentBlob(CommonFileUtil.getBlobFromByteArray(getDocContentBlob()));
		docContentBlob.setDocContentBlob(getDocContentBlob());
	}

}
