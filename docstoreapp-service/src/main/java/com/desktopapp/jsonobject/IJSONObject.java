package com.desktopapp.jsonobject;


public interface IJSONObject {
	
	public void copyEntityToJSON();
	
	public void copyJSONToEntity();

}
