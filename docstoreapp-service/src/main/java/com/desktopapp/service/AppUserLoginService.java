package com.desktopapp.service;

import static com.desktopapp.service.QueryParameter.with;

import java.util.ArrayList;
import java.util.List;

import com.desktopapp.model.AppUserLogin;


/**
 * App Login User Service
 *
 * @author Ratnala Diwakar Choudhury
 */
public class AppUserLoginService extends AbstractService<AppUserLogin> {

    public AppUserLoginService() {
        super(AppUserLogin.class);
    }
    
    
    public AppUserLogin getAppUserLoginByUserName(String userName) {
	List<AppUserLogin> list = (ArrayList<AppUserLogin>)getListWithNamedQuery(AppUserLogin.FIND_BY_APPUSERLOGIN, with("userName",userName).parameters());
	return (list != null && list.size() > 0) ?  list.get(0) : null ;
    }

}
