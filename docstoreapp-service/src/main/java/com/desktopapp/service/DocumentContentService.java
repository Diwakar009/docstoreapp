package com.desktopapp.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.desktopapp.bean.SendEmailBean;
import com.desktopapp.component.common.Attachment;
import com.desktopapp.constants.ArgumentConstants;
import com.desktopapp.jsonobject.DocContentJSON;
import com.desktopapp.model.DocContent;
import com.desktopapp.model.DocContentBlob;
import com.desktopapp.model.DocHolder;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.resource.util.ResourceUtil;
import com.desktopapp.service.common.IEmailGatewayService;
import com.desktopapp.template.ParsedTemplate;
import com.desktopapp.template.helper.TemplateHelper;
import com.desktopapp.util.Base64Util;
import com.desktopapp.util.CommonFileUtil;
import com.desktopapp.util.CommonUtil;
import com.desktopapp.util.KeyValueList;
import com.desktopapp.util.KeyValuePair;

/**
 * Document Content Service
 *
 * @author Ratnala Diwakar Choudhury
 */
public class DocumentContentService extends AbstractService<DocContent> {
    
	private final static Logger LOGGER = Logger
		    .getLogger(DocumentContentService.class);
	
	IEmailGatewayService emailGatewayService;
	

    public DocumentContentService() {
        super(DocContent.class);
    }
    
    	/**
	 * Download documents
	 * 
	 * @param exportDirPath
	 * @param ids
	 * @throws Exception
	 */
	public void downloadDocContents(String exportDirPath,ArrayList<DocContent> ids) throws Exception{
		
	    	LOGGER.debug("downloadDocContents started ");
		
		Map<String,Object> argumentMap = new HashMap<String,Object>();
		argumentMap.put(ArgumentConstants.ARG_MASKDOCMENTS, "N");
		
		try{
		    
		    	List<DocContent> docContents = null;
			
			if(ids == null || ids.size() == 0){
			    docContents = getListWithNamedQuery(DocContent.FIND_ALL);
			}else{
			    docContents = ids;
			}
			
			for(DocContent content : docContents){
			    DocContentJSON contentJSON = new DocContentJSON(argumentMap,content,exportDirPath);
			}
			  getEntityManager().clear();
			
	}catch(Exception anyException){
		anyException.printStackTrace();
		LOGGER.error("Exception Occoured " +  anyException.getMessage());
		throw anyException;
	}
		
		LOGGER.debug("downloadDocContents ended ");
		
	}
	
	
	/**
	 * Upload documents in the docstore tables automatically scaning it.
	 * 
	 * Folder Name would be name
	 * File Name would be document content name
	 * Document search key words would be parent folder to actual folder
	 * 
	 * @param docHolder
	 * @param folderPath
	 * @throws Exception
	 */
	public void uploadFolder(DocHolder docHolder, String folderPath,String docOwner) throws Exception{
		
	    LOGGER.debug("uploadFolder started ");
		//TODO: This bellow method returns worong results needs amendment
		Map<String,List<Path>> fileMap = CommonFileUtil.getDirectoryFileMap(folderPath);
		
		DocumentContentService contentService = (DocumentContentService)ObjectFactory.getServiceObject(ObjectFactory.DOC_CONTENT_SERVICE);
		
		fileMap.forEach((directory,fileList) -> {
		        /*
		        File directoryFile = new File(directory);
			DocHolder holder = new DocHolder();
			//holder.setDocOwner(CommonFileUtil.getFileOwner(directoryFile));
			holder.setDocOwner(docOwner); // Directory upload name
			holder.setDocName(directoryFile.getName());
			holder.setDocKeywords(directory);
			holder.setDocType("OT");
			holder.setDocTypeOthers("Folder Upload");

			create(holder);	
			*/
			//ArrayList<DocContent> contents = new ArrayList<DocContent>();
			
			fileList.forEach(filePath -> {
				DocContent content = new DocContent();
				content.setDocContentName(CommonFileUtil.getFileNameWithoutExtention(filePath.getFileName().toString()));
				content.setDocFileName(filePath.getFileName().toString());
				content.setDocDirPath(filePath.getParent().toString());
				content.setDocFileExtension(FilenameUtils.getExtension(filePath.getFileName().toString()));
				try {
				    content.setDocLastModifiedTime(new Date(Files.getLastModifiedTime(filePath).toMillis()));
				} catch (Exception e1) {
				    // TODO Auto-generated catch block
				    e1.printStackTrace();
				}
				content.setDocHolder(docHolder);
				
				if("Y".equals(docHolder.getDocPersist())){
        				ArrayList<DocContentBlob> contentBlobs = new ArrayList<DocContentBlob>();
        				
        				DocContentBlob contentBlob = new DocContentBlob();
        				contentBlob.setDocContent(content);
        				try {
        				      contentBlob.setDocContentBlob(FileUtils.readFileToByteArray(filePath.toFile()));
        				    //contentBlob.setDocContentBlob(CommonFileUtil.getBlobFromByteArray(FileUtils.readFileToByteArray(filePath.toFile())));
        				} catch (Exception e) {
        					// TODO Auto-generated catch block
        					e.printStackTrace();
        				}
        				contentBlobs.add(contentBlob);
        				
        				content.setDocContentBlobs(contentBlobs);
				}
				//contents.add(content);
				contentService.create(content);
				
			});
			
			getEntityManager().clear();
			
			/*
			holder.setDocContents(contents);
		
			if(holder.getDocContents().size() > 0){
				create(holder);	
				getEntityManager().clear();
			}
			*/
		});
		LOGGER.debug("uploadFolder ended ");
	}
	
	
	public void emailAttachments(SendEmailBean email){
		LOGGER.debug("emailAttachments started ");
		try{
			
			List<String> toList = email.getToList();
			List<String> ccList = email.getCcList();
			List<String> bccList = email.getBccList();
			String subject = email.getSubject();
			String body = email.getBody();
			List<DocContent> docContents = email.getIds();
					    
		    List<Attachment> attachments = new ArrayList<Attachment>();
		    
		    String convertedToHtmlTxt = CommonUtil.convertTxtToHtml(body);
		    KeyValueList parameters = KeyValueList.EMPTY;
		    parameters = new KeyValueList("bodyHtml",convertedToHtmlTxt,parameters);
		    
		    // templateData Get the template data from the VM file
		    String templateData = ResourceUtil.getResourceContent("EmailTemplate.vm");
		    ParsedTemplate template = TemplateHelper.parseTemplate("EMAIL_TEMPLATE", templateData, Locale.US);
		    String emailContent = TemplateHelper.renderTemplate(template, parameters);
		    
			for(DocContent content : docContents){
				List<DocContentBlob> contentBlob = content.getDocContentBlobs();
				if(contentBlob != null){
					DocContentBlob blob = contentBlob.get(0);
					// TODO: file type should be changed based on extension
					Attachment attachment = new Attachment(false, Long.toString(content.getId()), content.getDocFileName(), "application/pdf", Base64Util.encodeString(blob.getDocContentBlob()));
					attachments.add(attachment);
				}
			}
			
			getEntityManager().clear();
			
			emailGatewayService.send(toList, ccList, bccList, subject, emailContent, attachments);
			
	}catch(Exception anyException){
		anyException.printStackTrace();
		LOGGER.error("Exception Occoured " +  anyException.getMessage());
		throw anyException;
	}
		
		
	}
	
	public IEmailGatewayService getEmailGatewayService() {
		return emailGatewayService;
	}

	public void setEmailGatewayService(IEmailGatewayService emailGatewayService) {
		this.emailGatewayService = emailGatewayService;
	}
	

}
