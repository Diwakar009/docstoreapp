/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.service;

import com.desktopapp.model.AppUser;

/**
 * AppUser Service
 *
 * @author Ratnala Diwakar Choudhury
 */
public class AppUserService extends AbstractService<AppUser> {

    public AppUserService() {
        super(AppUser.class);
    }

}
