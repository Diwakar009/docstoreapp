package com.desktopapp.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.desktopapp.jsonobject.UISettingsJSON;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UISettingsService {
    
    public static final String UI_SETTING_JSON_FILE = "uisettings.json";
    
     /**
      * Get UI Setting JSON
      * 
      * @return
      */
    public Map<String,String> getUISettings() {
	InputStream  inputStream = UISettingsService.class.getClassLoader().getResourceAsStream(UI_SETTING_JSON_FILE);
	
	try {
		
	if(inputStream != null){
		StringWriter uiSettingsJSONStr = new StringWriter();
        	IOUtils.copy(inputStream, uiSettingsJSONStr, "UTF-8");
	    	Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	    	UISettingsJSON uiSettingsJSON = gson.fromJson(uiSettingsJSONStr.toString(), UISettingsJSON.class);
	
	    	if(uiSettingsJSON != null){
	    	    return uiSettingsJSON.getUiSettingMap();
	    	}
		
	}
	
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	
	return null;
    }
    
}
