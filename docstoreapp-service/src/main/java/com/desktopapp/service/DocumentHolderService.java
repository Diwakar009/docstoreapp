package com.desktopapp.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.desktopapp.component.constants.DocStoreConstants;
import com.desktopapp.constants.ArgumentConstants;
import com.desktopapp.jsonobject.DocHolderJSON;
import com.desktopapp.jsonobject.DocHolderListJSON;
import com.desktopapp.model.DocContent;
import com.desktopapp.model.DocContentBlob;
import com.desktopapp.model.DocHolder;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.util.CommonFileUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Document Holder Service
 *
 * @author Ratnala Diwakar Choudhury
 */
public class DocumentHolderService extends AbstractService<DocHolder> {
	
    final static Logger logger = Logger.getLogger(DocumentHolderService.class);
    
    final String DELETE_CONTENT_BLOB =  "delete from DocContentBlob where docContent.id =%s";
    final String DELETE_CONTENT = "delete from DocContent where id =%s";
    final String DELETE_DOCHOLDER = "delete from DocHolder where id =%s";
    
    
    /**
     * Remove entity.
     *
     * @param entity entity model
     */
    public void remove(DocHolder entity) {
        getEntityManager().getTransaction().begin();
        
        List<DocContent> contents = entity.getDocContents();
        
        for (DocContent docContent : contents) {
	    getEntityManager().createQuery(String.format(DELETE_CONTENT_BLOB,docContent.getId())).executeUpdate();
	    getEntityManager().createQuery(String.format(DELETE_CONTENT,docContent.getId())).executeUpdate();
	}
        getEntityManager().createQuery(String.format(
        	DELETE_DOCHOLDER,entity.getId())).executeUpdate();
	
        getEntityManager().getTransaction().commit();
        
        getEntityManager().clear();
    }
    
   /**
     * Remove entities.
     *
     * @param entities entity model
     */
    public void remove(ArrayList<DocHolder> entities) {
       getEntityManager().getTransaction().begin();
        for (DocHolder entity : entities) {
            
            List<DocContent> contents = entity.getDocContents();
            
            for (DocContent docContent : contents) {
        	    getEntityManager().createQuery(String.format(DELETE_CONTENT_BLOB,docContent.getId())).executeUpdate();
        	    getEntityManager().createQuery(String.format(DELETE_CONTENT,docContent.getId())).executeUpdate();
    	    }
            
            getEntityManager().createQuery(String.format(
            	DELETE_DOCHOLDER,entity.getId())).executeUpdate();
            
        }
        
        getEntityManager().getTransaction().commit();
        getEntityManager().clear();
    }
    
	/**
	 * Download all the documents
	 * 
	 * @param exportDirPath
	 * @param ids
	 * @throws Exception
	 */
	public void downloadDocHolders(String exportDirPath,ArrayList<DocHolder> ids) throws Exception{
		
		logger.debug("downloadDocHolders started ");
		
		Map<String,Object> argumentMap = new HashMap<String,Object>();
		argumentMap.put(ArgumentConstants.ARG_MASKDOCMENTS, "N");
		
		try{
			DocHolderListJSON backupDocHolder = new DocHolderListJSON();
			List<DocHolder> docHolders = null;
			
			if(ids == null || ids.size() == 0){
				docHolders = getListWithNamedQuery(DocHolder.FIND_ALL);
			}else{
				docHolders = ids;
			}
			
			for(DocHolder holder : docHolders){
				DocHolderJSON holderJSON = new DocHolderJSON(argumentMap,holder,exportDirPath);
				backupDocHolder.getDocHolderJSONList().add(holderJSON);
			}
			
			getEntityManager().clear();
			
    	}catch(Exception anyException){
    		anyException.printStackTrace();
    		logger.error("Exception Occoured " +  anyException.getMessage());
    		throw anyException;
    	}
		
		logger.debug("downloadDocHolders ended ");
		
	}
	
	
	
	/**
	 * Upload documents in the docstore tables automatically scaning it.
	 * 
	 * Folder Name would be docholder name
	 * File Name would be document content name
	 * Document search key words would be parent folder to actual folder
	 * 
	 * 
	 * @param folderPath
	 * @throws Exception
	 */
	public void uploadFolder(String folderPath,String docOwner) throws Exception{
		
		//TODO: This bellow method returns worong results needs amendment
		Map<String,List<Path>> fileMap = CommonFileUtil.getDirectoryFileMap(folderPath);
		
		DocumentContentService contentService = (DocumentContentService)ObjectFactory.getServiceObject(ObjectFactory.DOC_CONTENT_SERVICE);
		
		fileMap.forEach((directory,fileList) -> {
	
			File directoryFile = new File(directory);
			DocHolder holder = new DocHolder();
			//holder.setDocOwner(CommonFileUtil.getFileOwner(directoryFile));
			holder.setDocOwner(docOwner); // Directory upload name
			holder.setDocName(directoryFile.getName());
			holder.setDocKeywords(directory);
			holder.setDocType("OT");
			holder.setDocTypeOthers("Folder Upload");
			holder.setDocPersist("Y");

			create(holder);	
			
			//ArrayList<DocContent> contents = new ArrayList<DocContent>();
			
			fileList.forEach(filePath -> {
				DocContent content = new DocContent();
				content.setDocContentName(CommonFileUtil.getFileNameWithoutExtention(filePath.getFileName().toString()));
				content.setDocFileName(filePath.getFileName().toString());
				content.setDocDirPath(filePath.getParent().toString());
				content.setDocFileExtension(FilenameUtils.getExtension(filePath.getFileName().toString()));
				try {
				    content.setDocLastModifiedTime(new Date(Files.getLastModifiedTime(filePath).toMillis()));
				} catch (Exception e1) {
				    // TODO Auto-generated catch block
				    e1.printStackTrace();
				}
				content.setDocHolder(holder);
				
				ArrayList<DocContentBlob> contentBlobs = new ArrayList<DocContentBlob>();
				
				DocContentBlob contentBlob = new DocContentBlob();
				contentBlob.setDocContent(content);
				try {
				      contentBlob.setDocContentBlob(FileUtils.readFileToByteArray(filePath.toFile()));
				    //contentBlob.setDocContentBlob(CommonFileUtil.getBlobFromByteArray(FileUtils.readFileToByteArray(filePath.toFile())));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				contentBlobs.add(contentBlob);
				
				content.setDocContentBlobs(contentBlobs);
				//contents.add(content);
				contentService.create(content);
				
			});
			
			getEntityManager().clear();
			
			/*
			holder.setDocContents(contents);
		
			if(holder.getDocContents().size() > 0){
				create(holder);	
				getEntityManager().clear();
			}
			*/
		});
	}
	
	
	/**
	 * Back up document holders
	 * 
	 * @param backUpFolderPath
	 * @throws Exception
	 */
    public void backupDocumentHolder(String backUpFolderPath) throws Exception{
    
    	try{
    		List<DocHolder> docHolders = getListWithNamedQuery(DocHolder.FIND_ALL);
			DocHolderListJSON backupDocHolder = new DocHolderListJSON();
			
			Map<String,Object> argumentMap = new HashMap<String,Object>();
			argumentMap.put(ArgumentConstants.ARG_MASKDOCMENTS, "Y");
			
			for(DocHolder holder : docHolders){
				DocHolderJSON holderJSON = new DocHolderJSON(argumentMap,holder,backUpFolderPath);
				backupDocHolder.getDocHolderJSONList().add(holderJSON);
			}
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
		    
			try {
				FileUtils.writeStringToFile(new File(backUpFolderPath + File.separator + DocStoreConstants.MAIN_BACKUP_FILE), gson.toJson(backupDocHolder));
			} catch (IOException e) {
				throw e;
			}
			
			  getEntityManager().clear();
			
    	}catch(Exception anyException){
    		anyException.printStackTrace();
    		throw anyException;
    	}
    }
	
    /**
     * Restore documents holders
     * 
     * @param backUpFolderPath
     * @throws Exception
     */
    public void restoreDocumentHolder(String backUpFolderPath) throws Exception{
    	
    	  try
    	  {
    		  List<DocHolder> holderList = getListWithNamedQuery(DocHolder.FIND_ALL);
				
    		  remove(holderList);
			  
			  try (BufferedReader br = new BufferedReader(new FileReader(backUpFolderPath + File.separator + DocStoreConstants.MAIN_BACKUP_FILE)))
				{
					Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
					DocHolderListJSON backupDocHolder = gson.fromJson(br, DocHolderListJSON.class);
					
					for(DocHolderJSON json : backupDocHolder.getDocHolderJSONList()){
						json.setDocumentStoreDirPath(backUpFolderPath);
						json.copyJSONToEntity();
						DocHolder docHolderEntity = (DocHolder)json.getEntity();
						create(docHolderEntity);
						getEntityManager().clear();
					}
				}catch (IOException e) {
					throw e;
				}
    	  }catch(Exception anyException){
    		  throw anyException;
    	  }
    }
	
	public DocumentHolderService() {
        super(DocHolder.class);
    }

}
