package com.desktopapp.service;

import static com.desktopapp.service.QueryParameter.with;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.desktopapp.component.common.CodeDataItem;
import com.desktopapp.model.StaticCodeDecode;

/**
 * Static COde Decode Service
 *
 * @author Ratnala Diwakar Choudhury
 */
public class StaticCodeDecodeService extends AbstractService<StaticCodeDecode> {

    public StaticCodeDecodeService() {
        super(StaticCodeDecode.class);
    }
    
    
    /**
     * Get code decode item list with default en_us
     * 
     * @param codeName
     * @return
     */
    public Vector<CodeDataItem> getCodeDataItemList(String codeName){
    	return getCodeDataItemList(codeName, "EN_US");
    }
    
    public Vector<CodeDataItem> getCodeDataItemList(String codeName, String langauage){
	return getCodeDataItemList(codeName, langauage,false);
    }
    
    
    /**
     * Get code Data item List
     * 
     * @param codeName
     * @param langauage
     * @return
     * 
     */
    public Vector<CodeDataItem> getCodeDataItemList(String codeName, String langauage,boolean emptyCode){
    	
    	Vector<CodeDataItem> list = new Vector<CodeDataItem>();
    	CodeDataItem EMPTY_CODE_DECODE_ITEM =  new CodeDataItem("","","");
    	
    	if(!emptyCode){
    	    list.add(EMPTY_CODE_DECODE_ITEM);
    	}
    	
    	List<StaticCodeDecode> codeDecodeList = getListWithNamedQuery(StaticCodeDecode.FIND_BY_CODE_NAME_N_LANG,
                with("codeName", codeName).
                and("language", langauage)
                .parameters());
    	
    	if(codeDecodeList != null){
    		for(StaticCodeDecode codeDecode : codeDecodeList){
    			CodeDataItem item = new CodeDataItem(codeDecode.getCodeName(),codeDecode.getCodeValue(),codeDecode.getCodeDesc());
    			list.add(item);
    		}
    	}
    	
    	getEntityManager().clear();
    	
    	return list;
    }
    
    
    

}
