package com.desktopapp.test;

import org.junit.Test;

import com.desktopapp.contoller.DocumentHolderController;



public class DocumentStoreTestcase {
	
	@Test
	public void backup(){
		System.out.println("Back up started.....");
		DocumentHolderController holder = new  DocumentHolderController();
		holder.onBackup();
		System.out.println("Back up ended.....");
		
	}
	
	
	@Test
	public void restore(){
		System.out.println("Restore started.....");
		DocumentHolderController holder = new  DocumentHolderController();
		holder.onRestore();
		System.out.println("Restore ended.....");
		
	}
	

}
