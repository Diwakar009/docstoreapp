package com.desktopapp.test;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.jvnet.lafwidget.LafWidget;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.api.SubstanceConstants.TabContentPaneBorderKind;
import org.jvnet.substance.skin.SubstanceOfficeBlue2007LookAndFeel;

import com.desktopapp.component.JCodeDecodeField;
import com.desktopapp.component.JComboBoxExt;
import com.desktopapp.component.JTextFieldExt;

public class SwingComponentTest extends JFrame {

	public static void main(String args[]){
		 SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	            	
	            	 JFrame.setDefaultLookAndFeelDecorated(true);
	                 JDialog.setDefaultLookAndFeelDecorated(true);

	                 try {
	                     //UIManager.setLookAndFeel(new SubstanceOfficeSilver2007LookAndFeel());
	                 	UIManager.setLookAndFeel(new SubstanceOfficeBlue2007LookAndFeel());
	                 } catch (UnsupportedLookAndFeelException ex) {
	                 }

	                 // TabbedPane border settings in substance look and feel. 
	                 UIManager.put(SubstanceLookAndFeel.TABBED_PANE_CONTENT_BORDER_KIND,
	                         TabContentPaneBorderKind.DOUBLE_PLACEMENT);

	                 // Cut, copy, paste menu in TextField with substance look and feel. 
	                 UIManager.put(LafWidget.TEXT_EDIT_CONTEXT_MENU, true);

	            	
	            	SwingComponentTest frame = new SwingComponentTest();
	            	
	            	JPanel panel = new JPanel();
	            	JTextFieldExt txtCodeValue = new JTextFieldExt(5); 
	            	JComboBoxExt cbDecodeDesc = new JComboBoxExt();
	            	
	            	 JCodeDecodeField codeDecode = new JCodeDecodeField(txtCodeValue, cbDecodeDesc, "CD_LANGUAGE");
	            	
	            	panel.add(txtCodeValue);
	            	panel.add(cbDecodeDesc);
	            	
	            	cbDecodeDesc.setEnabled(false);
	            	
	            	
	            	txtCodeValue.setDataValue("4");
	            	
	            	frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	            	
	            	frame.add(panel);
	            	frame.setSize(500, 500);
	            	frame.pack();
	            	frame.setVisible(true);
	            }
	        });	
		
	}
}
