package com.desktopapp.test;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import java.net.*;
import static javax.swing.JOptionPane.*;
public class A implements ActionListener	{
	private BufferedImage image;
	private JFrame frame;
	private String fileName;
	private JMenuItem menuItem[] = new JMenuItem[2];
	private JLabel area = new JLabel();
	private JScrollPane JSp = new JScrollPane(area,
			JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	private JFileChooser dialog = new JFileChooser();

	public A() {
		if (frame == null) {
			frame = new JFrame();
			JMenuBar menuBar = new JMenuBar();
            JMenu menu = new JMenu("File");
            frame.add(menuBar,BorderLayout.NORTH);
            frame.add(JSp);
            menuBar.add(menu);
            menuItem[0] = new JMenuItem("Open...");
            menuItem[1] = new JMenuItem("Exit");
            menuItem[0].addActionListener(this);
            menuItem[1].addActionListener(this);
            menuItem[1].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
            						 Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
            menuItem[0].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
                                     Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
            menu.add(menuItem[0]);
            menu.add(menuItem[1]);
            frame.setJMenuBar(menuBar);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setTitle(fileName);
            frame.setResizable(false);
            frame.pack();
            frame.setVisible(true);
		}
		frame.repaint();
	}
	public void readInFile(String fileName) {
		this.fileName = fileName;
		File file = new File(fileName);
		if(file.isFile()) {
            try {
				image = ImageIO.read(file);
			} catch (IOException e) {
				showMessageDialog(frame,"Does not compute !","No file read or found",INFORMATION_MESSAGE);
				e.printStackTrace();
			}
        }
		else {
            URL url = getClass().getResource(fileName);
            if (url == null) { 
            	try {
					url = new URL(fileName);
				} catch (MalformedURLException e) {
					showMessageDialog(frame,"Does not compute !","No Image file or found",INFORMATION_MESSAGE);
					e.printStackTrace();
				} 
            }
            try {
				image = ImageIO.read(url);
			} catch (IOException e) {
				showMessageDialog(frame,"Does not compute !","No Image file",WARNING_MESSAGE);
				e.printStackTrace();
			}
       }
}
	public void setImage(JLabel area){
		ImageIcon icon = new ImageIcon(image);
		area.setIcon(icon);
		frame.setSize(icon.getIconWidth(),icon.getIconHeight());
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==menuItem[0]) {
			if(dialog.showOpenDialog(null)==(JFileChooser.APPROVE_OPTION)) {
					readInFile(dialog.getSelectedFile().getAbsolutePath());
				if(image==null) {
					showMessageDialog(frame,"Does not compute !","No Image file",INFORMATION_MESSAGE);
				}
				else {
					setImage(area);
				}
			}
		}
		else if(e.getSource()==menuItem[1]) {
			System.exit(0);
		}
	}
	public static void main(String[] arg) {
		new A();
	}
}