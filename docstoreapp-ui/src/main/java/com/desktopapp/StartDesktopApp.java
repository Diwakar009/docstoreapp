package com.desktopapp;

import java.util.Locale;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.desktopapp.component.DesktopApp;
import com.desktopapp.component.LoginPasswordDialog;
import com.desktopapp.component.MessageBox;
import com.desktopapp.contoller.AppController;
import com.desktopapp.util.I18n;
import com.desktopapp.util.JpaUtil;
import com.desktopapp.util.ViewHelpers;


/**
 * Desktop application.
 *
 * @author Ratnala Diwakar Choudhury
 */
public class StartDesktopApp extends AppController {

    private final static Logger LOGGER = Logger.getLogger(StartDesktopApp.class);
  
    public static DesktopApp desktopApp;
    /**
     * Customers Desktop Application
     */
    public StartDesktopApp() {
	
	

    }

    /**
     * Application main class.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // set locale for i18n
        Locale.setDefault(new Locale("en", "US"));
        // for german
        //Locale.setDefault(new Locale("de", "DE"));
        // for turkish
        //Locale.setDefault(new Locale("tr", "TR"));
        
        // application splash form
        
       
        try{
        
            desktopApp = new DesktopApp(I18n.DOCUMENTHOLDER.getString("App.Title"),
                    ViewHelpers.ICONS16 + "documents.png",
                    ViewHelpers.IMAGES + "splash.png");
            
                     
            desktopApp.setVisible(true);
    
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                LOGGER.error(ex);
            }
    
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    controller = new StartDesktopApp();
                    controller.setDesktopApp(desktopApp);
                    try {
			controller.start();
		    } catch (Exception e) {
			 MessageBox.showError("Application Controller Initialization Error", e);
			 JpaUtil.closeEntityManagerFactory();
			 System.exit(0);
		    }
                }
            });
            
            /*
            Thread backgroundThread = new Thread(new Runnable() {
	        @Override
	        public void run() {
	            while(Thread.currentThread().isAlive()){
	                    try {
        			Thread.currentThread().sleep(1000);
        		    } catch (InterruptedException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		    }
        	            System.out.println("BackGround Thread Running Every 5 seconds");
	            }
	        }
	    });
            backgroundThread.start();
            */
            
        }catch(Exception e){
            MessageBox.showError("Application Initialization Error", e);
            JpaUtil.closeEntityManagerFactory();
            System.exit(0);
            
        }
    }
    
   
}
