package com.desktopapp.component;

import java.awt.event.ActionListener;

import org.jvnet.flamingo.common.AbstractCommandButton;
import org.jvnet.flamingo.common.icon.ResizableIcon;
import org.jvnet.flamingo.ribbon.JRibbonBand;
import org.jvnet.flamingo.ribbon.RibbonElementPriority;

import com.desktopapp.session.LoginSession;
import com.desktopapp.view.AppView;


/**
 * Provides additional access check before displaying the component
 * 
 * @author "Diwakar Choudhury"
 *
 */
public class JRibbonBandExt extends JRibbonBand {

    public JRibbonBandExt(String title, ResizableIcon icon) {
	super(title, icon);
	// TODO Auto-generated constructor stub
    }

    public JRibbonBandExt(String title, ResizableIcon icon,
	    ActionListener expandActionListener) {
	super(title, icon, expandActionListener);
	// TODO Auto-generated constructor stub
    }

    public void addCommandButton(AbstractCommandButton commandButton,
	    RibbonElementPriority priority,String functionName) {

	if(AppView.hasFunctionAccess(functionName)){
	    super.addCommandButton(commandButton, priority);
	}
    }
}
