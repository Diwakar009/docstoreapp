package com.desktopapp.component.constants;

public class UIConstants {

	
	/**
	 * append debug to see the outling of the UI.
	 *  eg . insets 0 0 0 0,debug
	 */
	public static String MIGLAYOUT_PARAM_1_INSETS_0 = "insets 0 0 0 0";
	public static String MIGLAYOUT_PARAM_1_INSETS_VAR = "insets 20 10 10 10";
	
	public static String DEFAULT_LANGUAGE = "EN_US";
	
}
