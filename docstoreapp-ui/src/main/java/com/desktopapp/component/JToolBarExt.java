package com.desktopapp.component;

import java.awt.Component;

import javax.swing.JToolBar;

import com.desktopapp.component.common.IGenericComponent;
import com.desktopapp.view.AppView;

public class JToolBarExt extends JToolBar implements IGenericComponent{

    /**
     * Add tool button to the tool bar.
     * 
     * @param paramComponent
     * @param functionName
     * @return
     */
    public Component addToolButton(Component paramComponent,String functionName) {
	if(AppView.hasFunctionAccess(functionName)){
	    return add(paramComponent);
	}
	
	return null;
    }
    
    @Override
    public void setAsReadOnly(boolean readOnly) {
	
    }

    @Override
    public void clearDataValue() {
	
	
    }

    @Override
    public void destroyComponent() {
	
	
    }
    

}
