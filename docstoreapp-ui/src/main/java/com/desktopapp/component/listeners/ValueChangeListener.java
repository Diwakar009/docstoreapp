package com.desktopapp.component.listeners;

public interface ValueChangeListener {
	
	public void valueChanged();

}
