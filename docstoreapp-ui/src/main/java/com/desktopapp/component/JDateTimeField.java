package com.desktopapp.component;

public class JDateTimeField extends JTextFieldExt {

    public JDateTimeField(boolean readonly) {
	super(40, readonly);
    }
    
    public JDateTimeField() {
	super(40, false);
    }

}
