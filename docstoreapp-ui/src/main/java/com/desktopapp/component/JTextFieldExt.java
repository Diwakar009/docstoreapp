package com.desktopapp.component;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.desktopapp.component.common.IGenericComponent;
import com.desktopapp.component.listeners.ValueChangeListener;
import com.desktopapp.util.CustomStringUtils;

/**
 * JTextFieldExt Component.
 *
 * <p>
 *      JTextField with max lenght property. <br/>
 * </p>
 *
 * <p>
 * <pre><code>
 *      JTextFieldExt tfxName = new JTextFieldExt(50);
 * </code></pre>
 * </p>
 *
 * @author Ratnala Diwakar Choudhury
 */
public class JTextFieldExt extends JTextField implements IGenericComponent{
	
	
	/**
     * max lenght property
     */
	
	private String name = "";
	
    private int maxLength;
    
    private List<ValueChangeListener> valueChangeListenerList = null;
    
    private boolean disableValueChangeListener = false;
    
    private String prevTextValue = "";
    
   // private boolean fromValueChangeListener = false;
    public boolean mandatory = false;
	
    /**
     * Constructor with max lenght property
     *
     * @param maxLength max lenght property
     */
    public JTextFieldExt(String name,int maxLength) {
        super();
        this.name = name;
        this.maxLength = maxLength;
        this.setDocument(new PlainDocumentExt(maxLength));
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
    }
    
    
    /**
     * Constructor with max lenght property
     *
     * @param maxLength max lenght property
     */
    public JTextFieldExt(int maxLength) {
        super();
        this.maxLength = maxLength;
        this.setDocument(new PlainDocumentExt(maxLength));
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
    }
    
    /**
     * Constructor with max lenght property and readonly
     *
     * @param maxLength max lenght property
     */
    public JTextFieldExt(int maxLength,boolean readonly) {
        super();
        this.maxLength = maxLength;
        this.setDocument(new PlainDocumentExt(maxLength));
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
        setAsReadOnly(readonly);
    }

    /**
     * Gets max length
     *
     * @return maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * Sets max length
     *
     * @param maxLength max length property
     */
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
        this.setDocument(new PlainDocumentExt(maxLength));
    }

    /**
     * Set Data Value
     * @param value
     */
	public void setDataValue(String value){
		
		if(value == null)
			value = "";
		
		if(!prevTextValue.equals(value)){
			setText(value);
			
			
			if(!isDisableValueChangeListener()){
				disableValueChangeListener = true;
				for (ValueChangeListener listener : valueChangeListenerList) {
						listener.valueChanged();
				}
				disableValueChangeListener = false;
			}
			
			
			
		}else{
			setText(value);
		}
	}
	
	/**
	 * Get Data Value (Do any formating if necessary)
	 * @return
	 */
	public String getDataValue() {
		return super.getText();
	}

	public boolean isDisableValueChangeListener() {
		return disableValueChangeListener;
	}

	public void setDisableValueChangeListener(boolean disableValueChangeListener) {
		this.disableValueChangeListener = disableValueChangeListener;
	}
	
	public void addValueChangeListener(ValueChangeListener valueChangeListener){
		valueChangeListenerList.add(valueChangeListener);
	}

	@Override
	public void setText(String t) {
		// TODO Auto-generated method stub
		super.setText(t);
		
		
		if(t != null){
			prevTextValue = t;
		}else{
			prevTextValue = "";
		}
	}

	
	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	@Override
	public void setAsReadOnly(boolean readOnly) {
		setEnabled(!readOnly);
	}

	@Override
	public void clearDataValue() {
		setText("");
	}

	@Override
	public void destroyComponent() {
		valueChangeListenerList = null;
	}

	@Override
	public boolean validateInput() {
		
		if(this.mandatory){
			if(CustomStringUtils.isEmpty(getDataValue())){
				 MessageBox.showWarning(this.name + " Field is mandatory!");
		         requestFocus();
		         return false;
		    }
		}

		return true;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
}

/*
 * Plain document with max length
 */
class PlainDocumentExt extends PlainDocument {

    private final int maxLength;

    public PlainDocumentExt(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public void insertString(int offset, String str, AttributeSet a)
            throws BadLocationException {

        if (str.length() == 0) {
            return;
        }

        if (getLength() + str.length() <= maxLength) {
            super.insertString(offset, str, a);
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
    }

}
