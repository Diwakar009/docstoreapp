package com.desktopapp.component;

import com.desktopapp.util.ValidationUtil;
import com.desktopapp.view.varifier.EmailAddressInputVerifier;

public class JEmailTextField extends JTextFieldExt {
	
	
	public JEmailTextField(String name){

		super(name,1000);
		
		init();

	}

	public void init(){
		setInputVerifier(new EmailAddressInputVerifier());
	}

	@Override
	public boolean validateInput() {
		
		if(!super.validateInput()){
			return false;
		}
		
		if(!ValidationUtil.isEmailFormatValid(getDataValue())){
			 MessageBox.showWarning( this.getName() + " Email format is wrong!");
	         requestFocus();
	         return false;
		}
		
		return true;
	}
	
	

}
