package com.desktopapp.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.temporal.WeekFields;
import java.util.Date;
import java.util.Locale;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

import net.miginfocom.swing.MigLayout;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXHeader;

import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.contoller.AppUserLoginController;
import com.desktopapp.model.AppUserLogin;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.resource.util.ResourceUtil;
import com.desktopapp.service.util.PasswordHash;
import com.desktopapp.session.LoginSession;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;
import com.desktopapp.view.AppView;

/**
 * Login Password Dialog
 * 
 * @author "Diwakar Choudhury"
 *
 */
public class LoginPasswordDialog extends JDialog {

    private final static Logger LOGGER = Logger
	    .getLogger(LoginPasswordDialog.class);
    
    private JXHeader xheader;
    private JButton btnOk;
    private JButton btnCancel;
    private Action acOk;
    private Action acCancel;
    private JTextFieldExt jtfUsername;
    private JPasswordFieldExt jpfPassword;
    
    private KeyEventDispatcher loginKeyEventDispatcher;
    /*
    private JTextFieldExt txtLanguage;
    private JComboBoxExt cbLanguage;
    private JCodeDecodeField cdLanguage;
    */
   
    private JLabel jlblStatus = new JLabel(" ");

    public LoginPasswordDialog() {
        this(null, true);
    }

    public LoginPasswordDialog(final JFrame parent, boolean modal) {
        super(parent, modal);
        
       

        setLayout(new MigLayout());
        setTitle(I18n.COMPONENT.getString("LoginPassword.LoginDialog"));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        jtfUsername = new JTextFieldExt(100);
        jpfPassword = new JPasswordFieldExt(50,false);
    
        /*
        txtLanguage= new JTextFieldExt(5);
        cbLanguage = new JComboBoxExt();
      
        // Read only as the support is only english
        txtLanguage.setPreferredSize(cbLanguage.getPreferredSize());
        cdLanguage = new JCodeDecodeField(txtLanguage, cbLanguage, DocStoreConstants.CD_LANGUAGE);
        txtLanguage.setDataValue(UIConstants.DEFAULT_LANGUAGE);
        cdLanguage.setAsReadOnly(true);
        */
        
        jlblStatus.setForeground(Color.RED);
        jlblStatus.setHorizontalAlignment(SwingConstants.CENTER);
        
        xheader = new JXHeader();
        xheader.setTitle("<html><body><b>"
                + I18n.COMPONENT.getString("LoginPassword.Header.Title")
                + "</b></body></html>");
        xheader.setDescription(I18n.COMPONENT.getString("LoginPassword.Header.Description"));
        xheader.setFont(new Font("Tahoma", 0, 12));
        xheader.setIcon(new ImageIcon(getClass().getResource(
                ViewHelpers.ICONS22 + "documents.png")));
        xheader.setBorder(new MatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));
        
        /**
         * 
         * Login key dispatcher for enter to on and escape to cancel
         * 
         * 
         */
        
        loginKeyEventDispatcher = new KeyEventDispatcher() {
                @Override
                public boolean dispatchKeyEvent(KeyEvent e) {
                  
                  switch(e.getKeyCode()){
                  case KeyEvent.VK_ESCAPE:
                      parent.dispose();
                      destroy();
                      System.exit(0);  
                      return true;
                	  
                  case KeyEvent.VK_ENTER:
            	     btnOk.doClick();
            	     return true;	  
                	            	  
                  default:
                	  return false;
                  
                  }
    			  
                }
        };
        
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(loginKeyEventDispatcher);
        
        acOk = new AbstractAction(I18n.COMMON.getString("Action.Ok"),
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "ok.png"))) {
            
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent e) {

                        
                        String defaultUser = ResourceUtil.getPropertyValue("DEFAULT_USER");
                        String accessLevel = "";
                        AppUserLoginController appUserLoginController = (AppUserLoginController)ObjectFactory.getControllerObject(ObjectFactory.APP_USER_LOGIN_CTRL);
            	    
                        LoginSession session = new LoginSession();
                    	boolean isLoginSuccessfull = false;
                        if (defaultUser.equals(jtfUsername.getDataValue())) { // if the username is default username 
                            String passwordHash = ResourceUtil.getPropertyValue("DEFAULT_PWD_HASH");
                            try {
                		   if(PasswordHash.validatePassword(jpfPassword.getPassword(), passwordHash)){
                			isLoginSuccessfull = true;
                			accessLevel = ResourceUtil.getPropertyValue("DEFAULT_USER_ACCESS");
                		   }
            		    } catch (NoSuchAlgorithmException e1) {
            			// TODO Auto-generated catch block
            			LOGGER.error(e1);
            			e1.printStackTrace();
            		    } catch (InvalidKeySpecException e1) {
            			// TODO Auto-generated catch block
            			LOGGER.error(e1);
            			e1.printStackTrace();
            		    }
                    	}else{
                    	    // get the username from DB and connect
                            // its not default super user
                    	    AppUserLogin appUserLogin = appUserLoginController.getAppUserLoginByUserName(jtfUsername.getDataValue());
                    	    
                    	    if(appUserLogin != null){
            			try {
            			    if (PasswordHash.validatePassword(
            				    jpfPassword.getPassword(), appUserLogin.getPassword())) {
            				isLoginSuccessfull = true;
            				accessLevel = appUserLogin.getFnAccessLevel();
            			    }
            			} catch (NoSuchAlgorithmException e1) {
            			    // TODO Auto-generated catch block
            			    LOGGER.error(e1);
            			    e1.printStackTrace();
            			} catch (InvalidKeySpecException e1) {
            			    // TODO Auto-generated catch block
            			    LOGGER.error(e1);
            			    e1.printStackTrace();
            			}
                    	    }
                        }
                    	    
                    	if(isLoginSuccessfull)    {
                    	        
                    		KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(loginKeyEventDispatcher);
                    		KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
                    	        
                    	    	Locale.setDefault(new Locale("en", "US")); // set the locale
                    	    	//session.setLanguage(txtLanguage.getText());
                    	    	session.setUserName(jtfUsername.getDataValue());
                            	session.setAccessType(accessLevel);
                            	session.setFunctionAccessList(appUserLoginController.getFunctionAccessList(accessLevel));
                            	session.setUISettings(appUserLoginController.getUISettings());
                            	session.setLoginTime(new Date().toString());
                            	AppView.setLoginSession(session);
                            	parent.setVisible(true);
                            	setVisible(false);
                            	dispose();
                    	}else{
                    	    // get the username from DB and connect
                            jlblStatus.setText("Invalid username or password");
                        }
                    	    
                  }
                };
        btnOk = new JButton(acOk);

        acCancel = new AbstractAction(I18n.COMMON.getString("Action.Cancel"),
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "cancel.png"))) {
                    @Override
                    public void actionPerformed(java.awt.event.ActionEvent e) {
                        setVisible(false);
                        parent.dispose();
                        destroy();
                        System.exit(0);
                    }
                };
        btnCancel = new JButton(acCancel);
        
        
        JPanel buttonPanel = new JPanel(new MigLayout("nogrid, fillx, aligny 100%, gapy unrel"));
        buttonPanel.add(btnCancel, "tag cancel");
        buttonPanel.add(btnOk, "tag ok");

        JPanel buttonBar = new JPanel(new BorderLayout());
        buttonBar.add(new JSeparator(), BorderLayout.NORTH);
        buttonBar.add(buttonPanel, BorderLayout.CENTER);

        getContentPane().add(xheader, "dock north");
        getContentPane().add(buildCenterPanel(), "dock center");
        getContentPane().add(buttonBar, "dock south");
        
        pack();
        setLocationRelativeTo(null);
        
        setSize(400, 250);
        
        setResizable(false);
        
        addWindowListener(new WindowAdapter() {  
            @Override
            public void windowClosing(WindowEvent e) { 
        	parent.dispose();
        	destroy();
                System.exit(0);  
            }  
        });
        
      
        
        // TODO : Remove this only for testing purpose
        /*
        jtfUsername.setText(ResourceUtil.getPropertyValue("DEFAULT_USER"));
        jpfPassword.setText("admin123");
        */
    }
    
    
      
    /**
     * Center Panel
     */
    private JPanel buildCenterPanel() {
	 JPanel outerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
	 JPanel firstInnerPanel = new JPanel(new MigLayout("insets 20 10 10 10", "[][fill,grow][fill,grow]", ""));
	
	 firstInnerPanel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.username")), "gap para");
	 firstInnerPanel.add(jtfUsername,"span"); 
        
	 firstInnerPanel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.password")), "gap para");
	 firstInnerPanel.add(jpfPassword,"span"); 
	 /*
	 firstInnerPanel.add(new JLabel(I18n.STATICCODEDECODE.getString("StaticCodeDecode.Form.Language")), "gap para");
	 firstInnerPanel.add(cdLanguage.getCodeDecodeUIPanel(), "span");
	 */
	 outerPanel.add(firstInnerPanel,"align,span");
	 
	 JPanel secondInnerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
		
	 secondInnerPanel.add(jlblStatus,"span");
	 outerPanel.add(secondInnerPanel, "align");
	
        return outerPanel;
    }
    
    
    private void destroy(){
	
	   xheader = null;
	   btnOk= null;
	   btnCancel= null;
	   acOk= null;
	   acCancel= null;
	   jtfUsername= null;
	   jpfPassword= null;
	   jlblStatus = null;
	   loginKeyEventDispatcher = null;
	   
	   /*
	    txtLanguage = null;
	    cbLanguage= null
	    cdLanguage= null
	    */
	
	dispose();
    }
    
    
}