package com.desktopapp.component;

import org.jvnet.flamingo.ribbon.RibbonApplicationMenu;
import org.jvnet.flamingo.ribbon.RibbonApplicationMenuEntryPrimary;

import com.desktopapp.view.AppView;

public class RibbonApplicationMenuExt extends RibbonApplicationMenu {

    public synchronized void addMenuEntry(
	    RibbonApplicationMenuEntryPrimary entry,String functionName) {
	
	if(AppView.hasFunctionAccess(functionName)){
	    super.addMenuEntry(entry);
	}
    }
   
}
