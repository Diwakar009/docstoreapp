package com.desktopapp.component;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPasswordField;

import org.jvnet.lafwidget.LafWidget;
import org.jvnet.lafwidget.text.PasswordStrengthChecker;
import org.jvnet.lafwidget.text.PasswordStrengthCheckerWidget;
import org.jvnet.lafwidget.utils.LafConstants.PasswordStrength;

import com.desktopapp.component.common.IGenericComponent;
import com.desktopapp.component.listeners.ValueChangeListener;

/**
 * JTextFieldExt Component.
 *
 * <p>
 *      JTextField with max lenght property. <br/>
 * </p>
 *
 * <p>
 * <pre><code>
 *      JTextFieldExt tfxName = new JTextFieldExt(50);
 * </code></pre>
 * </p>
 *
 * @author Ratnala Diwakar Choudhury
 */
public class JPasswordFieldExt extends JPasswordField implements IGenericComponent{

    /**
     * max lenght property
     */
    private int maxLength;
    
    private List<ValueChangeListener> valueChangeListenerList = null;
    
     /**
     * Constructor with max lenght property
     *
     * @param maxLength max lenght property
     */
    public JPasswordFieldExt(int maxLength) {
        super();
        this.maxLength = maxLength;
        putClientProperty(LafWidget.PASSWORD_STRENGTH_CHECKER, new PasswordStrengthCheckerExt());
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
    }
    
    /**
     * Constructor with max lenght property and readonly
     *
     * @param maxLength max lenght property
     */
    public JPasswordFieldExt(int maxLength,boolean passwordStrengthChecker) {
        super();
        this.maxLength = maxLength;
        if(passwordStrengthChecker){
            putClientProperty(LafWidget.PASSWORD_STRENGTH_CHECKER, new PasswordStrengthCheckerExt());
        }
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
    }

    /**
     * Gets max length
     *
     * @return maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * Sets max length
     *
     * @param maxLength max length property
     */
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

 	public void addValueChangeListener(ValueChangeListener valueChangeListener){
		valueChangeListenerList.add(valueChangeListener);
	}

	@Override
	public void setAsReadOnly(boolean readOnly) {
		setEnabled(!readOnly);
	}

	@Override
	public void clearDataValue() {
		setText("");
	}

	@Override
	public void destroyComponent() {
		valueChangeListenerList = null;
	}
	
}

class PasswordStrengthCheckerExt implements PasswordStrengthChecker {

    @Override
    public PasswordStrength getStrength(char[] paramArrayOfChar) {
	if (paramArrayOfChar == null)
	    return PasswordStrength.WEAK;
	int length = paramArrayOfChar.length;
	if (length < 3)
	    return PasswordStrength.WEAK;
	if (length < 6)
	    return PasswordStrength.MEDIUM;
	
	return PasswordStrength.STRONG;
    }

    @Override
    public String getDescription(PasswordStrength paramPasswordStrength) {
	 if (paramPasswordStrength == PasswordStrength.WEAK)
	    return "This password is way too weak";
	if (paramPasswordStrength == PasswordStrength.MEDIUM)
	    return "Come on, you can do a little better than that";
	if (paramPasswordStrength == PasswordStrength.STRONG)
	    return "OK";
	return null;
    }


}


