package com.desktopapp.component;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.desktopapp.component.common.IGenericComponent;
import com.desktopapp.component.listeners.ValueChangeListener;

/**
 * JTextAreaExt Component.
 *
 * <p>
 *      JTextArea with max lenght property. <br/>
 * </p>
 *
 * <p>
 * <pre><code>
 *      JTextAreaExt tfxName = new JTextFieldExt(50);
 * </code></pre>
 * </p>
 *
 * @author Ratnala Diwakar Choudhury
 */
public class JTextAreaExt extends JTextArea implements IGenericComponent{

    /**
     * max lenght property
     */
    private int maxLength;
    
    private List<ValueChangeListener> valueChangeListenerList = null;
    
    private boolean disableValueChangeListener = false;
    
    private String prevTextValue = "";
    
    /**
     * Constructor with max lenght property
     *
     * @param maxLength max lenght property
     */
    public JTextAreaExt(int maxLength) {
        super();
        this.maxLength = maxLength;
        this.setDocument(new PlainDocumentTextAreaExt(maxLength));
        valueChangeListenerList = new ArrayList<ValueChangeListener>();
    }

    /**
     * Gets max length
     *
     * @return maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * Sets max length
     *
     * @param maxLength max length property
     */
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
        this.setDocument(new PlainDocumentExt(maxLength));
    }

    public String getDataValue() {
		return super.getText();
	}
    /**
     * Set Data Value
     * @param value
     */
	public void setDataValue(String value){
		
		
		if(!prevTextValue.equals(value)){
			if(!isDisableValueChangeListener()){
				for (ValueChangeListener listener : valueChangeListenerList) {
						listener.valueChanged();
				}
			}
		}
		
		setText(value);
	}

	public boolean isDisableValueChangeListener() {
		return disableValueChangeListener;
	}

	public void setDisableValueChangeListener(boolean disableValueChangeListener) {
		this.disableValueChangeListener = disableValueChangeListener;
	}
	
	public void addValueChangeListener(ValueChangeListener valueChangeListener){
		valueChangeListenerList.add(valueChangeListener);
	}

	@Override
	public void setText(String t) {
		// TODO Auto-generated method stub
		super.setText(t);
		
		prevTextValue = t;
	}

	@Override
	public void setAsReadOnly(boolean readOnly) {
		setEditable(readOnly);
	}

	@Override
	public void clearDataValue() {
		setText("");
	}

	@Override
	public void destroyComponent() {
		valueChangeListenerList = null;
	}
	
}

/*
 * Plain document with max length
 */
class PlainDocumentTextAreaExt extends PlainDocument {

    private final int maxLength;

    public PlainDocumentTextAreaExt(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public void insertString(int offset, String str, AttributeSet a)
            throws BadLocationException {

        if (str.length() == 0) {
            return;
        }

        if (getLength() + str.length() <= maxLength) {
            super.insertString(offset, str, a);
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
    }

}
