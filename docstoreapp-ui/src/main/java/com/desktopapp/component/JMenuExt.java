package com.desktopapp.component;

import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import com.desktopapp.view.AppView;

public class JMenuExt extends JMenu{

    public JMenuExt() {
	super();
	// TODO Auto-generated constructor stub
    }

    public JMenuExt(Action paramAction) {
	super(paramAction);
	// TODO Auto-generated constructor stub
    }

    public JMenuExt(String paramString, boolean paramBoolean) {
	super(paramString, paramBoolean);
	// TODO Auto-generated constructor stub
    }

    public JMenuExt(String paramString) {
	super(paramString);
	// TODO Auto-generated constructor stub
    }

    public JMenuItem add(Action action,String functionName) {
	
	if(AppView.hasFunctionAccess(functionName)){
	    return super.add(action);
	}
	
	return null;
    }
    
    

}
