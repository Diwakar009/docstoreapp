package com.desktopapp.component;

import javax.swing.JComboBox;

import com.desktopapp.component.common.IGenericComponent;

public class JComboBoxExt<E> extends JComboBox<E> implements IGenericComponent {

	
	
	@Override
	public void setAsReadOnly(boolean readOnly) {
		// TODO Auto-generated method stub
		setEditable(!readOnly);
	}

	@Override
	public void clearDataValue() {
		setSelectedIndex(0);
	}

	@Override
	public void destroyComponent() {
		// TODO Auto-generated method stub
		
	}

}
