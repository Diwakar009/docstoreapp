package com.desktopapp.component;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import net.miginfocom.swing.MigLayout;

import com.desktopapp.component.common.CodeDataItem;
import com.desktopapp.component.common.IGenericComponent;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.component.listeners.ValueChangeListener;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.service.StaticCodeDecodeService;

/**
 * Code Decode Field
 * 
 * @author diwakarchoudhury
 *
 */
public class JCodeDecodeField implements IGenericComponent{
	
	private String codeName = "";
	
	private JTextFieldExt txtCodeValue;
	
    private JComboBoxExt<CodeDataItem> cbDecodeDesc;
    
    private Vector<CodeDataItem> codeDataItemList = new Vector<CodeDataItem>();
    
    private StaticCodeDecodeService service = null;
    
    private CodeDataItem EMPTY_CODE_DECODE = new CodeDataItem("","","");
    
    private final String EN_US = "EN_US";
    
    private boolean isEmptyCode = false;
    
   
    
    /**
     * Constructor
     * 
     * @param txtCodeValue
     * @param cbDecodeDesc
     * @param codeName
     */
	public JCodeDecodeField(JTextFieldExt txtCodeValue,
			JComboBoxExt<CodeDataItem> cbDecodeDesc,String codeName) {
		super();
		this.codeName = codeName;
		this.txtCodeValue = txtCodeValue;
		this.cbDecodeDesc = cbDecodeDesc;
		this.isEmptyCode = false;
		init();
		
	}
	
	public JCodeDecodeField(JTextFieldExt txtCodeValue,
		JComboBoxExt<CodeDataItem> cbDecodeDesc,String codeName,boolean isEmptyCode) {
        	super();
        	this.codeName = codeName;
        	this.txtCodeValue = txtCodeValue;
        	this.cbDecodeDesc = cbDecodeDesc;
        	this.isEmptyCode = isEmptyCode;
        	init();
      }
	
	
	
	
	public void init(){
		
		service = (StaticCodeDecodeService)ObjectFactory.getServiceObject(ObjectFactory.STATIC_CODE_DECODE_SERVICE);
		
		codeDataItemList = service.getCodeDataItemList(codeName, EN_US,this.isEmptyCode);
		
		DefaultComboBoxModel<CodeDataItem> model 
				= new DefaultComboBoxModel<CodeDataItem>(codeDataItemList);
		
		cbDecodeDesc.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
	                  JComboBoxExt localCombo = (JComboBoxExt)e.getSource();
	                  CodeDataItem item = (CodeDataItem)localCombo.getSelectedItem(); 
	                  
	                  if(!EMPTY_CODE_DECODE.getCodeValue().equals(item.getCodeValue())){
	                	    txtCodeValue.setDataValue(item.getCodeValue());
		  			  }else{
		  					txtCodeValue.setDataValue(EMPTY_CODE_DECODE.getCodeValue());
		  			  }
	            }  
				
			}
		});
		
		this.txtCodeValue.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				CodeDataItem item = getCodeDataItem(codeName,txtCodeValue.getDataValue());
				if(isEmptyCode && EMPTY_CODE_DECODE.equals(item)){
				  	CodeDataItem item1 = (CodeDataItem)cbDecodeDesc.getSelectedItem();
					txtCodeValue.setDataValue(item1.getCodeValue());
				}else{
					if(!EMPTY_CODE_DECODE.getCodeValue().equals(item.getCodeValue())){
    					cbDecodeDesc.setSelectedItem(item);
					}else{
    					cbDecodeDesc.setSelectedItem(EMPTY_CODE_DECODE);
    					txtCodeValue.setDataValue(EMPTY_CODE_DECODE.getCodeValue());
					}
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		this.txtCodeValue.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChanged() {
				
				CodeDataItem item = getCodeDataItem(codeName,txtCodeValue.getDataValue());
				
				if(isEmptyCode && EMPTY_CODE_DECODE.equals(item)){
				  	CodeDataItem item1 = (CodeDataItem)cbDecodeDesc.getSelectedItem();
					txtCodeValue.setDataValue(item1.getCodeValue());
				}else{
					if(!EMPTY_CODE_DECODE.getCodeValue().equals(item.getCodeValue())){
        					cbDecodeDesc.setSelectedItem(item);
        				}else{
        					cbDecodeDesc.setSelectedItem(EMPTY_CODE_DECODE);
        					txtCodeValue.setDataValue(EMPTY_CODE_DECODE.getCodeValue());
        				}
				}
			}
		});
		
		cbDecodeDesc.setModel(model);
		
		//cbDecodeDesc.setRenderer(new CodeDecodeComboBoxCellRenderer());
		
		
		if(this.isEmptyCode){
		    txtCodeValue.setDataValue(codeDataItemList.get(0).getCodeValue());
        	}else{
        	    txtCodeValue.setDataValue(EMPTY_CODE_DECODE.getCodeValue());
        	}
		
	}
	
	public CodeDataItem getCodeDataItem(String codeName,String codeValue){
		
		for(CodeDataItem codeData : codeDataItemList){
			if(codeData.getCodeValue().equals(codeValue)){
				return codeData;
			}
		}
		
		return EMPTY_CODE_DECODE;
		
	}
	
	
	/**
	 * Set as ReadOnly
	 * 
	 * @param isReadOnly
	 */
	public void setAsReadOnly(boolean isReadOnly){
		txtCodeValue.setEnabled(!isReadOnly);
		cbDecodeDesc.setEnabled(!isReadOnly);
	}

	@Override
	public void clearDataValue() {
		txtCodeValue.clearDataValue();
		cbDecodeDesc.clearDataValue();
	}

	@Override
	public void destroyComponent() {
		// TODO Auto-generated method stub
		txtCodeValue.destroyComponent();
		cbDecodeDesc.destroyComponent();

		cbDecodeDesc = null;
		txtCodeValue = null;
		service = null;
	}

	public List<CodeDataItem> getCodeDataItemList() {
		return codeDataItemList;
	}
	
	/**
	 * Return the code decode UI Panel.
	 * 
	 * @return
	 */
	public JPanel getCodeDecodeUIPanel(){
		JPanel panel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0,"[50:50,fill][fill,grow]", ""));
		if(txtCodeValue != null)
			panel.add(txtCodeValue);
		
		if(cbDecodeDesc != null)
			panel.add(cbDecodeDesc);
		
		return panel;
	}

}



/**
 * Custom comboxbox cell renderer
 * 
 * @author diwakarchoudhury
 *
 */
class CodeDecodeComboBoxCellRenderer extends BasicComboBoxRenderer{
	
	public CodeDecodeComboBoxCellRenderer() {
		super();
	}

	public Component getListCellRendererComponent(
			JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
		{
			super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

			if (value instanceof CodeDataItem)
			{
				CodeDataItem codeData = (CodeDataItem)value;
				setText( codeData.getCodeDescription() );
				
			}
			
			

			return this;
		}
}
