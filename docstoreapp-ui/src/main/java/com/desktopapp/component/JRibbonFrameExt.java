package com.desktopapp.component;

import java.awt.Component;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import org.apache.log4j.Logger;
import org.jvnet.flamingo.ribbon.JRibbonFrame;

import com.desktopapp.contoller.AppController;
import com.desktopapp.util.I18n;
import com.desktopapp.view.AppView;

public class JRibbonFrameExt extends JRibbonFrame {
    
    private final static Logger LOGGER = Logger
	    .getLogger(JRibbonFrameExt.class);
    
    protected boolean showExitDialog = false;
    

    public JRibbonFrameExt() throws HeadlessException {
	super();
	// TODO Auto-generated constructor stub
	init();
    }

    public JRibbonFrameExt(GraphicsConfiguration gc) {
	super(gc);
	// TODO Auto-generated constructor stub
	init();
    }

    public JRibbonFrameExt(String title, GraphicsConfiguration gc) {
	super(title, gc);
	// TODO Auto-generated constructor stub
	init();
    }

    public JRibbonFrameExt(String title) throws HeadlessException {
	super(title);
	// TODO Auto-generated constructor stub
	init();
    }
    
    public void addTaskbarComponent(Component comp,String functionName) {
	if(AppView.hasFunctionAccess(functionName)){
	    this.getRibbon().addTaskbarComponent(comp);	
	}
	
    }
    
    /**
     * Application exit
     * 
     * @see AppController#exit() 
     */
    protected void exitForm(WindowEvent evt) {
        if (showExitDialog) {
            if (MessageBox.showAskYesNo(I18n.COMMON.getString(
                    "MessageBox.Confirm.ProgramExit")) == MessageBox.NO_OPTION) {
                return;
            }
        }

        AppController.get().exit();
    }
    
    private void init() {
	
	addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                exitForm(e);
            }
        });
    }

   
}
