/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.component;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.desktopapp.contoller.AppUserLoginController;
import com.desktopapp.model.AppUserLogin;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.resource.util.ResourceUtil;
import com.desktopapp.service.util.PasswordHash;
import com.desktopapp.session.LoginSession;
import com.desktopapp.view.AppView;

/**
 * Desktop App Form Component.
 * 
 *
 * @author Ratnala Diwakar
 */
public class DesktopApp extends JFrame {

    /**
     * Title of Splash form.
     */
    private final String title;
    
    /**
     * Icon path of Splash form.
     */
    private final String iconPath;
    
    /**
     * Image path of Splash form.
     */
    private final String imagePath;
    
    private LoginPasswordDialog passDialog;
    

    /**
     * Create DesktopApp form
     * 
     * @param title title of splash form for taskbar
     * @param iconPath icon path for taskbar
     * @param imagePath image path for splash form
     */
    public DesktopApp(String title, String iconPath, String imagePath) {
        this.title = title;
        this.iconPath = iconPath;
        this.imagePath = imagePath;

        initComponents();
    }

    /**
     * init components
     */
    private void initComponents() {
        setTitle(title);
        
        URL url = getClass().getResource(iconPath);
        setIconImage(new ImageIcon(url).getImage());
        setResizable(false);
        setUndecorated(true);

        JLabel imgSplash = new JLabel(new ImageIcon(getClass().getResource(imagePath)));
        
        JProgressBar progressBar = new JProgressBar(0, 100);
        progressBar.setIndeterminate(true);
        progressBar.setPreferredSize(new Dimension(7, 7));
        progressBar.setBackground(new Color(165, 196, 238));
        progressBar.setForeground(new Color(243, 179, 69));

        getContentPane().add(imgSplash, BorderLayout.CENTER);
        getContentPane().add(progressBar, BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(null);
        
        passDialog = new LoginPasswordDialog(this, true);
        passDialog.setVisible(true);
        
    }

}
