package com.desktopapp.component.common;

/**
 * Generic component methods
 *  
 * @author diwakarchoudhury
 */
public interface IGenericComponent {
	public void setAsReadOnly(boolean readOnly);
	public void clearDataValue();
	public void destroyComponent();
	default public boolean validateInput(){
		return true;
	};
}
