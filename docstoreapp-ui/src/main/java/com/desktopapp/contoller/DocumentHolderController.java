package com.desktopapp.contoller;

import static com.desktopapp.service.QueryParameter.with;

import java.awt.Cursor;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import com.desktopapp.component.MessageBox;
import com.desktopapp.component.constants.DocStoreConstants;
import com.desktopapp.contoller.worker.BackgroundWorker;
import com.desktopapp.contoller.worker.executor.BackgroundWorkerExecutor;
import com.desktopapp.framework.AbstractDataPageController;
import com.desktopapp.framework.DataPageView;
import com.desktopapp.model.DocHolder;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.DocumentHolderService;
import com.desktopapp.util.I18n;
import com.desktopapp.view.AppView;
import com.desktopapp.view.DocumentHolderForm;
import com.desktopapp.view.DocumentHolderPage;

/**
 * Document Holder Controller
 *
 * @author Ratnala Diwakar Choudhury
 */
public class  DocumentHolderController extends AbstractDataPageController<DocHolder> {
	
    public final static Cursor BUSY_CURSOR = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
    public final static Cursor DEFAULT_CURSOR = Cursor.getDefaultCursor();
	
    @Override
    protected AbstractService<DocHolder> createService() {
        return (DocumentHolderService)ObjectFactory.getServiceObject(ObjectFactory.DOC_HOLDER_SERVICE);
    }

    @Override
    protected DataPageView<DocHolder> createDataPageView() {
        return new DocumentHolderPage();
    }

    @Override
    public void openFormView(DocHolder docHolder) {
        new DocumentHolderForm(this, docHolder,this.dataPageView.getFilterSearch()).showDialog();
    }

    
    @Override
    public void onAddNew() {
        openFormView(new DocHolder());
    }
    
    @Override
	public void onEdit() {
		super.onEdit();
	}
    
    
    @Override
    public void onDelete() {
        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }
        if (MessageBox.showAskYesNo(I18n.COMMON.getString(
                "MessageBox.Confirm.Delete")) == MessageBox.YES_OPTION) {
         	try {
	            //getService().remove(dataPageView.getSelectedModels());
         	    List<DocHolder> docHolderList = dataPageView.getSelectedModels();
 		    Map<String,Object> dataMap = new HashMap<String, Object>();
 	      	    dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentHolderService)getService());
 	      	    dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{ArrayList.class});
 	      	    dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new Object[]{docHolderList});
 	      	    dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "remove");
 	      	    dataMap.put(BackgroundWorker.REFRESH_NEEDED, "Y");
 	      	    dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Document Successfully Deleted");
 	      	    dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Deleting");
       	       	  
         	    BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap,this));
         	    taskExecutor.executeTask();
	           	//onRefresh();
            } catch (Exception e) {
                MessageBox.showError(I18n.COMMON.getString("Messages.Error.DeleteError"), e);
            }
        }
    }
    
    
  
    @Override
  	public void onBackup() {
  		
      	super.onBackup();
      	
      	final JFileChooser chooser = new JFileChooser(); 
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select directory to create backup");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        if (chooser.showOpenDialog(this.getDataPageView().asComponent()) == JFileChooser.APPROVE_OPTION) { 
            
        	  try{

        	      	  /*
        		  BackupBackgroundWorker backupBackGroupWorker = new BackupBackgroundWorker(this,(DocumentHolderService)getService(),chooser.getSelectedFile().getAbsolutePath());
        		 
        		  BackGroundTaskExecutor taskExecutor = new BackGroundTaskExecutor(backupBackGroupWorker);
        		  
        		  taskExecutor.executeTask();
        		 */
        	      	  Map<String,Object> dataMap = new HashMap<String, Object>();
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentHolderService)getService());
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{String.class});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new Object[]{chooser.getSelectedFile().getAbsolutePath()});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "backupDocumentHolder");
        	      	  dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Backup Successfully Completed");
        	      	  dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Backup");
              	       	  
        	      	  BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap));
        		  taskExecutor.executeTask();
          	}catch(Exception anyException){
          		MessageBox.showError("Error in backup ",anyException);
          	}
              
          }
          
         
     }
    
    @Override
  	public void onDownload() {
  		
      	super.onDownload();
      	
      	final JFileChooser chooser = new JFileChooser(); 
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select directory to download ");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        if (chooser.showOpenDialog(new JPanel()) == JFileChooser.APPROVE_OPTION) { 
            
        	  try{
        		  
        		  
        		  List<DocHolder> docHolderList = dataPageView.getSelectedModels();
        		  
        	      	  Map<String,Object> dataMap = new HashMap<String, Object>();
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentHolderService)getService());
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{String.class,ArrayList.class});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new Object[]{chooser.getSelectedFile().getAbsolutePath(),docHolderList});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "downloadDocHolders");
        	      	  dataMap.put(BackgroundWorker.REFRESH_NEEDED, "Y");
        	      	  dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Download Successfully Completed");
        	      	  dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Downloading");
              	       	  
        	      	  BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap,this));
        		  taskExecutor.executeTask();
        		 
        		  //BackGroundTaskExecutor taskExecutor = new BackGroundTaskExecutor(downloadBackGroupWorker);
        		  
        		  //taskExecutor.executeTask();
          			  		
          	}catch(Exception anyException){
          		MessageBox.showError("Error in download ",anyException);
          	}
              
          }
          
         
     }
    
    
  	public void onUpload() {
  		
      	
      	final JFileChooser chooser = new JFileChooser(); 
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select directory to scan and upload");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        if (chooser.showOpenDialog(this.getDataPageView().asComponent()) == JFileChooser.APPROVE_OPTION) { 
            
        	  try{
        		  //UploadBackgroundWorker uploadBackGroupWorker = new UploadBackgroundWorker((DocumentHolderService)getService(),chooser.getSelectedFile().getAbsolutePath());
        	      	  Map<String,Object> dataMap = new HashMap<String, Object>();
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentHolderService)getService());
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{String.class,String.class});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new String[]{chooser.getSelectedFile().getAbsolutePath(),AppView.getLoginSession().getUserName()});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "uploadFolder");
        	      	  
        	      	  dataMap.put(BackgroundWorker.REFRESH_NEEDED, "Y");
        	      	  dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Upload Successfully Completed");
        	      	  dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Uploading");
              	       	  //GenericBackgroundWorker uploadBackGroupWorker = new GenericBackgroundWorker(dataMap);
        	       	  BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap,this));
        		  taskExecutor.executeTask();
          			  		
          	}catch(Exception anyException){
          		MessageBox.showError("Error in worker ",anyException);
          	}
              
          }
          
         
     }
    
   
    @Override
	public void onRestore() {
		
		    super.onRestore();
		  
		    JFileChooser chooser = new JFileChooser(); 
	        chooser.setCurrentDirectory(new java.io.File("."));
	        chooser.setDialogTitle("Select document store backup file location to restore");
	        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        chooser.setAcceptAllFileFilterUsed(false);
	        
	          if (chooser.showOpenDialog(this.getDataPageView().asComponent()) == JFileChooser.APPROVE_OPTION) {   
	        	
	        	  String path = chooser.getSelectedFile().getAbsolutePath();
	        	
	        	  File backupFolerFile = new File(path + File.separator + DocStoreConstants.MAIN_BACKUP_FILE);
	      		
	    		  if(!backupFolerFile.exists()){
	    			  
	    			  MessageBox.showError("Invalid backup directory");
	    			  
	    			  return;
	    		  }
	    		  
	        	
				  if (MessageBox.showAskYesNo("Restore delete all existing documents, do you want to continue") == MessageBox.YES_OPTION) {
					  try{
						
					   	 //BackGroundTaskExecutor taskExecutor = new 	BackGroundTaskExecutor(new RestoreBackgroundWorker(this,(DocumentHolderService)getService(),chooser.getSelectedFile().getAbsolutePath()));
					      	  Map<String,Object> dataMap = new HashMap<String, Object>();
			        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentHolderService)getService());
			        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{String.class});
			        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new String[]{chooser.getSelectedFile().getAbsolutePath()});
			        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "restoreDocumentHolder");
			        	      	  
			        	      	  dataMap.put(BackgroundWorker.REFRESH_NEEDED, "Y");
			        	      	  dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Restore Successfully Completed");
			        	      	  dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Restoring");
			              	       	  BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap,this));
			        		  taskExecutor.executeTask();
			          			  		
        			          }catch(Exception anyException){
						  MessageBox.showError("Error in restoring ",anyException);
				      }
					
				  }
	        }
	}
	

	@Override
    public List<DocHolder> getData(String filter, int start, int end) {
        if (filter.equals("")) {
            return getService().getListWithNamedQuery(getNamedQuery(), start, end);
        } else {
            return getService().getListWithNamedQuery(getNamedQueryWithFilter(),
                    with("docOwner", "%" + filter + "%").
                    and("docCoowner", "%" + filter + "%").
                    and("docType", "%" + filter + "%").
                    and("docTypeOthers", "%" + filter + "%").
                    and("docName", "%" + filter + "%").
                    and("docKeywords", "%" + filter + "%").parameters(), start, end);
        }
    }

    @Override
    public int getDataSize(String filter) {
        if (filter.equals("")) {
            return getService().getListWithNamedQuery(getNamedQuery()).size();
        } else {
            return getService().getListWithNamedQuery(getNamedQueryWithFilter(),
            		with("docOwner", "%" + filter + "%").
                    and("docCoowner", "%" + filter + "%").
                    and("docType", "%" + filter + "%").
                    and("docTypeOthers", "%" + filter + "%").
                    and("docName", "%" + filter + "%").
                    and("docKeywords", "%" + filter + "%")
                    .parameters()).size();
        }
    }

    @Override
    public String getNamedQuery() {
        return DocHolder.FIND_ALL;
    }

    @Override
    public String getNamedQueryWithFilter() {
        return DocHolder.FIND_BY_DOC_LIKE_ALL;
    }

    @Override
    public String getName() {
        return "DocumentHolderController";
    }

   
}

