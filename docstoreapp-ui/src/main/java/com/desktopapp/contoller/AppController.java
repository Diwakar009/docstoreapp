/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.contoller;

import java.util.Locale;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.MetalLookAndFeel;

import org.apache.log4j.Logger;
import org.jvnet.lafwidget.LafWidget;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.api.SubstanceConstants.TabContentPaneBorderKind;
import org.jvnet.substance.skin.SubstanceMistAquaLookAndFeel;
import org.jvnet.substance.skin.SubstanceOfficeBlue2007LookAndFeel;

import com.desktopapp.StartDesktopApp;
import com.desktopapp.component.DesktopApp;
import com.desktopapp.component.MessageBox;
import com.desktopapp.framework.AbstractDataPageController;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.settings.IUISettings;
import com.desktopapp.util.I18n;
import com.desktopapp.util.JpaUtil;
import com.desktopapp.util.ViewHelpers;
import com.desktopapp.view.AppView;

/**
 * Application Controller
 *
 * @author Ratnala Diwakar
 */
public abstract class AppController {

   private final static Logger LOGGER = Logger.getLogger(AppController.class);
   
    protected static AppController controller;
    protected AppView appView;
    private DesktopApp desktopApp;
    private Locale appLocale;
    
    
    public AppController() {
    }

    /**
     * Start the application and show the application view.
     */
    public void start() throws Exception{
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        
        IUISettings uiSettings = AppView.getLoginSession().getUISettings();
       
        try {
                //UIManager.setLookAndFeel(new SubstanceOfficeSilver2007LookAndFeel());
            	/*
                Class<?> clazz = Class.forName(uiSettings.getUILookAndFeelPackage() + "." + uiSettings.getUILookAndFeel());
                SubstanceLookAndFeel substanceLookAndFeel = (SubstanceLookAndFeel)clazz.newInstance();
                UIManager.setLookAndFeel(substanceLookAndFeel);
                */
                //UIManager.setLookAndFeel(new SubstanceOfficeBlue2007LookAndFeel());
            	UIManager.setLookAndFeel(new SubstanceMistAquaLookAndFeel());
            
        } catch (UnsupportedLookAndFeelException ex) {
        	LOGGER.error("Unsupported Look and Feel Error ",ex);
                UIManager.setLookAndFeel(new SubstanceOfficeBlue2007LookAndFeel());
        }catch (Exception e) {
        	LOGGER.error("Substance Look and Feel Error ",e);
        	UIManager.setLookAndFeel(new SubstanceOfficeBlue2007LookAndFeel());
        }

        // TabbedPane border settings in substance look and feel. 
        UIManager.put(SubstanceLookAndFeel.TABBED_PANE_CONTENT_BORDER_KIND,
                TabContentPaneBorderKind.DOUBLE_PLACEMENT);

        // Cut, copy, paste menu in TextField with substance look and feel. 
        UIManager.put(LafWidget.TEXT_EDIT_CONTEXT_MENU, true);
        UIManager.put(LafWidget.TEXT_SELECT_ON_FOCUS, true);
        UIManager.put(LafWidget.TEXT_FLIP_SELECT_ON_ESCAPE, true);
        //UIManager.put(LafWidget.PASSWORD_STRENGTH_CHECKER, true);
         
        
        
        if (desktopApp != null) {
            desktopApp.setVisible(false);
            desktopApp.dispose();
            desktopApp = null;
        }

        appView = new AppView(I18n.DOCUMENTHOLDER.getString("App.Title"));
        appView.setVisible(true);
    }

    /**
     * Gets application controller.
     *
     * @return app controller
     */
    public static AppController get() {
        return controller;
    }

    /**
     * Gets application view.
     *
     * @return app view
     */
    public AppView getAppView() {
        return appView;
    }

    /**
     * Sets application splash form
     *
     * @param desktopApp splash form.
     */
    public void setDesktopApp(DesktopApp desktopApp) {
        this.desktopApp = desktopApp;
    }
    
    

    public DesktopApp getDesktopApp() {
		return desktopApp;
	}

	/**
     * Application exit.
     */
    public void exit() {
        JpaUtil.closeEntityManagerFactory();
        System.exit(0);
    }
    
    /**
     * Application logout.
     */
    public void logout() {
	
	nullifyDataViewObjects();
	getAppView().distroy();
	
	appView = null;
	
	try {
	    UIManager.setLookAndFeel(new MetalLookAndFeel());
	} catch (UnsupportedLookAndFeelException ex) {
	    LOGGER.error("Metal Look and Feel Error ", ex);
	}
	
	SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                controller = new StartDesktopApp();
	                DesktopApp desktopApp = new DesktopApp(I18n.DOCUMENTHOLDER.getString("App.Title"),
	                        ViewHelpers.ICONS16 + "documents.png",
	                        ViewHelpers.IMAGES + "splash.png");
	                desktopApp.setVisible(true);
	                
	                try {
	                    Thread.sleep(1000);
	                } catch (InterruptedException ex) {
	                    LOGGER.error(ex);
	                }
	                
	                controller.setDesktopApp(desktopApp);
	                try {
			    controller.start();
			} catch (Exception e) {
			    MessageBox.showError("Application Controller Initialization Error", e);
			    JpaUtil.closeEntityManagerFactory();
			    System.exit(0);
			}
	            }
	 });
	
    }

    private void nullifyDataViewObjects() {
	
	AbstractDataPageController<?> controller =(AbstractDataPageController<?>) ObjectFactory.getControllerObject(ObjectFactory.DOC_HOLDER_CTRL);
	controller.setDataPageView(null);
	
	controller =(AbstractDataPageController<?>) ObjectFactory.getControllerObject(ObjectFactory.APP_USER_CTRL);
	controller.setDataPageView(null);
	
	controller =(AbstractDataPageController<?>) ObjectFactory.getControllerObject(ObjectFactory.STATIC_CODE_DECODE_CTRL);
	controller.setDataPageView(null);
	
    }
    

}
