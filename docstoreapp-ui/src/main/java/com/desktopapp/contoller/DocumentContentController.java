package com.desktopapp.contoller;

import static com.desktopapp.service.QueryParameter.with;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTable;

import com.desktopapp.component.MessageBox;
import com.desktopapp.contoller.worker.BackgroundWorker;
import com.desktopapp.contoller.worker.executor.BackgroundWorkerExecutor;
import com.desktopapp.framework.AbstractDataPageController;
import com.desktopapp.framework.DataPageView;
import com.desktopapp.model.DocContent;
import com.desktopapp.model.DocContentBlob;
import com.desktopapp.model.DocHolder;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.DocumentContentService;
import com.desktopapp.service.common.IEmailGatewayService;
import com.desktopapp.service.common.impl.EmailGatewayServiceImpl;
import com.desktopapp.util.CommonFileUtil;
import com.desktopapp.util.I18n;
import com.desktopapp.util.UIFileUtil;
import com.desktopapp.view.AppView;
import com.desktopapp.view.DocumentContentForm;
import com.desktopapp.view.DocumentContentPage;
import com.desktopapp.view.DocumentHolderForm;
import com.desktopapp.view.DownloadDocument;
import com.desktopapp.view.SendEmailForm;

/**
 * Document Content Controller
 *
 * @author Ratnala Diwakar Choudhury
 */
public class DocumentContentController extends AbstractDataPageController<DocContent> {

	private DocHolder docHolder;
	private String searchFilter;
	private DocumentHolderForm documentHolderForm;
	private IEmailGatewayService emailGatewayService;
	
	
	public DocumentContentController() {
		super();
		init();
	}

	public DocumentContentController(DocHolder docHolder) {
		super();
		this.docHolder = docHolder;
		init();
	}
	
	public DocumentContentController(DocHolder docHolder,String searchFilter) {
		super();
		this.docHolder = docHolder;
		this.searchFilter = searchFilter;
		init();
		
	}
	
	void init(){
		emailGatewayService = (EmailGatewayServiceImpl)ObjectFactory.getServiceObject(ObjectFactory.EMAIL_GATEWAY_SERVICE);
	}

	@Override
    protected AbstractService<DocContent> createService() {
      return (DocumentContentService)ObjectFactory.getServiceObject(ObjectFactory.DOC_CONTENT_SERVICE);
    }

    @Override
    protected DataPageView<DocContent> createDataPageView() {
        return new DocumentContentPage(searchFilter);
    }

    @Override
    public DataPageView<DocContent> getDataPageView() {
        if (dataPageView == null) {
            dataPageView = createDataPageView();
            dataPageView.init(this);
            dataPageView.refreshDataWithFilter();
        } else {
            dataPageView.refreshData();
        }
        
        return dataPageView;
    }
    
    @Override
    public void openFormView(DocContent docContent) {
        new DocumentContentForm(this,docHolder, docContent).showDialog();
    }
    
    public void downloadDocument(DocContent docContent) {
        boolean isDocumentOpened = false;
        
        if (MessageBox.showAskYesNo("Do you want to open the document") == MessageBox.YES_OPTION) {
          	isDocumentOpened = new DownloadDocument(this,docHolder, docContent).openDocument();
        }
        
        if(!isDocumentOpened){
        	openFormView(docContent);
        }
        
    }
    
    @Override
    public void onDownload() {
	
	super.onDownload();
	
	final JFileChooser chooser = new JFileChooser(); 
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select directory to download ");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        if (chooser.showOpenDialog(new JPanel()) == JFileChooser.APPROVE_OPTION) { 
            
        	  try{
        		  
        		  
        		  List<DocContent> docContentList = dataPageView.getSelectedModels();
        		  
        		  //DownloadBackgroundWorker downloadBackGroupWorker = new DownloadBackgroundWorker(this,(DocumentHolderService)getService(),chooser.getSelectedFile().getAbsolutePath(),docHolderList);
        		  
        		  //BackGroundTaskExecutor taskExecutor = new 	BackGroundTaskExecutor(new RestoreBackgroundWorker(this,(DocumentHolderService)getService(),chooser.getSelectedFile().getAbsolutePath()));
		      	  Map<String,Object> dataMap = new HashMap<String, Object>();
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentContentService)getService());
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{String.class,ArrayList.class});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new Object[]{chooser.getSelectedFile().getAbsolutePath(),docContentList});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "downloadDocContents");
        	      	  dataMap.put(BackgroundWorker.REFRESH_NEEDED, "Y");
        	      	  dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Download Successfully Completed");
        	      	  dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Downloading");
              	       	  
        	      	  BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap,this));
        		  taskExecutor.executeTask();
        		 
        		  //BackGroundTaskExecutor taskExecutor = new BackGroundTaskExecutor(downloadBackGroupWorker);
        		  
        		  //taskExecutor.executeTask();
          			  		
          	}catch(Exception anyException){
          		MessageBox.showError("Error in download ",anyException);
          	}
              
          }
	
    }
    
    
    public void onUpload() {
  		
      	
      	final JFileChooser chooser = new JFileChooser(); 
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select directory to scan and upload");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        
        if (chooser.showOpenDialog(this.getDataPageView().asComponent()) == JFileChooser.APPROVE_OPTION) { 
            
        	  try{
        		  Map<String,Object> dataMap = new HashMap<String, Object>();
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentContentService)getService());
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{DocHolder.class,String.class,String.class});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new Object[]{this.docHolder,chooser.getSelectedFile().getAbsolutePath(),AppView.getLoginSession().getUserName()});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "uploadFolder");
        	      	  
        	      	  dataMap.put(BackgroundWorker.REFRESH_NEEDED, "Y");
        	      	  dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Upload Successfully Completed");
        	      	  dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Uploading");
              	       	  //GenericBackgroundWorker uploadBackGroupWorker = new GenericBackgroundWorker(dataMap);
        	       	  BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap,this));
        		  taskExecutor.executeTask();
          			  		
          	}catch(Exception anyException){
          		MessageBox.showError("Error in worker ",anyException);
          	}
              
          }
          
         
     }
    
    
    @Override
	public void sendEmail() {
    	
    	/*
    	if (MessageBox.showAskYesNo(I18n.COMMON.getString(
                  "MessageBox.Confirm.Email")) == MessageBox.YES_OPTION) {
            
        	  try{
        		  
        		  List<DocContent> docContentList = dataPageView.getSelectedModels();
        	  	  Map<String,Object> dataMap = new HashMap<String, Object>();
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentContentService)getService());
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{ArrayList.class});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new Object[]{docContentList});
        	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "emailAttachments");
        	      	  dataMap.put(BackgroundWorker.REFRESH_NEEDED, "N");
        	      	  dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Email Successfully Sent");
        	      	  dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Sending Email");
              	       	  
        	      	  BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap,this));
        	      	  
        	      	  taskExecutor.executeTask();
        	      	  
          	}catch(Exception anyException){
          		MessageBox.showError("Error in sending email ",anyException);
          	}
              
          }
          
         */
    	
    	 ArrayList<DocContent> docContentList = (ArrayList<DocContent> )dataPageView.getSelectedModels();
    	 new SendEmailForm(this,docHolder, docContentList).showDialog(); 
  
	}

	@Override
	public void print() {
		
		 if (MessageBox.showAskYesNo(I18n.COMMON.getString(
                 "MessageBox.Confirm.Print")) == MessageBox.YES_OPTION) {
           
       	  try{
       		  
       		  List<DocContent> docContentList = dataPageView.getSelectedModels();
       		  /*
	       	  	  Map<String,Object> dataMap = new HashMap<String, Object>();
	       	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentContentService)getService());
	       	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{ArrayList.class});
	       	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new Object[]{docContentList});
	       	      	  dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "printAttachments");
	       	      	  dataMap.put(BackgroundWorker.REFRESH_NEEDED, "N");
	       	      	  dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Print Successfully Sent");
	       	      	  dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Print");
	             	       	  
	       	      	  BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap,this));
	       	      	  
	       	      	  taskExecutor.executeTask();
          	*/
       		for(DocContent content : docContentList){
				List<DocContentBlob> contentBlob = content.getDocContentBlobs();
				String fileNameWithOutExt = CommonFileUtil.getFileNameWithoutExtention(content.getDocFileName());
				if(contentBlob != null){
					DocContentBlob blob = contentBlob.get(0);
					byte[] blobData = blob.getDocContentBlob();
					UIFileUtil.printByteDataAsFile(blobData,fileNameWithOutExt,content.getDocFileExtension());
					blob = null;
				}
			}
		 	}catch(Exception anyException){
         		MessageBox.showError("Error in print ",anyException);
         	}
                
         }
	}

	public void tableModelChanged(JXTable xtable){
	
	/*
	if(xtable.getModel().getRowCount() > 1){
	    
	}
	*/
	
    }
    
    
    @Override
    public void onAddNew() {
        openFormView(new DocContent());
    }

    @Override
    public List<DocContent> getData(String filter, int start, int end) {
        if (filter.equals("")) {
        	return getService().getListWithNamedQuery(getNamedQuery(),
                    with("docHolderId",  docHolder.getId() ).
                    parameters(), start, end);
        } else {
            return getService().getListWithNamedQuery(getNamedQueryWithFilter(),
            		with("docHolderId",  docHolder.getId()).
                    and("docContentName", "%" + filter + "%").
                    and("docContentKeywords", "%" + filter + "%").
                    parameters(), start, end);
        }
    }

    @Override
    public int getDataSize(String filter) {
        if (filter.equals("")) {
        	return getService().getListWithNamedQuery(getNamedQuery(),
                    with("docHolderId",  docHolder.getId() ).
                    parameters()).size();
        } else {
            return getService().getListWithNamedQuery(getNamedQueryWithFilter(),
            		with("docHolderId",  docHolder.getId()).
                    and("docContentName", "%" + filter + "%").
                    and("docContentKeywords", "%" + filter + "%").
                    parameters()).size();
        }
    }

    @Override
    public String getNamedQuery() {
        return DocContent.FIND_BY_DOCHOLDER_ID;
    }

    @Override
    public String getNamedQueryWithFilter() {
        return DocContent.FIND_BY_DOCCONTENT_LIKE_ALL;
    }
    
    @Override
	public void onMouseDoubleClickOnTable() {
		  if (dataPageView == null) {
              return;
          }
          if (dataPageView.getSelectedModel() == null) {
              return;
          }
          
           downloadDocument(dataPageView.getSelectedModel());
    }

	@Override
    public String getName() {
        return "DocumentContentController";
    }

	public DocHolder getDocHolder() {
		return docHolder;
	}

	public void setDocHolder(DocHolder docHolder) {
		this.docHolder = docHolder;
	}

	public String getSearchFilter() {
		return searchFilter;
	}

	public void setSearchFilter(String searchFilter) {
		this.searchFilter = searchFilter;
	}

	public DocumentHolderForm getDocumentHolderForm() {
	    return documentHolderForm;
	}

	public void setDocumentHolderForm(DocumentHolderForm documentHolderForm) {
	    this.documentHolderForm = documentHolderForm;
	}

	public IEmailGatewayService getEmailGatewayService() {
		return emailGatewayService;
	}

	public void setEmailGatewayService(IEmailGatewayService emailGatewayService) {
		this.emailGatewayService = emailGatewayService;
	}

	


}
