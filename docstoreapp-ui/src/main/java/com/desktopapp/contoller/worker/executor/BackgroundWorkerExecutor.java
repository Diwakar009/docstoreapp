package com.desktopapp.contoller.worker.executor;

import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JProgressBar;

import com.desktopapp.contoller.worker.BaseBackgroundWorker;


public class BackgroundWorkerExecutor {

	private BaseBackgroundWorker backGroundWorker;
	
	public BackgroundWorkerExecutor(BaseBackgroundWorker backGroundWorker) {
		super();
		this.backGroundWorker = backGroundWorker;
	}
	
	
	public void executeTask() throws Exception{
		
	    	JDialog progressBarDialog = new JDialog();
	    	progressBarDialog.setTitle("Please Wait...!!!");
	        progressBarDialog.setSize(200, 20);
	        progressBarDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	        progressBarDialog.setModalityType(ModalityType.APPLICATION_MODAL);
	        progressBarDialog.setLocationRelativeTo(null);
	        final JProgressBar progressBar = new JProgressBar(0,1);
	        progressBar.setValue(0);
	        progressBar.setIndeterminate(true);
	        progressBar.setPreferredSize(new Dimension(200, 10));
	        progressBarDialog.add(progressBar);
	        progressBarDialog.pack();
	        
	        PropertyChangeListener changeListener = new PropertyChangeListener() {
				
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					  if ("progress" == evt.getPropertyName() ) {
				            int progress = (Integer) evt.getNewValue();
				            progressBar.setValue(progress);
				            if(progress == 1){
				            	progressBarDialog.dispose();
				            }
				      }
					
				}
			};
			
		backGroundWorker.addPropertyChangeListener(changeListener);
		backGroundWorker.execute();
    		progressBarDialog.setVisible(true);
	}
	

	public BaseBackgroundWorker getBackGroundWorker() {
		return backGroundWorker;
	}

	public void setBackGroundWorker(BaseBackgroundWorker backGroundWorker) {
		this.backGroundWorker = backGroundWorker;
	}
	
	

}
