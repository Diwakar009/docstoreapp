package com.desktopapp.contoller.worker;

import java.awt.Toolkit;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingWorker;

import org.apache.log4j.Logger;

import com.desktopapp.framework.DataPageController;
import com.desktopapp.model.BaseEntity;

public abstract class BaseBackgroundWorker extends SwingWorker<Void, Void> {
	
	private final static Logger LOGGER = Logger
			.getLogger(BaseBackgroundWorker.class);
	

	private DataPageController dataPageController;
	
	public BaseBackgroundWorker(
			DataPageController dataPageController) {
		super();
		this.dataPageController = dataPageController;
	}

	@Override
	protected Void doInBackground() throws Exception {
	    	
		try{
			setProgress(0);
			
			doProcess();
			
		}finally{
			setProgress(1);
		}
		
		return null;
	}

	@Override
	protected void done() {
		super.done();
		Toolkit.getDefaultToolkit().beep();

		try {
	    
	        get();
	        
	        handleSuccess();
	        
		} catch (Exception e) {
			handleError(e);
		}
		
	}
	
	public abstract void doProcess() throws Exception;
	
	public void handleSuccess(){}
	
	public void handleError(Exception e){}

	public DataPageController getDataPageController() {
		return dataPageController;
	}

	public void setDataPageController(DataPageController dataPageController) {
		this.dataPageController = dataPageController;
	}

	
	
	
	

}




	