package com.desktopapp.contoller.worker;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.pattern.LogEvent;

import com.desktopapp.component.MessageBox;
import com.desktopapp.framework.DataPageController;
import com.desktopapp.model.BaseEntity;
import com.desktopapp.service.DocumentHolderService;

public class BackgroundWorker extends BaseBackgroundWorker {
    
    	public static final String WORKER_SERVICE_OBJECT = "WORKER_SERVICE_OBJECT";
    	public static final String WORKER_SERVICE_METHOD_NAME = "WORKER_SERVICE_METHOD_NAME";
    	public static final String WORKER_SERVICE_PARAM_CLASS_TYPES = "WORKER_SERVICE_PARAM_CLASS_TYPES";
    	public static final String WORKER_SERVICE_PARAM_OBJECTS = "WORKER_SERVICE_PARAM_OBJECTS";
    	
    	public static final String REFRESH_NEEDED = "REFRESH_NEEDED";
    	public static final String SUCCESS_MESSAGE = "SUCCESS_MESSAGE";
    	public static final String ERROR_MESSAGE = "ERROR_MESSAGE";
    	
    	
    	private final static Logger LOGGER = Logger.getLogger(BackgroundWorker.class);


	private Map<String,Object> workerDataMap = new HashMap<String, Object>();
	
    	
	public BackgroundWorker(Map<String,Object> dataMap) {
		super(null);
		
		setWorkerDataMap(dataMap);
	}
	
	public BackgroundWorker(Map<String,Object> dataMap,DataPageController dataPageController) {
		super(dataPageController);
		
		setWorkerDataMap(dataMap);
	}
	

	@Override
	public void doProcess() throws Exception{
	    
	    Object object = getWorkerDataMap().get(WORKER_SERVICE_OBJECT);
	    String methodName = (String)getWorkerDataMap().get(WORKER_SERVICE_METHOD_NAME);
	    Class[] paramTypes = (Class[])getWorkerDataMap().get(WORKER_SERVICE_PARAM_CLASS_TYPES);
	    Object[] paramObjects = (Object[])getWorkerDataMap().get(WORKER_SERVICE_PARAM_OBJECTS);
	    
	    try{
		
		Method method = object.getClass().getMethod(methodName, paramTypes);
		method.invoke(object, paramObjects);
		
	    } catch (SecurityException e) {
		throw e;
	    } catch (NoSuchMethodException e1) {
		throw e1;
	    }catch (Exception anyException){
	    	anyException.printStackTrace();
		throw anyException;
	    }
	    
	}
	
	@Override
	public void handleSuccess() {
		super.handleSuccess();
		
		String doRefreshNeeded = (String)getWorkerDataMap().get(REFRESH_NEEDED);
		if("Y".equals(doRefreshNeeded)){
		    if(getDataPageController() != null){
			getDataPageController().onRefresh();
		    }
		}
		
		String successMessage = (String)getWorkerDataMap().get(SUCCESS_MESSAGE);
		if(successMessage != null){
			MessageBox.showInfo(successMessage);
		}else{
		    MessageBox.showInfo("Process Sucessfully Completed");
		}
	}

	@Override
	public void handleError(Exception e) {
		super.handleError(e);
		
		String errorMessage = (String)getWorkerDataMap().get(ERROR_MESSAGE);
		if(errorMessage != null){
		    MessageBox.showError(errorMessage,e);
		}else{
		    MessageBox.showError("Error in process " , e);
		}
	}
	
	public Map<String, Object> getWorkerDataMap() {
	    return workerDataMap;
	}

	public void setWorkerDataMap(Map<String, Object> workerDataMap) {
	    this.workerDataMap = workerDataMap;
	}

}
