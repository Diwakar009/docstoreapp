package com.desktopapp.contoller;

import static com.desktopapp.service.QueryParameter.with;

import java.util.List;

import com.desktopapp.framework.AbstractDataPageController;
import com.desktopapp.framework.DataPageView;
import com.desktopapp.model.AppUser;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.service.AbstractService;
import com.desktopapp.view.AppUserForm;
import com.desktopapp.view.AppUserPage;

/**
 * AppUser Controller
 *
 * @author Ratnala Diwakar Choudhury
 */
public class AppUserController extends AbstractDataPageController<AppUser> {

    @Override
    protected AbstractService<AppUser> createService() {
        return (AbstractService<AppUser>)ObjectFactory.getControllerObject(ObjectFactory.APP_USER_SERVICE);
    }

    @Override
    protected DataPageView<AppUser> createDataPageView() {
        return new AppUserPage();
    }

    @Override
    public void openFormView(AppUser AppUser) {
        new AppUserForm(this, AppUser).showDialog();
    }

    @Override
    public void onAddNew() {
        AppUser AppUser = new AppUser();
        AppUser.setActive(true);
        openFormView(AppUser);
    }

   
    @Override
    public List<AppUser> getData(String filter, int start, int end) {
        if (filter.equals("")) {
            return getService().getListWithNamedQuery(getNamedQuery(), start, end);
        } else {
            return getService().getListWithNamedQuery(getNamedQueryWithFilter(),
                    with("firstName", "%" + filter + "%").
                    and("lastName", "%" + filter + "%").
                    and("shortName", "%" + filter + "%").
                    and("phone", "%" + filter + "%").
                    and("email", "%" + filter + "%").
                    and("userName", "%" + filter + "%").parameters(), start, end);
        }
    }

    @Override
    public int getDataSize(String filter) {
        if (filter.equals("")) {
            return getService().getListWithNamedQuery(getNamedQuery()).size();
        } else {
            return getService().getListWithNamedQuery(getNamedQueryWithFilter(),
            		 with("firstName", "%" + filter + "%").
                     and("lastName", "%" + filter + "%").
                     and("shortName", "%" + filter + "%").
                     and("phone", "%" + filter + "%").
                     and("email", "%" + filter + "%").
                     and("userName", "%" + filter + "%")
                    .parameters()).size();
        }
    }

    @Override
    public String getNamedQuery() {
        return AppUser.FIND_ALL;
    }

    @Override
    public String getNamedQueryWithFilter() {
        return AppUser.FIND_BY_USER_LIKE_ALL;
    }

    @Override
    public String getName() {
        return "AppUserController";
    }

}
