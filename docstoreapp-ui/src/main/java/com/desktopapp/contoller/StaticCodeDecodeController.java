package com.desktopapp.contoller;

import static com.desktopapp.service.QueryParameter.with;

import java.util.List;

import com.desktopapp.framework.AbstractDataPageController;
import com.desktopapp.framework.DataPageView;
import com.desktopapp.model.DocHolder;
import com.desktopapp.model.StaticCodeDecode;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.StaticCodeDecodeService;
import com.desktopapp.spring.util.ApplicationContextUtils;
import com.desktopapp.view.StaticCodeDecodeForm;
import com.desktopapp.view.StaticCodeDecodePage;

/**
 * Static code decode Controller
 *
 * @author Ratnala Diwakar Choudhury
 */
public class StaticCodeDecodeController extends AbstractDataPageController<StaticCodeDecode> {

    @Override
    protected AbstractService<StaticCodeDecode> createService() {
        //return new StaticCodeDecodeService();
    	return (StaticCodeDecodeService)ObjectFactory.getServiceObject(ObjectFactory.STATIC_CODE_DECODE_SERVICE);
    }

    @Override
    protected DataPageView<StaticCodeDecode> createDataPageView() {
        return new StaticCodeDecodePage();
    }

    @Override
    public void openFormView(StaticCodeDecode staticCodeDecode) {
        new StaticCodeDecodeForm(this, staticCodeDecode).showDialog();
    }

    @Override
    public void onAddNew() {
        openFormView(new StaticCodeDecode());
    }

    @Override
    public List<StaticCodeDecode> getData(String filter, int start, int end) {
        if (filter.equals("")) {
            return getService().getListWithNamedQuery(getNamedQuery(), start, end);
        } else {
            return getService().getListWithNamedQuery(getNamedQueryWithFilter(),
            		 with("codeName", "%" + filter + "%").
                     and("codeValue", "%" + filter + "%").
                     and("codeDesc", "%" + filter + "%").parameters(), start, end);
        }
    }

    @Override
    public int getDataSize(String filter) {
        if (filter.equals("")) {
            return getService().getListWithNamedQuery(getNamedQuery()).size();
        } else {
            return getService().getListWithNamedQuery(getNamedQueryWithFilter(),
                    with("codeName", "%" + filter + "%").
                    and("codeValue", "%" + filter + "%").
                    and("codeDesc", "%" + filter + "%").parameters()).size();
        }
    }

    @Override
    public String getNamedQuery() {
        return StaticCodeDecode.FIND_ALL;
    }

    @Override
    public String getNamedQueryWithFilter() {
        return StaticCodeDecode.FIND_BY_CODE_NAME_OR_CODE_VAUE_OR_CODE_DESC;
    }

    @Override
    public String getName() {
        return "StaticCodeDecodeController";
    }

}
