package com.desktopapp.contoller;

import static com.desktopapp.service.QueryParameter.with;

import java.util.List;

import com.desktopapp.framework.AbstractDataPageController;
import com.desktopapp.framework.DataPageView;
import com.desktopapp.model.AppUser;
import com.desktopapp.model.AppUserLogin;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.service.AbstractService;
import com.desktopapp.service.AppUserLoginService;
import com.desktopapp.service.FunctionAccessService;
import com.desktopapp.service.UISettingsService;
import com.desktopapp.settings.IUISettings;
import com.desktopapp.settings.impl.UISettingsImpl;
import com.desktopapp.view.AppUserLoginForm;
import com.desktopapp.view.AppUserLoginPage;

/**
 * App user login controller
 *
 * @author Ratnala Diwakar Choudhury
 */
public class AppUserLoginController extends
	AbstractDataPageController<AppUserLogin> {

    private AppUser appUser;
    private String searchFilter;

    public AppUserLoginController() {
	super();
    }

    public AppUserLoginController(AppUser appUser) {
	super();
	this.appUser = appUser;
    }

    public AppUserLoginController(AppUser appUser, String searchFilter) {
	super();
	this.appUser = appUser;
	this.searchFilter = searchFilter;
    }

    @Override
    protected AbstractService<AppUserLogin> createService() {
	return (AppUserLoginService) ObjectFactory
		.getServiceObject(ObjectFactory.APP_USER_LOGIN_SERVICE);
    }

    @Override
    protected DataPageView<AppUserLogin> createDataPageView() {
	return new AppUserLoginPage(searchFilter);
    }

    @Override
    public DataPageView<AppUserLogin> getDataPageView() {
	if (dataPageView == null) {
	    dataPageView = createDataPageView();
	    dataPageView.init(this);
	    dataPageView.refreshDataWithFilter();
	} else {
	    dataPageView.refreshData();
	}

	return dataPageView;
    }

    @Override
    public void openFormView(AppUserLogin appUserLogin) {
	new AppUserLoginForm(this, appUser, appUserLogin).showDialog();
    }

    @Override
    public void onAddNew() {
	openFormView(new AppUserLogin());
    }

    @Override
    public List<AppUserLogin> getData(String filter, int start, int end) {
	if (filter.equals("")) {
	    return getService()
		    .getListWithNamedQuery(getNamedQuery(),
			    with("appUserId", appUser.getId()).parameters(),
			    start, end);
	} else {
	    return getService().getListWithNamedQuery(
		    getNamedQueryWithFilter(),
		    with("appUserId", appUser.getId()).and("userName",
			    "%" + filter + "%").parameters(), start, end);
	}
    }

    @Override
    public int getDataSize(String filter) {
	if (filter.equals("")) {
	    return getService().getListWithNamedQuery(getNamedQuery(),
		    with("appUserId", appUser.getId()).parameters()).size();
	} else {
	    return getService().getListWithNamedQuery(
		    getNamedQueryWithFilter(),
		    with("appUserId", appUser.getId()).and("userName",
			    "%" + filter + "%").parameters()).size();
	}
    }
    
    public List<String> getFunctionAccessList(String accessLevel) {
	FunctionAccessService functionAccessService =  (FunctionAccessService) ObjectFactory.getServiceObject(ObjectFactory.APP_USER_FUNCTION_ACC_SERVICE);
	
	return functionAccessService.getFunctionAccessList(accessLevel);
    }
    
    public IUISettings getUISettings(){
	  UISettingsService uiSettingService =  (UISettingsService) ObjectFactory.getServiceObject(ObjectFactory.APP_USER_UI_SETTINGS_SERVICE);
	  IUISettings uiSettings = new UISettingsImpl(uiSettingService.getUISettings());
	 
	  return uiSettings;
    }
    
    @Override
    public String getNamedQuery() {
	return AppUserLogin.FIND_BY_APPUSER_ID;
    }

    @Override
    public String getNamedQueryWithFilter() {
	return AppUserLogin.FIND_BY_APPUSERLOGIN_LIKE_ALL;
    }

    @Override
    public void onMouseDoubleClickOnTable() {
	if (dataPageView == null) {
	    return;
	}
	if (dataPageView.getSelectedModel() == null) {
	    return;
	}

	openFormView(dataPageView.getSelectedModel());

    }

    @Override
    public String getName() {
	return "AppUserLoginController";
    }

    public AppUserLogin getAppUserLoginByUserName(String userName) {
	return ((AppUserLoginService) getService())
		.getAppUserLoginByUserName(userName);
    }

    public AppUser getAppUser() {
	return appUser;
    }

    public void setAppUser(AppUser appUser) {
	this.appUser = appUser;
    }

    public String getSearchFilter() {
	return searchFilter;
    }

    public void setSearchFilter(String searchFilter) {
	this.searchFilter = searchFilter;
    }

}
