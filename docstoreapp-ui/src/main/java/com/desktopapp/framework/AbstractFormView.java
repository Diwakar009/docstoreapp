/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.framework;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.jdesktop.swingx.JXStatusBar;

import com.desktopapp.component.JMenuBarExt;
import com.desktopapp.component.JMenuExt;
import com.desktopapp.component.JToolBarExt;
import com.desktopapp.component.common.IGenericComponent;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.contoller.AppController;
import com.desktopapp.model.BaseEntity;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * Abstract Form View for data editing in JDialog.
 *
 * @param <T> entity
 *
 * @author Diwakar Choudhury
 */
public abstract class AbstractFormView<T extends BaseEntity> extends JDialog {

    protected DataPageController<T> controller;
    protected JMenuBarExt menuBar;
    protected JToolBarExt toolBar;
    protected JTabbedPane tpPages;
    protected JXStatusBar xstatusBar;
    
    // menu and toolbar actions
    protected Action acSave;
    protected Action acPrintPreview;
    protected Action acPrint;
    protected Action acClose;
    protected Action acHelp;
    protected List<IGenericComponent> genericComponentList;
    private JPanel canvasMainPanel;
    
    private KeyEventDispatcher formKeyEventDispatcher;
    
    
    /**
     * Creates form view
     *
     * @param parent parent view
     * @param controller form's controller
     */
    public AbstractFormView(JFrame parent, DataPageController<T> controller) {
        super(parent);
        this.controller = controller;
        genericComponentList = new ArrayList<IGenericComponent>();
    }

    /**
     * init components
     */
    public void initComponents() {
    	
    	setLayout(new BorderLayout());
    	setIconImage(new ImageIcon(getClass().getResource(getFormIconPath())).getImage());
        setTitle(getFormTitle());
        buildFormActions();
        setJMenuBar(buildMenuBar());
        getContentPane().add(buildToolBar(), BorderLayout.NORTH);
        JPanel canvasRootPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
        
        
        formKeyEventDispatcher = new KeyEventDispatcher() {
            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
              
              switch(e.getKeyCode()){
              case KeyEvent.VK_ESCAPE:
        	  onCloseForm();
                  return true;
            	            	  
              default:
            	  return false;
              
              }
			  
            }
        };
    
       KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(formKeyEventDispatcher);
        
        canvasMainPanel = new JPanel();
        canvasMainPanel.setLayout(new BoxLayout(canvasMainPanel,BoxLayout.Y_AXIS));
    	
        if (isMultiPageForm()) {
            tpPages = new JTabbedPane();
            tpPages.setFocusable(false);
            canvasMainPanel.add(tpPages, BorderLayout.CENTER);
        }
        
        canvasRootPanel.add(canvasMainPanel);
      
        getContentPane().add(canvasRootPanel, BorderLayout.CENTER);
        getContentPane().add(buildStatusBar(), BorderLayout.SOUTH);
    	
        addMorePanels();
       
    }

  	protected void addMorePanels() {
		// TODO Auto-generated method stub
		
	}

	/**
     * Gets form view's title
     *
     * @return form view's title
     */
    public abstract String getFormTitle();

    /**
     * Gets form view's icon path
     *
     * @return form view's icon path
     */
    public abstract String getFormIconPath();

    /**
     * Builds form view components.
     * Call first initComponents() in this method.
     */
    public abstract void buildUI();

    /**
     * Pop model values to UI
     */
    public abstract void popFields();

    /**
     * Push UI values to model
     */
    public abstract void pushFields();

    /**
     * Save data of form view
     *
     * @return true if save is ok
     */
    public boolean onSave() {
        if (!validateForm()) {
            return false;
        }

        pushFields();
        controller.onSave(getEntity());

        return true;
    }

    /**
     * Validate fields before save
     *
     * @return
     */
    public boolean validateForm() {
        return true;
    }

    /**
     * Gets entity object
     *
     * @return entity
     */
    public abstract T getEntity();

    /**
     * Adds new page to form view.
     *
     * @param title page's title
     * @param page page panel
     */
    public void addPageToForm(String title, JPanel page) {
        if (isMultiPageForm()) {
            tpPages.add(title, page);
        } else {
            //getContentPane().add(page, BorderLayout.CENTER);
        	canvasMainPanel.add(page, BorderLayout.CENTER);
        	
        }
    }

    /**
     * Has this form multi pages?
     *
     * @return true if this form has multi pages.
     */
    public boolean isMultiPageForm() {
        return false;
    }

    /**
     * Is this form view printable?
     *
     * @return true if this form has print actions.
     */
    public boolean isPrintable() {
        return false;
    }

    /**
     * Print preview
     */
    public void onPrintPreview() {
    }

    /**
     * Print
     */
    public void onPrint() {
    }

    /**
     * Help
     */
    public abstract void onHelp();
    
    /**
     * Disable and Enable Fields
     */
    
    public abstract void disableFields();
    

    /**
     * Show dialog
     */
    public void showDialog() {
        buildUI();
        setLocationRelativeTo(AppController.get().getAppView());
        setModalityType(ModalityType.APPLICATION_MODAL);
        disableFields();
        setVisible(true);
    }

    /**
     * Form view close
     */
    public void onCloseForm() {
	KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(formKeyEventDispatcher);
    	KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
    	formKeyEventDispatcher = null;
    	setVisible(false);
    	destroy();
        dispose();
    }

    /**
     * Builds form view's menu bar
     *
     * @return menu bar
     */
    private JMenuBarExt buildMenuBar() {
        menuBar = new JMenuBarExt();

        // File menu
        JMenuExt mnuFile = new JMenuExt(I18n.COMMON.getString("AbstractFormView.Menu.File"));
        mnuFile.add(acSave,getAccessFunctionName() + FunctionAccessConstants.SAVE);
        mnuFile.add(new JSeparator());
        if (isPrintable()) {
            mnuFile.add(acPrintPreview);
            mnuFile.add(acPrint);
            mnuFile.add(new JSeparator());
        }
        mnuFile.add(acClose);
        menuBar.add(mnuFile);

        // Help menu
        JMenu mnuHelp = new JMenu(I18n.COMMON.getString("AbstractFormView.Menu.Help"));
        mnuHelp.add(acHelp);
        menuBar.add(mnuHelp);

        return menuBar;
    }

    /**
     * Builds form view's toolbar
     *
     * @return toolBar action toolbar
     */
    private JToolBar buildToolBar() {
        toolBar = new JToolBarExt();
        toolBar.setRollover(true);

        toolBar.addToolButton(ViewHelpers.createToolButton(acSave, true, true),getAccessFunctionName() + FunctionAccessConstants.SAVE);
        if (isPrintable()) {
            toolBar.add(ViewHelpers.createToolButton(acPrintPreview, true, true));
            toolBar.add(ViewHelpers.createToolButton(acPrint, true, true));
        }
        toolBar.add(ViewHelpers.createToolButton(acHelp, true, true));
        toolBar.add(ViewHelpers.createToolButton(acClose, true, true));

        return toolBar;
    }

    /**
     * Builds form view actions
     */
    private void buildFormActions() {
        acSave = new AbstractAction(I18n.COMMON.getString("Action.Save"),
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "save.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        if (onSave()) {
                            controller.onRefresh();
                            onCloseForm();
                        }
                    }
                };
        acSave.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Save"));
        acSave.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_S));
        acSave.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));

        acPrintPreview = new AbstractAction(I18n.COMMON.getString("Action.PrintPreview"),
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "printpreview.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        onPrintPreview();
                    }
                };
        acPrintPreview.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.PrintPreview"));
        acPrintPreview.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_V));
        acPrintPreview.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));

        acPrint = new AbstractAction(I18n.COMMON.getString("Action.Print"),
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "print.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        onPrint();
                    }
                };
        acPrint.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Print"));
        acPrint.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_D));
        acPrint.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));

        acHelp = new AbstractAction(I18n.COMMON.getString("Action.Help"),
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "help.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        onHelp();
                    }
                };
        acHelp.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Help"));
        acHelp.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_F1));
        acHelp.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));

        acClose = new AbstractAction(I18n.COMMON.getString("Action.Close"),
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "close.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        controller.onRefresh();
                        onCloseForm();
                    }
                };
        acClose.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Close"));
        acClose.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_X));
        acClose.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
    }

    /**
     * Builds form view's status bar.
     *
     * @return xstatusBar
     */
    private JXStatusBar buildStatusBar() {
        xstatusBar = new JXStatusBar();
        xstatusBar.setPreferredSize(new Dimension(15, 20));

        return xstatusBar;
    }

	public JPanel getCanvasMainPanel() {
		return canvasMainPanel;
	}

   public abstract String getAccessFunctionName(); 
   
   
   public void destroy(){
       
       for(IGenericComponent iGenericComponent : genericComponentList) {
		if(iGenericComponent != null){
		    iGenericComponent.destroyComponent();
		}
       }
       
       menuBar = null;
       toolBar= null;
       tpPages= null;
       xstatusBar= null;
       formKeyEventDispatcher = null;
       acSave = null;
       acPrintPreview = null;;
       acPrint = null;
       acClose= null;
       acHelp= null;
       canvasMainPanel = null;
       genericComponentList = null;
   }
    

}
