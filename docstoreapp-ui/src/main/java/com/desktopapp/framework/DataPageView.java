/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.framework;

import java.util.List;

import com.desktopapp.model.BaseEntity;

/**
 * Data Page View interface
 *
 * @param <T> entity
 *
 * @author Ratnala Diwakar Choudhury
 */
public interface DataPageView<T extends BaseEntity> extends View {

    /**
     * Data page view init
     *
     * @param controller data page view's controller
     */
    void init(DataPageController<T> controller);

    /**
     * Gets data page controller
     *
     * @return data page controller controller
     */
    DataPageController<T> getController();

    /**
     * Gets selected data model of data page view
     *
     * @return selected entity model
     */
    T getSelectedModel();
    
    /**
     * Gets selected data model of data page view
     *
     * @return selected entitys model
     */
    List<T> getSelectedModels();

    /**
     * Refresh data of data page view
     */
    void refreshData();
    
    String getFilterSearch();
    
    void refreshDataWithFilter();
    
    void tableModelChanged();

}
