package com.desktopapp.framework;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import net.miginfocom.swing.MigLayout;

import com.desktopapp.component.JComboBoxExt;
import com.desktopapp.component.JTextFieldExt;
import com.desktopapp.component.common.CodeDataItem;
import com.desktopapp.component.common.IGenericComponent;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.component.listeners.ValueChangeListener;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.service.StaticCodeDecodeService;

/**
 * Code Decode Field
 * 
 * @author diwakarchoudhury
 *
 */
public class TableColumnCodeDecode {
	
	private String codeName = "";
	
    
    private Vector<CodeDataItem> codeDataItemList = new Vector<CodeDataItem>();
    
    private StaticCodeDecodeService service = null;
    
    private CodeDataItem EMPTY_CODE_DECODE = new CodeDataItem("","","");
    
    private final String EN_US = "EN_US";
    
   
    
    /**
     * Constructor
     * 
     * @param txtCodeValue
     * @param cbDecodeDesc
     * @param codeName
     */
	public TableColumnCodeDecode(String codeName) {
		super();
		this.codeName = codeName;
		
		init();
		
	}
	
	public void init(){
		
	        service = (StaticCodeDecodeService)ObjectFactory.getServiceObject(ObjectFactory.STATIC_CODE_DECODE_SERVICE);
		
		codeDataItemList = service.getCodeDataItemList(codeName, EN_US);
		
	}
	
	private CodeDataItem getCodeDataItem(String codeName,String codeValue){
		
		for(CodeDataItem codeData : codeDataItemList){
			if(codeData.getCodeValue().equals(codeValue)){
				return codeData;
			}
		}
		
		return EMPTY_CODE_DECODE;
		
	}
	
	public String getCodeDescription(String codeValue)
	{
		CodeDataItem item = getCodeDataItem(codeName, codeValue);
		
		if(item != null){
			return item.getCodeDescription();
		}
		
	
		return codeValue;
		
	}
	

	public List<CodeDataItem> getCodeDataItemList() {
		return codeDataItemList;
	}
	
	
}



