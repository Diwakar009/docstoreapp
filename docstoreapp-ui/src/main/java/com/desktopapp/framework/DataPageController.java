/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */

package com.desktopapp.framework;

import java.util.List;

import com.desktopapp.model.BaseEntity;
import com.desktopapp.service.AbstractService;

/**
 * Data Page Controller interface.
 * 
 * @param <T> entity
 * 
 * @author Cem Ikta
 */
public interface DataPageController<T extends BaseEntity> extends Controller {
    
    /**
     * Gets service of this controller
     * 
     * @return service  
     */
    AbstractService<T> getService();
    
    /**
     * Gets data page view
     * 
     * @return data page view 
     */
    DataPageView<T> getDataPageView();
    
    /**
     * Open form view
     * 
     * @param entity form view's entity object 
     */
    void openFormView(T entity);
    
    /**
     * Add new action 
     */
    void onAddNew();
    
    /**
     * Edit action
     */
    void onEdit();
    
    /**
     * Delete action
     */
    void onDelete();
    
    /**
     * Refresh action
     */
    void onRefresh();
    
    /**
     * Save action
     * 
     * @param entity to save  
     */
    void onSave(T entity);
    
    /**
     * Save multiple entities
     * 
     * @param entity to save  
     */
    void onSave(List<T> entities);
    
    
    /**
     * Gets data list for JXTable
     * 
     * @param filter filter for data list
     * @param start start for paging
     * @param end end for paging
     * @return data list
     */
    List<T> getData(String filter, int start, int end);
    
    /**
     * Gets data size
     * 
     * @param filter filter for data list
     * @return data record size
     */
    int getDataSize(String filter);
    
    /**
     * Gets named query
     * 
     * @return named query in entity 
     */
    String getNamedQuery();
    
    /**
     * Gets named query with filter
     * 
     * @return named query with filter in entity 
     */
    String getNamedQueryWithFilter();
    
 
    /**
     * On doubleclick on table row
     * 
     */
    void onMouseDoubleClickOnTable();
    
    
    /**
     * Implementation of the back up data.
     * 
     */
    void onBackup();
    
    /**
     * Implementation of syncing back the back up.
     * 
     */
    void onRestore();
    
    
    /**
     * Implementation of ondownload.
     * 
     */
    void onDownload();
    
    /**
     * Implementation of sendEmail
     * 
     */
    void sendEmail();
    
    /**
     * Implementation of sendEmail
     * 
     */
    void print();
    
    
    
    
}
