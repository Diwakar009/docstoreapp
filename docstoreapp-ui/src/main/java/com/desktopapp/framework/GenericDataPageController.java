package com.desktopapp.framework;

import java.util.List;

import org.apache.log4j.Logger;

import com.desktopapp.component.MessageBox;
import com.desktopapp.model.BaseEntity;
import com.desktopapp.service.AbstractService;
import com.desktopapp.util.I18n;
import com.desktopapp.view.AppView;

/**
 * Abstract Data Page Controller.
 *
 * @param <T> entity
 *
 * @author Ratnala Diwakar Choudhury
 */
public abstract class GenericDataPageController
        implements DataPageController {

    private final static Logger LOGGER = Logger
	    .getLogger(GenericDataPageController.class);
    
    protected AbstractService service;
    protected DataPageView dataPageView;

    public GenericDataPageController() {
    }

    protected abstract AbstractService createService();

    @Override
    public AbstractService getService() {
        if (service == null) {
            service = createService();
        }
        return service;
    }

    protected abstract DataPageView createDataPageView();

    @Override
    public DataPageView getDataPageView() {
        if (dataPageView == null) {
            dataPageView = createDataPageView();
            dataPageView.init(this);
            dataPageView.refreshData();
        } else {
            dataPageView.refreshData();
        }
        
        return dataPageView;
    }

    @Override
    public void onEdit() {
        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }
        openFormView(dataPageView.getSelectedModel());
    }

    @Override
    public void onDelete() {
        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }
        if (MessageBox.showAskYesNo(I18n.COMMON.getString(
                "MessageBox.Confirm.Delete")) == MessageBox.YES_OPTION) {
         	try {
	            	getService().remove(dataPageView.getSelectedModels());
	                onRefresh();
            } catch (Exception e) {
                MessageBox.showError(I18n.COMMON.getString("Messages.Error.DeleteError"), e);
            }
        }
    }

    @Override
    public void onRefresh() {
        if (dataPageView != null) {
            dataPageView.refreshData();
        }
    }

   

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onMouseDoubleClickOnTable() {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void onBackup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRestore() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onDownload(){
		// TODO Auto-generated method stub
			
		
	}
	
	@Override
	public void sendEmail(){
		// TODO Auto-generated method stub
			
		
	}
	
	@Override
	public void print(){
		// TODO Auto-generated method stub
			
		
	}

	public void setService(AbstractService service) {
		this.service = service;
	}

	public void setDataPageView(DataPageView dataPageView) {
	    this.dataPageView = dataPageView;
	}
	
	

}
