package com.desktopapp.framework;

import java.awt.Component;

/**
 * View interface.
 * 
 * @author Ratnala Diwakar Choudhury
 */
public interface View {
    
    /**
     * Gets view's title
     *
     * @return view's title
     */
    String getTitle();
    
    /**
     * Gets view's icon path
     *
     * @return view's icon path
     */
    String getIconPath();
    
    /**
     * This view as component
     *
     * @return view as component
     */
    Component asComponent();
    
}
