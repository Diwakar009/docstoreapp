/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.framework;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.jdesktop.swingx.JXStatusBar;

import com.desktopapp.component.JMenuBarExt;
import com.desktopapp.component.JMenuExt;
import com.desktopapp.component.JToolBarExt;
import com.desktopapp.component.common.IGenericComponent;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.contoller.AppController;
import com.desktopapp.model.BaseEntity;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * Abstract Form View for data editing in JDialog.
 *
 * @param <T> entity
 *
 * @author Diwakar Choudhury
 */
public abstract class GenericFormView extends JDialog {

    //protected DataPageController<T> controller;
    protected JMenuBarExt menuBar;
    protected JToolBarExt toolBar;
    protected JTabbedPane tpPages;
    protected JXStatusBar xstatusBar;
    protected Action acClose;
   
    
    protected List<IGenericComponent> genericComponentList;
    private JPanel canvasMainPanel;
    
    private KeyEventDispatcher formKeyEventDispatcher;
    
    
    /**
     * Creates form view
     *
     * @param parent parent view
     * @param controller form's controller
     */
   // public GenericFormView(JFrame parent, DataPageController<T> controller) {
    public GenericFormView(JFrame parent, DataPageController controller) {
        super(parent);
        //this.controller = controller;
        genericComponentList = new ArrayList<IGenericComponent>();
    }

    /**
     * init components
     */
    public void initComponents() {
    	
    	setLayout(new BorderLayout());
    	setIconImage(new ImageIcon(getClass().getResource(getFormIconPath())).getImage());
        setTitle(getFormTitle());
        buildFormActions();
        setJMenuBar(buildMenuBar());
        getContentPane().add(buildToolBar(), BorderLayout.NORTH);
        JPanel canvasRootPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
        
        
        formKeyEventDispatcher = new KeyEventDispatcher() {
            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
              
              switch(e.getKeyCode()){
              case KeyEvent.VK_ESCAPE:
        	  onCloseForm();
                  return true;
            	            	  
              default:
            	  return false;
              
              }
			  
            }
        };
    
       KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(formKeyEventDispatcher);
        
        canvasMainPanel = new JPanel();
        canvasMainPanel.setLayout(new BoxLayout(canvasMainPanel,BoxLayout.Y_AXIS));
    	
        if (isMultiPageForm()) {
            tpPages = new JTabbedPane();
            tpPages.setFocusable(false);
            canvasMainPanel.add(tpPages, BorderLayout.CENTER);
        }
        
        canvasRootPanel.add(canvasMainPanel);
      
        getContentPane().add(canvasRootPanel, BorderLayout.CENTER);
        getContentPane().add(buildStatusBar(), BorderLayout.SOUTH);
    	
        addMorePanels();
       
    }

  	protected void addMorePanels() {
		// TODO Auto-generated method stub
		
	}

	/**
     * Gets form view's title
     *
     * @return form view's title
     */
    public abstract String getFormTitle();

    /**
     * Gets form view's icon path
     *
     * @return form view's icon path
     */
    public abstract String getFormIconPath();

    /**
     * Builds form view components.
     * Call first initComponents() in this method.
     */
    public abstract void buildUI();

    /**
     * Pop model values to UI
     */
    public abstract void popFields();

    /**
     * Push UI values to model
     */
    public abstract void pushFields();
   

    /**
     * Validate fields before save
     *
     * @return
     */
    public boolean validateForm() {
        return true;
    }

   
    /**
     * Adds new page to form view.
     *
     * @param title page's title
     * @param page page panel
     */
    public void addPageToForm(String title, JPanel page) {
        if (isMultiPageForm()) {
            tpPages.add(title, page);
        } else {
            //getContentPane().add(page, BorderLayout.CENTER);
        	canvasMainPanel.add(page, BorderLayout.CENTER);
        	
        }
    }

    /**
     * Has this form multi pages?
     *
     * @return true if this form has multi pages.
     */
    public boolean isMultiPageForm() {
        return false;
    }

  
    /**
     * Disable and Enable Fields
     */
    
    public abstract void disableFields();
    

    /**
     * Show dialog
     */
    public void showDialog() {
        buildUI();
        setLocationRelativeTo(AppController.get().getAppView());
        setModalityType(ModalityType.APPLICATION_MODAL);
        disableFields();
        setVisible(true);
    }

    /**
     * Form view close
     */
    public void onCloseForm() {
	KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(formKeyEventDispatcher);
    	KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
    	formKeyEventDispatcher = null;
    	setVisible(false);
    	destroy();
        dispose();
    }

    /**
     * Builds form view's menu bar
     *
     * @return menu bar
     */
    private JMenuBarExt buildMenuBar() {
        menuBar = new JMenuBarExt();
        JMenuExt mnuFile = new JMenuExt(I18n.COMMON.getString("AbstractFormView.Menu.File"));
        
        addMenuAction(mnuFile);
        
        mnuFile.add(new JSeparator());
        mnuFile.add(acClose);
        menuBar.add(mnuFile);
        
        return menuBar;
    }

    protected void addMenuAction(JMenuExt mnuFile) {
		// TODO Auto-generated method stub
		
	}

	/**
     * Builds form view's toolbar
     *
     * @return toolBar action toolbar
     */
    private JToolBar buildToolBar() {
        toolBar = new JToolBarExt();
        toolBar.setRollover(true);
       
        addToolBarButton(toolBar);
         
        toolBar.add(ViewHelpers.createToolButton(acClose, true, true));
        return toolBar;
    }

    protected void addToolBarButton(JToolBarExt toolBar2) {
		// TODO Auto-generated method stub
		
	}

	/**
     * Builds form view actions
     */
    protected void buildFormActions() {
    	
    	acClose = new AbstractAction(I18n.COMMON.getString("Action.Close"),
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "close.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                       // controller.onRefresh();
                        onCloseForm();
                    }
                };
        acClose.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Close"));
        acClose.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_X));
        acClose.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
        
    }

    /**
     * Builds form view's status bar.
     *
     * @return xstatusBar
     */
    private JXStatusBar buildStatusBar() {
        xstatusBar = new JXStatusBar();
        xstatusBar.setPreferredSize(new Dimension(15, 20));

        return xstatusBar;
    }

	public JPanel getCanvasMainPanel() {
		return canvasMainPanel;
	}

   public abstract String getAccessFunctionName(); 
   
   
   public void destroy(){
       
       for(IGenericComponent iGenericComponent : genericComponentList) {
		if(iGenericComponent != null){
		    iGenericComponent.destroyComponent();
		}
       }
       
       menuBar = null;
       toolBar= null;
       tpPages= null;
       xstatusBar= null;
       formKeyEventDispatcher = null;
       acClose= null;
       canvasMainPanel = null;
       genericComponentList = null;
   }
    

}
