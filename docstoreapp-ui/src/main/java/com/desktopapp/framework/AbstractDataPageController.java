package com.desktopapp.framework;

import java.util.List;

import org.apache.log4j.Logger;

import com.desktopapp.component.MessageBox;
import com.desktopapp.model.BaseEntity;
import com.desktopapp.service.AbstractService;
import com.desktopapp.util.I18n;
import com.desktopapp.view.AppView;

/**
 * Abstract Data Page Controller.
 *
 * @param <T> entity
 *
 * @author Ratnala Diwakar Choudhury
 */
public abstract class AbstractDataPageController<T extends BaseEntity> 
        implements DataPageController<T> {

    private final static Logger LOGGER = Logger
	    .getLogger(AbstractDataPageController.class);
    
    protected AbstractService<T> service;
    protected DataPageView<T> dataPageView;

    public AbstractDataPageController() {
    }

    protected abstract AbstractService<T> createService();

    @Override
    public AbstractService<T> getService() {
        if (service == null) {
            service = createService();
        }
        return service;
    }

    protected abstract DataPageView<T> createDataPageView();

    @Override
    public DataPageView<T> getDataPageView() {
        if (dataPageView == null) {
            dataPageView = createDataPageView();
            dataPageView.init(this);
            dataPageView.refreshData();
        } else {
            dataPageView.refreshData();
        }
        
        return dataPageView;
    }

    @Override
    public void onEdit() {
        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }
        openFormView(dataPageView.getSelectedModel());
    }

    @Override
    public void onDelete() {
        if (dataPageView == null) {
            return;
        }
        if (dataPageView.getSelectedModel() == null) {
            return;
        }
        if (MessageBox.showAskYesNo(I18n.COMMON.getString(
                "MessageBox.Confirm.Delete")) == MessageBox.YES_OPTION) {
         	try {
	            	getService().remove(dataPageView.getSelectedModels());
	                onRefresh();
            } catch (Exception e) {
                MessageBox.showError(I18n.COMMON.getString("Messages.Error.DeleteError"), e);
            }
        }
    }

    @Override
    public void onRefresh() {
        if (dataPageView != null) {
            dataPageView.refreshData();
        }
    }

    @Override
    public void onSave(T entity) {
        try {
            if (entity.getEntityPK() == null) {
                getService().create(entity);
            } else {
                getService().update(entity);
            }

        } catch (Exception e) {
            MessageBox.showError(I18n.COMMON.getString("Messages.Error.SaveError"), e);
        }
    }

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onMouseDoubleClickOnTable() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSave(List<T> entities) {
		for(T entity: entities){
			onSave(entity);;
		}
	}

	@Override
	public void onBackup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRestore() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onDownload(){
		// TODO Auto-generated method stub
			
		
	}
	
	@Override
	public void sendEmail(){
		// TODO Auto-generated method stub
			
		
	}
	
	@Override
	public void print(){
		// TODO Auto-generated method stub
			
		
	}

	public void setService(AbstractService<T> service) {
		this.service = service;
	}

	public void setDataPageView(DataPageView<T> dataPageView) {
	    this.dataPageView = dataPageView;
	}
	
	

}
