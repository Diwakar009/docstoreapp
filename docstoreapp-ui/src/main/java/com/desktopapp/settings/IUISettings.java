package com.desktopapp.settings;

public interface IUISettings {
    
    public String getUILookAndFeel();
       
    public String getUILookAndFeelPackage();

}
