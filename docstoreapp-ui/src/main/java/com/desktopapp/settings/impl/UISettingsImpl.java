package com.desktopapp.settings.impl;

import java.util.Map;

import com.desktopapp.settings.IUISettings;

public class UISettingsImpl implements IUISettings{
    
    public static String LOOKANDFEEL = "UILookAndFeel";
    public static String LOOKANDFEEL_PACKAGE = "UILookAndFeelPackage";
    
    
    private Map<String,String> settingMap = null;

    public UISettingsImpl(Map<String, String> settingMap) {
	super();
	this.settingMap = settingMap;
    }
    
    public String getUILookAndFeel(){
	return (String)settingMap.get(LOOKANDFEEL);
    }
    
    public String getUILookAndFeelPackage(){
   	return (String)settingMap.get(LOOKANDFEEL_PACKAGE);
    }

}
