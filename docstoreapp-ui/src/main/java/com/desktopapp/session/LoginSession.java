package com.desktopapp.session;

import java.util.ArrayList;
import java.util.List;

import com.desktopapp.settings.IUISettings;
import com.desktopapp.settings.impl.UISettingsImpl;


public class LoginSession {

    private String userName;
    private String loginTime;
    private String accessType;
    private List<String> functionAccessList;
    private String language;
    private IUISettings uiSettings;
    
    public LoginSession() {
	// TODO Auto-generated constructor stub
	functionAccessList = new ArrayList<String>();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }
    
    public List<String> getFunctionAccessList() {
        return functionAccessList;
    }

    public void setFunctionAccessList(List<String> functionAccessList) {
        this.functionAccessList = functionAccessList;
    }
    
    public IUISettings getUISettings() {
        return uiSettings;
    }

    public void setUISettings(IUISettings uiSettings) {
        this.uiSettings = uiSettings;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
	return "LoginSession [userName=" + userName + ", loginTime="
		+ loginTime + ", accessType=" + accessType
		+ ", functionAccessList=" + functionAccessList + ", language="
		+ language + "]";
    }

 
    

}
