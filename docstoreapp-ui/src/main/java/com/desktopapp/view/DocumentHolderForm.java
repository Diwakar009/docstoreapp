package com.desktopapp.view;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import net.miginfocom.swing.MigLayout;

import com.desktopapp.component.JCodeDecodeField;
import com.desktopapp.component.JComboBoxExt;
import com.desktopapp.component.JTextAreaExt;
import com.desktopapp.component.JTextFieldExt;
import com.desktopapp.component.MessageBox;
import com.desktopapp.component.constants.DocStoreConstants;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.contoller.AppController;
import com.desktopapp.contoller.DocumentContentController;
import com.desktopapp.contoller.DocumentHolderController;
import com.desktopapp.framework.AbstractFormView;
import com.desktopapp.framework.DataPageController;
import com.desktopapp.model.DocHolder;
import com.desktopapp.session.LoginSession;
import com.desktopapp.spring.util.ApplicationContextUtils;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * Document Holder Form
 *
 * @author Ratnala Diwakar Choudhury
 */
public class DocumentHolderForm extends AbstractFormView<DocHolder> {

    private DocHolder docHolder = null;
    private boolean isNewModel = false;

    // form fields
    private JTextFieldExt txtDocumentName;
    private JTextFieldExt txtDocumentType;
    private JComboBoxExt cbDcoumentType;
    private JTextFieldExt txtDocumentTypeOth;
    private JTextFieldExt txtDocumentOwner;
    private JTextFieldExt txtDocumentCoOwner;
    private JTextAreaExt tpSearchKeywords;
    private JCodeDecodeField cdDcoumentType;
    
    
    private JTextFieldExt txtPersistDoc;
    private JComboBoxExt cbPersistDoc;
    private JCodeDecodeField cdPersistDoc;
    
    DocumentContentController documentContentController = null;
    private String searchFilter = "";
    
    /**
     * Gets new instance of country form
     *
     * @param controller country controller
     * @param docHolder model
     */
    public DocumentHolderForm(DataPageController<DocHolder> controller, DocHolder docHolder) {
        super(AppController.get().getAppView(), controller);
        this.docHolder = docHolder;
        if (docHolder.getId() == null) {
            isNewModel = true;
        }
    }
    
    
    /**
     * Gets new instance of country form
     *
     * @param controller country controller
     * @param docHolder model
     */
    public DocumentHolderForm(DataPageController<DocHolder> controller, DocHolder docHolder,String searchFilter) {
        super(AppController.get().getAppView(), controller);
        this.docHolder = docHolder;
        this.searchFilter = searchFilter;
        if (docHolder.getId() == null) {
            isNewModel = true;
        }
    }

    @Override
    public void buildUI() {
        initComponents();
        
        txtDocumentName = new JTextFieldExt(100);
        txtDocumentType = new JTextFieldExt(5);
        cbDcoumentType = new JComboBoxExt();
        txtDocumentTypeOth = new JTextFieldExt(100);
        txtDocumentOwner = new JTextFieldExt(1000);
        txtDocumentCoOwner = new JTextFieldExt(1000);
        tpSearchKeywords = new JTextAreaExt(1000);
        cdDcoumentType = new JCodeDecodeField(txtDocumentType, cbDcoumentType, DocStoreConstants.CD_DOCTYPE);
        
        txtPersistDoc= new JTextFieldExt(1);
        cbPersistDoc= new JComboBoxExt();;
        cdPersistDoc= new JCodeDecodeField(txtPersistDoc, cbPersistDoc, DocStoreConstants.CD_YESNO,true);
        
        
        genericComponentList.add(txtDocumentName);
        genericComponentList.add(txtDocumentType);
        genericComponentList.add(cbDcoumentType);
        genericComponentList.add(txtDocumentTypeOth);
        genericComponentList.add(txtDocumentOwner);
        genericComponentList.add(txtDocumentCoOwner);
        genericComponentList.add(tpSearchKeywords);
        genericComponentList.add(cdDcoumentType);
        
        genericComponentList.add(txtPersistDoc);
        genericComponentList.add(cbPersistDoc);
        genericComponentList.add(cdPersistDoc);
        
         
        
        
        tpSearchKeywords.setPreferredSize(new Dimension(50, 100));
        tpSearchKeywords.setMargin(new Insets(0, 0, 0, 0));
        
        //documentContentController = new DocumentContentController(docHolder,searchFilter);
        documentContentController = (DocumentContentController)ApplicationContextUtils.getBean("documentContentController");
        documentContentController.setDocHolder(docHolder);
        documentContentController.setSearchFilter(searchFilter);
        documentContentController.setDocumentHolderForm(this);
        
        //JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][50:100,fill] [50:200,fill] [fill,grow]", ""));
        //JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][50:200,fill][fill,grow]", ""));
        
        JPanel outerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
        JPanel firstInnerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_VAR, "[][fill,grow][fill,grow]", ""));
        
        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docName")), "gap para");
        firstInnerPanel.add(txtDocumentName,"span 2, wrap"); // span covers 2 cells and then wraps 
        
        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docType")), "gap para");
        firstInnerPanel.add(cdDcoumentType.getCodeDecodeUIPanel(), "span 2,wrap");
        
        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docTypeOthers")), "gap para");
        firstInnerPanel.add(txtDocumentTypeOth, "span 2, wrap");
        
        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docOwner")), "gap para");
        firstInnerPanel.add(txtDocumentOwner, "span 2, wrap");

        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docCoowner")), "gap para");
        firstInnerPanel.add(txtDocumentCoOwner, "span 2, wrap");
        
        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docKeywords")), "gap para");
        firstInnerPanel.add(new JScrollPane(tpSearchKeywords), "span");
        
        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docPersist")), "gap para");
        firstInnerPanel.add(cdPersistDoc.getCodeDecodeUIPanel(), "span 2,wrap");
        
        outerPanel.add(firstInnerPanel,"align,span");
        
        if(!isNewModel){
            JPanel secondInnerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
	        secondInnerPanel.add(documentContentController.getDataPageView().asComponent(),"span");
	        outerPanel.add(secondInnerPanel,"align");
        }
        
        addPageToForm("", outerPanel);

        
        popFields();
        
        
        pack();
        
        
        if(!isNewModel){
        	setSize(700, 700);
        }else{
        	setSize(430, 330);
        }
    }
    
    
    

    @Override
    public DocHolder getEntity() {
        return docHolder;
    }

    @Override
    public void popFields() {
    	txtDocumentName.setDataValue(docHolder.getDocName());
    	txtDocumentType.setDataValue(docHolder.getDocType());
    	txtDocumentTypeOth.setDataValue(docHolder.getDocTypeOthers());
    	txtDocumentOwner.setDataValue(docHolder.getDocOwner());
    	txtDocumentCoOwner.setDataValue(docHolder.getDocCoowner());
    	tpSearchKeywords.setText(docHolder.getDocKeywords());
    	txtPersistDoc.setDataValue(docHolder.getDocPersist());
    	
    }

    @Override
    public void pushFields() {
    	docHolder.setDocName(txtDocumentName.getDataValue());
    	docHolder.setDocType(txtDocumentType.getDataValue());
    	docHolder.setDocOwner(txtDocumentOwner.getDataValue());
    	docHolder.setDocCoowner(txtDocumentCoOwner.getDataValue());
    	docHolder.setDocTypeOthers(txtDocumentTypeOth.getDataValue());
    	docHolder.setDocKeywords(tpSearchKeywords.getDataValue());
    	docHolder.setDocPersist(txtPersistDoc.getDataValue());
    	
    	if (isNewModel) {
            // AppContoller.getAppContoller.getLoggedUser().getName();
            docHolder.setCreatedBy(AppView.getLoginSession().getUserName());
        } else {
            docHolder.setUpdatedBy(AppView.getLoginSession().getUserName());
        }
    }

    /**
     * Validate form fields
     *
     * @return true if no validate errors
     */
    @Override
    public boolean validateForm() {
    	/*
        if (tfxCode.getDataValue().equals("")) {
            MessageBox.showWarning(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.CodeError"));
            tfxCode.requestFocus();
            return false;
        }
        if (tfxName.getDataValue().equals("")) {
            MessageBox.showWarning(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.NameError"));
            tfxName.requestFocus();
            return false;
        }
        */
        return true;
    }

    @Override
    public String getFormIconPath() {
        return ViewHelpers.ICONS16 + "folder.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return I18n.DOCUMENTHOLDER.getString("DocHolder.Form.NewTitle");
        } else {
            return I18n.DOCUMENTHOLDER.getString("DocHolder.Form.EditTitle");
        }
    }

    @Override
    public void onHelp() {
        MessageBox.showInfo("Help not implemented yet!");
    }

	@Override
	public void disableFields() {
		// TODO Auto-generated method stub
		
	}


	public DocumentContentController getDocumentContentController() {
		return documentContentController;
	}


	public void setDocumentContentController(
			DocumentContentController documentContentController) {
		this.documentContentController = documentContentController;
	}


	@Override
	public String getAccessFunctionName() {
	    // TODO Auto-generated method stub
	    return FunctionAccessConstants.FN_DOCHOLDER;
	}


	@Override
	public void destroy() {
	    super.destroy();
	    
	    docHolder = null;
	    // form fields
	    txtDocumentName= null;
	    txtDocumentType= null;
	    cbDcoumentType= null;
	    txtDocumentTypeOth= null;
	    txtDocumentOwner= null;
	    txtDocumentCoOwner= null;
	    tpSearchKeywords= null;
	    cdDcoumentType= null;
	 
	    
	}
	
	

}
