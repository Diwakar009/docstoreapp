package com.desktopapp.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXStatusBar;
import org.jvnet.flamingo.common.JCommandButton;
import org.jvnet.flamingo.common.JCommandButton.CommandButtonKind;
import org.jvnet.flamingo.common.JCommandToggleButton;
import org.jvnet.flamingo.common.icon.EmptyResizableIcon;
import org.jvnet.flamingo.ribbon.RibbonApplicationMenuEntryFooter;
import org.jvnet.flamingo.ribbon.RibbonApplicationMenuEntryPrimary;
import org.jvnet.flamingo.ribbon.RibbonElementPriority;
import org.jvnet.flamingo.ribbon.RibbonTask;
import org.jvnet.flamingo.ribbon.resize.CoreRibbonResizePolicies;

import com.desktopapp.component.AboutDialog;
import com.desktopapp.component.JRibbonBandExt;
import com.desktopapp.component.JRibbonFrameExt;
import com.desktopapp.component.LookAndFeelDialog;
import com.desktopapp.component.MessageBox;
import com.desktopapp.component.RibbonApplicationMenuExt;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.contoller.AppController;
import com.desktopapp.contoller.AppUserController;

import com.desktopapp.contoller.DocumentHolderController;
import com.desktopapp.contoller.StaticCodeDecodeController;
import com.desktopapp.framework.View;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.session.LoginSession;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * Application View
 *
 * @author Diwakar Choudhury
 */
public class AppView extends JRibbonFrameExt {
    
    private final static Logger LOGGER = Logger.getLogger(AppView.class);

    private JXStatusBar xstatusBar;
    private JPanel centerPanel;
    private static LoginSession loginSession = null;
    
    List<JCommandButton> commandbuttonList = new ArrayList<JCommandButton>();
    
    
    /**
     * Creates a new instance of AppView
     *
     * @param title
     */
    public AppView(String title) {
        super(title);
        this.setIconImages(Arrays.asList(
                new ImageIcon(getClass().getResource(
                                ViewHelpers.ICONS16 + "documents.png")).getImage(),
                new ImageIcon(getClass().getResource(
                                ViewHelpers.ICONS22 + "documents.png")).getImage())
        );

        //setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        initComponents();
        //setSize(1024, 768);
        // for max screen
        setExtendedState(MAXIMIZED_BOTH);
        
        setResizable(true);
        
        setLocationRelativeTo(null);
        
        LOGGER.info(loginSession.toString());
    }

    /**
     * init ui components
     */
    private void initComponents() {
        
        
        getContentPane().add(buildStatusBar(), BorderLayout.SOUTH);

        // ribbon menu 
        configureRibbonMenu();
        
        centerPanel = new JPanel(new BorderLayout());
        
        centerPanel.setBorder(BorderFactory.createMatteBorder(0, -1, 0, -1,
                ViewHelpers.getSubstanceComponentBorderColor(centerPanel)));
       
	DocumentHolderController docmentHolderController = (DocumentHolderController)ObjectFactory.getControllerObject(ObjectFactory.DOC_HOLDER_CTRL);
        
	    centerPanel.add(docmentHolderController.getDataPageView().asComponent(), BorderLayout.CENTER);
 	    
        getContentPane().add(centerPanel, BorderLayout.CENTER);
        
        
        /*
        KeyboardFocusManager.getCurrentKeyboardFocusManager()
        .addKeyEventDispatcher(new KeyEventDispatcher() {
            @Override
            public boolean dispatchKeyEvent(KeyEvent e) {
              
              switch(e.getKeyCode()){
              
              case KeyEvent.VK_ESCAPE:
            	  exitForm(null);
            	  return true;
            	  
              default:
            	  return false;
              
              }
			  
            }
      });
      
      */

      pack();
    }

   
    
    /**
     * Application logout
     * 
     * @see AppController#logout() 
     */
    private void logoutForm(WindowEvent evt) {
        if (showExitDialog) {
            if (MessageBox.showAskYesNo(I18n.COMMON.getString(
                    "MessageBox.Confirm.ProgramLogout")) == MessageBox.NO_OPTION) {
                return;
            }
        }

        AppController.get().logout();
    }
    
    
    /**
     * Open static code decode page
     */
    public void onOpenStaticCodeDecode() {
    	StaticCodeDecodeController controller = (StaticCodeDecodeController)ObjectFactory.getControllerObject(ObjectFactory.STATIC_CODE_DECODE_CTRL);
        addPageToCenter(controller.getDataPageView());
    }
    
    /**
     * Open document Holder Page
     */
    public void onOpenDocumentHolder() {
    	DocumentHolderController controller = (DocumentHolderController)ObjectFactory.getControllerObject(ObjectFactory.DOC_HOLDER_CTRL);
        addPageToCenter(controller.getDataPageView());
    }
    
    
    /**
     * Open customers page
     */
    public void onOpenAppUsers() {
    	AppUserController controller = (AppUserController)ObjectFactory.getControllerObject(ObjectFactory.APP_USER_CTRL);
        addPageToCenter(controller.getDataPageView());
    }
    
   
   /**
     * Help
     */
    public void onHelp() {
        MessageBox.showInfo("Help not implemented yet!");
    }

    /**
     * Show hide the status bar
     */
    public void onToogleStatusBar() {
        xstatusBar.setVisible(!xstatusBar.isVisible());
    }

    /**
     * Look and feel dialog
     */
    public void onLookAndFeel() {
        LookAndFeelDialog.showDialog();
    }

    /**
     * About Dialog
     */
    public void onAbout() {
        AboutDialog.showDialog();
    }

    /**
     * Settings Dialog
     */
    public void onSettings() {
        MessageBox.showInfo("Settings not implemented yet!");
    }

    /**
     * Adds page view or data page view to center
     *
     * @param page page view or data page view
     */
    public void addPageToCenter(View page) {
        centerPanel.removeAll();
        centerPanel.add(page.asComponent());
        centerPanel.revalidate();
        centerPanel.repaint();
        page.asComponent().requestFocus();
    }

    /**
     * Ribbon menu config
     */
    private void configureRibbonMenu() {
        // file task
        RibbonTask fileTask = new RibbonTask(
                I18n.COMMON.getString("AppView.File"),
                getActionsBand(),
                getAppExitBand());
        fileTask.setKeyTip("F");

        // extras task
        RibbonTask extrasTask = new RibbonTask(
                I18n.COMMON.getString("AppView.Extras"),
                getViewBand(),
                getExtrasBand());
        extrasTask.setKeyTip("E");

        RibbonTask helpTask = new RibbonTask(
                I18n.COMMON.getString("AppView.Help"),
                getHelpBand());
        helpTask.setKeyTip("H");

        this.getRibbon().addTask(fileTask);
        this.getRibbon().addTask(extrasTask);
        this.getRibbon().addTask(helpTask);
        
        
        configureTaskBar();
        configureApplicationMenu();

        // help button to left side
        this.getRibbon().configureHelp(ViewHelpers.createResizableIcon(
        	new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "help.png"))),
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        onHelp();
                    }
                }
        );
        
      
    }

    /**
     * Ribbon menu top taskbar actions
     */
    protected void configureTaskBar() {
    	
    	  
	/*
	  JCommandButton cbtnStaticCodeDecode = new JCommandButton("",
                  ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                          ViewHelpers.ICONS16 + "code.png"))));
    	  cbtnStaticCodeDecode.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                  onOpenStaticCodeDecode();
              }
          });
        */  
          
    	  JCommandButton cbtnAppUser = new JCommandButton("",
                  ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                          ViewHelpers.ICONS16 + "user.png"))));
    	  cbtnAppUser.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                  onOpenAppUsers();
              }
          });
    	  
    	  commandbuttonList.add(cbtnAppUser);

          JCommandButton cbtnDocHolder = new JCommandButton("",
                  ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                          ViewHelpers.ICONS16 + "folder.png"))));
          cbtnDocHolder.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                  onOpenDocumentHolder();
                  
              }
          });
          commandbuttonList.add(cbtnDocHolder);
          
          //this.getRibbon().addTaskbarComponent(cbtnStaticCodeDecode);
          //this.getRibbon().addTaskbarComponent(cbtnAppUser);
          //this.getRibbon().addTaskbarComponent(cbtnDocHolder);
      
          addTaskbarComponent(cbtnAppUser,FunctionAccessConstants.FN_APPUSER);
          addTaskbarComponent(cbtnDocHolder,FunctionAccessConstants.FN_DOCHOLDER);
      
    }

    /**
     * Ribbon Application Menu settings
     */
    protected void configureApplicationMenu() {
    	
    	
	/*
    	  RibbonApplicationMenuEntryPrimary amStaticCodeDecode = new RibbonApplicationMenuEntryPrimary(
                  ViewHelpers.createResizableIcon(
                          new ImageIcon(getClass().getResource(ViewHelpers.ICONS22 + "code.png"))),
                  I18n.STATICCODEDECODE.getString("Action.StaticCodeDecode"),
                  new ActionListener() {
                      @Override
                      public void actionPerformed(ActionEvent e) {
                          onOpenStaticCodeDecode();
                      }
                  }, CommandButtonKind.ACTION_ONLY);
    	  
    	  amStaticCodeDecode.setActionKeyTip("S");
    	  amStaticCodeDecode.setEnabled(false);
    	*/
	 RibbonApplicationMenuEntryPrimary amAppUser = new RibbonApplicationMenuEntryPrimary(
                  ViewHelpers.createResizableIcon(
                          new ImageIcon(getClass().getResource(ViewHelpers.ICONS22 + "user.png"))),
                  I18n.APPUSER.getString("Action.AppUser"),
                  new ActionListener() {
                      @Override
                      public void actionPerformed(ActionEvent e) {
                          onOpenAppUsers();;
                      }
                  }, CommandButtonKind.ACTION_ONLY);
          
    	  amAppUser.setActionKeyTip("U");
    	  

          RibbonApplicationMenuEntryPrimary amDocHolder = new RibbonApplicationMenuEntryPrimary(
                  ViewHelpers.createResizableIcon(
                          new ImageIcon(getClass().getResource(ViewHelpers.ICONS22 + "folder.png"))),
                  I18n.DOCUMENTHOLDER.getString("Action.DocHolder"),
                  new ActionListener() {
                      @Override
                      public void actionPerformed(ActionEvent e) {
                          onOpenDocumentHolder();
                      }
                  }, CommandButtonKind.ACTION_ONLY);
          
          amDocHolder.setActionKeyTip("D");
    	
    	
        RibbonApplicationMenuEntryFooter amEntryExit = new RibbonApplicationMenuEntryFooter(
                ViewHelpers.createResizableIcon(
                        new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "exit.png"))),
                I18n.COMMON.getString("Action.Exit"),
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        exitForm(null);
                    }
                });
        
        amEntryExit.setActionKeyTip("X");

        RibbonApplicationMenuExt applicationMenu = new RibbonApplicationMenuExt();
        applicationMenu.addMenuEntry(amAppUser,FunctionAccessConstants.FN_APPUSER);
	applicationMenu.addMenuEntry(amDocHolder,FunctionAccessConstants.FN_DOCHOLDER);
        applicationMenu.addFooterEntry(amEntryExit);
        this.getRibbon().setApplicationMenu(applicationMenu);
    }

    /**
     * File menu actions band
     *
     * @return file menu actionsBand
     */
    private JRibbonBandExt getActionsBand() {
    	
    	  JRibbonBandExt actionsBand = new JRibbonBandExt(
                  I18n.COMMON.getString("AppView.ActionsBand"),
                  new EmptyResizableIcon(22));
    	  
    	  actionsBand.setResizePolicies(
                  CoreRibbonResizePolicies.getCorePoliciesRestrictive(actionsBand));
    	
    
        
        JCommandButton cbtnStaticCodeDecode = new JCommandButton(
                I18n.STATICCODEDECODE.getString("Action.StaticCodeDecode"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "code.png"))));
        cbtnStaticCodeDecode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOpenStaticCodeDecode();
            }
        });
        
        commandbuttonList.add(cbtnStaticCodeDecode);
       
        
        cbtnStaticCodeDecode.setActionKeyTip("S");
        
        actionsBand.addCommandButton(cbtnStaticCodeDecode, RibbonElementPriority.TOP,FunctionAccessConstants.FN_CODEDECODE);
        
        JCommandButton cbtnAppUser = new JCommandButton(
                I18n.APPUSER.getString("Action.AppUser"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "customer.png"))));
        cbtnAppUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOpenAppUsers();
            }
        });
        
        commandbuttonList.add(cbtnAppUser);
        
        cbtnAppUser.setActionKeyTip("U");
        actionsBand.addCommandButton(cbtnAppUser, RibbonElementPriority.TOP,FunctionAccessConstants.FN_APPUSER);
          
        JCommandButton cbtnDocumentHolder = new JCommandButton(
                I18n.DOCUMENTHOLDER.getString("Action.DocHolder"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "folder.png"))));
        cbtnDocumentHolder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOpenDocumentHolder();
            }
        });
        cbtnDocumentHolder.setActionKeyTip("D");
        actionsBand.addCommandButton(cbtnDocumentHolder, RibbonElementPriority.TOP,FunctionAccessConstants.FN_DOCHOLDER);
        
        commandbuttonList.add(cbtnDocumentHolder);
        
        
        return actionsBand;
    }
    
    
     /**
     * File menu exit band
     *
     * @return exitBand
     */
    private JRibbonBandExt getAppExitBand() {
	
	
        JRibbonBandExt appExitBand = new JRibbonBandExt(
                I18n.COMMON.getString("AppView.AppExitBand"),
                new EmptyResizableIcon(22));
        appExitBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(appExitBand));

        JCommandButton cbtnAppLogout = new JCommandButton(
                I18n.COMMON.getString("Action.Logout"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "logout.png"))));
        cbtnAppLogout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logoutForm(null);
            }
        });
        cbtnAppLogout.setActionKeyTip("L");
        appExitBand.addCommandButton(cbtnAppLogout, RibbonElementPriority.TOP);
        
        commandbuttonList.add(cbtnAppLogout);
        
        JCommandButton cbtnAppExit = new JCommandButton(
                I18n.COMMON.getString("Action.Exit"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "exit.png"))));
        cbtnAppExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitForm(null);
            }
        });
        cbtnAppExit.setActionKeyTip("X");
        appExitBand.addCommandButton(cbtnAppExit, RibbonElementPriority.TOP);

        commandbuttonList.add(cbtnAppExit);
        
        return appExitBand;
    }

    /**
     * Extras menu view band and command buttons
     *
     * @return viewBand
     */
    private JRibbonBandExt getViewBand() {
        JRibbonBandExt viewBand = new JRibbonBandExt(
                I18n.COMMON.getString("AppView.ViewBand"),
                new EmptyResizableIcon(22));

        JCommandToggleButton cbtnStatusBar = new JCommandToggleButton(
                I18n.COMMON.getString("Action.StatusBar"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "statusbar.png"))));
        cbtnStatusBar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onToogleStatusBar();
            }
        });
        cbtnStatusBar.setActionKeyTip("S");
        cbtnStatusBar.getActionModel().setSelected(true);
        viewBand.addCommandButton(cbtnStatusBar, RibbonElementPriority.TOP);
        
    
        JCommandButton cbtnLookAndFeel = new JCommandButton(
                I18n.COMMON.getString("Action.LookAndFeel"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "laf.png"))));
        cbtnLookAndFeel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onLookAndFeel();
            }
        });
        cbtnLookAndFeel.setActionKeyTip("T");
        viewBand.addCommandButton(cbtnLookAndFeel, RibbonElementPriority.TOP);
        commandbuttonList.add(cbtnLookAndFeel);

        
        return viewBand;
    }

    /**
     * Extras menu band and command buttons
     *
     * @return extrasBand
     */
    private JRibbonBandExt getExtrasBand() {
        JRibbonBandExt extrasBand = new JRibbonBandExt(
                I18n.COMMON.getString("AppView.ExtrasBand"),
                new EmptyResizableIcon(22));
        extrasBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(extrasBand));

        JCommandButton cbtnSettings = new JCommandButton(
                I18n.COMMON.getString("Action.Settings"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "settings.png"))));
        cbtnSettings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onSettings();
            }
        });
        cbtnSettings.setActionKeyTip("E");
        extrasBand.addCommandButton(cbtnSettings, RibbonElementPriority.TOP);
        commandbuttonList.add(cbtnSettings);

        return extrasBand;
    }

    /**
     * Help menu band and command buttons
     *
     * @return helpBand
     */
    private JRibbonBandExt getHelpBand() {
        JRibbonBandExt helpBand = new JRibbonBandExt(
                I18n.COMMON.getString("AppView.HelpBand"),
                new EmptyResizableIcon(22));
        helpBand.setResizePolicies(
                CoreRibbonResizePolicies.getCorePoliciesRestrictive(helpBand));

        JCommandButton cbtnHelp = new JCommandButton(
                I18n.COMMON.getString("AppView.Help"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "help.png"))));
        cbtnHelp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onHelp();
            }
        });
        cbtnHelp.setActionKeyTip("H");
        helpBand.addCommandButton(cbtnHelp, RibbonElementPriority.TOP);
        
        commandbuttonList.add(cbtnHelp);

        JCommandButton cbtnAbout = new JCommandButton(
                I18n.COMMON.getString("AppView.Help.About"),
                ViewHelpers.createResizableIcon(new ImageIcon(getClass().getResource(
                                        ViewHelpers.ICONS22 + "about.png"))));
        cbtnAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAbout();
            }
        });
        cbtnAbout.setActionKeyTip("A");
        helpBand.addCommandButton(cbtnAbout, RibbonElementPriority.TOP);

        commandbuttonList.add(cbtnAbout);
        return helpBand;
    }

    /**
     * Create XStatusBar with user name and date
     */
    private JXStatusBar buildStatusBar() {
        xstatusBar = new JXStatusBar();
        xstatusBar.setPreferredSize(new Dimension(15, 20));

        JLabel lblUser = new JLabel();
        //lblUser.setText(I18n.COMMON.getString("AppView.StatusBar.User") + " : Admin User ");
        lblUser.setText(I18n.COMMON.getString("AppView.StatusBar.User") + " : " + getLoginSession().getUserName().toUpperCase());
        xstatusBar.add(lblUser, new JXStatusBar.Constraint(400));

        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern("EEEE', 'dd. MMMM yyyy");
        JLabel lblDate = new JLabel(" " + sdf.format(new Date()).toString());
        xstatusBar.add(lblDate, new JXStatusBar.Constraint(300));

        return xstatusBar;
    }

    public static LoginSession getLoginSession() {
        return loginSession;
    }

    public static void setLoginSession(LoginSession loginSession) {
        AppView.loginSession = loginSession;
    }

  
    /**
     * Is Function access.
     * 
     * @param functionName
     * @return
     */
    public static boolean hasFunctionAccess(String functionName){
	
	boolean isAccessable = false;
	
	if(!(isAccessable = getLoginSession().getFunctionAccessList().contains(functionName))){
	    if(functionName.lastIndexOf("_") > 0){
        	    String replacableText = functionName.substring(functionName.lastIndexOf("_"),functionName.length());
        	    String likeFunction = "%" + replacableText;
        	    isAccessable = getLoginSession().getFunctionAccessList().contains(likeFunction);
	    }
	}
	
	return isAccessable;
    }
    
    
    public void distroy(){
	
        xstatusBar = null;
	centerPanel = null;
	commandbuttonList = null;
	dispose();
    }
    
}
