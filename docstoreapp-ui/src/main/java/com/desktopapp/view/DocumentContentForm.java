package com.desktopapp.view;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;

import org.apache.commons.io.FilenameUtils;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXList;

import com.desktopapp.component.JDateTimeField;
import com.desktopapp.component.JTextAreaExt;
import com.desktopapp.component.JTextFieldExt;
import com.desktopapp.component.MessageBox;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.component.listeners.ValueChangeListener;
import com.desktopapp.contoller.AppController;
import com.desktopapp.framework.AbstractFormView;
import com.desktopapp.framework.DataPageController;
import com.desktopapp.model.DocContent;
import com.desktopapp.model.DocContentBlob;
import com.desktopapp.model.DocHolder;
import com.desktopapp.util.CommonFileUtil;
import com.desktopapp.util.I18n;
import com.desktopapp.util.UIFileUtil;
import com.desktopapp.util.ViewHelpers;

/**
 * Document Holder Form
 *
 * @author Ratnala Diwakar Choudhury
 */
public class DocumentContentForm extends AbstractFormView<DocContent> {

	private List<DocContent> docContentList = new ArrayList<DocContent>();
	
    private DocContent docContent = null;
    private DocHolder docHolder = null;
    private DocContentBlob contentBlob = null;
    private boolean isNewModel = false;

    // form fields
    private JTextFieldExt txtDocContentSeqNo;
    private JTextFieldExt txtDocContentName;
    private JTextAreaExt tpSearchKeywords;
    private JTextFieldExt txtDirPath;
    private JXButton btnMultipleUpload;
    private JXButton btnSingleUpload;
    private JFileChooser fileChooser;
    private JTextFieldExt txtFileName;
    private JTextFieldExt txtFileExtension;
    private JXButton btnOpenDocment;
    private JXList liSelectionFileList;
    private List<File> selectedFileList = new ArrayList<File>();
    private JDateTimeField dtLastModifiedDate;
    
    
    
    /**
     * Gets new instance of country form
     *
     * @param controller country controller
     * @param docContent model
     */
    public DocumentContentForm(DataPageController<DocContent> controller, DocContent docContent) {
        super(AppController.get().getAppView(), controller);
        this.docContent = docContent;
        if (docContent.getId() == null) {
            isNewModel = true;
        }
    }
    
    /**
     * Gets new instance of country form
     *
     * @param controller country controller
     * @param docContent model
     */
    public DocumentContentForm(DataPageController<DocContent> controller,DocHolder docHolder, DocContent docContent) {
        super(AppController.get().getAppView(), controller);
        this.docContent = docContent;
        this.docHolder = docHolder;
        if (docContent.getId() == null) {
            isNewModel = true;
        }
    }
    

    @Override
    public void buildUI() {
        initComponents();
        selectedFileList = new ArrayList<File>();
        txtDocContentSeqNo = new JTextFieldExt(5,true);
        txtDocContentName = new JTextFieldExt(100);
        tpSearchKeywords = new JTextAreaExt(1000);
        txtDirPath = new JTextFieldExt(1000,true);
        txtFileExtension =new JTextFieldExt(5, true);
        liSelectionFileList = new JXList();
        dtLastModifiedDate= new JDateTimeField(true);
        
        
        
        
        txtDirPath.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChanged() {
				//fireFilePathChanged(txtDirPath.getDataValue());
			}
		});
        
        txtDirPath.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent paramFocusEvent) {
				//fireFilePathChanged(txtDirPath.getDataValue());
			}
			
			@Override
			public void focusGained(FocusEvent paramFocusEvent) {
				// TODO Auto-generated method stub
				
			}
		});
        
        txtFileName = new JTextFieldExt(100, true);
        btnMultipleUpload = new JXButton();
        btnSingleUpload = new JXButton();
        
        btnOpenDocment= new JXButton();
        
        genericComponentList.add(txtDocContentSeqNo);
        genericComponentList.add(txtDocContentName);
        genericComponentList.add(tpSearchKeywords);
        genericComponentList.add(txtDirPath);
        genericComponentList.add(txtFileExtension);
        genericComponentList.add(dtLastModifiedDate);
        genericComponentList.add(txtFileName);
        
        
        
        tpSearchKeywords.setPreferredSize(new Dimension(50, 100));
        tpSearchKeywords.setMargin(new Insets(0, 0, 0, 0));
        
        JPanel outerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
        JPanel firstInnerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_VAR, "[][fill,grow][fill,grow]", ""));
        
        if(!isNewModel){
	        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docName")), "gap para");
	        firstInnerPanel.add(txtDocContentName,"span 2, wrap"); // span covers 2 cells and then wraps
        }
        
        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docKeywords")), "gap para");
        firstInnerPanel.add(new JScrollPane(tpSearchKeywords), "span");
        
        if(!isNewModel){
        	
            firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.FileName")), "gap para");
	    firstInnerPanel.add(txtFileName, "span");
	        
	    firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.FileExtension")), "gap para");
	    firstInnerPanel.add(txtFileExtension, "span");
	        
	    firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.docLastModifiedDate")), "gap para");
	    firstInnerPanel.add(dtLastModifiedDate, "span");
	        
        }
        
        
        if(isNewModel){
        	
            firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.liSelectedFiles")), "gap para");
            firstInnerPanel.add(new JScrollPane(liSelectionFileList), "span");
        }
        
        
        final JPanel outerFileChooserPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
        //final JPanel fileChooserPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow][50:75]", ""));
        final JPanel fileChooserPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0));
        
        //fileChooserPanel.add(txtDirPath,"align");
        fileChooserPanel.add(txtDirPath,"dock center");

        if(!isNewModel){
        	// In case of update single document upload
        	fileChooserPanel.add(btnSingleUpload,"align");
        	btnSingleUpload.setAction(new AbstractAction() {
    			@Override
    			public void actionPerformed(ActionEvent actionEvent) {
    				if(fileChooser == null){
    					fileChooser = new JFileChooser();
    					fileChooser.setAcceptAllFileFilterUsed(true);
    				}
    		        int returnVal = fileChooser.showOpenDialog(fileChooserPanel);
    		        if (returnVal == JFileChooser.APPROVE_OPTION) {
    		            
    		        	File file = fileChooser.getSelectedFile();
    		        	if(file != null){
    		            	selectedFileList = Arrays.asList(file);
    		            }
    		            txtDirPath.setDataValue(fileChooser.getCurrentDirectory().getAbsolutePath());
    		        }
    		        
    		        fileChooser.setSelectedFile(null);
    			}
    		});
        	btnSingleUpload.setText("Browse");
         }else{
        	 // In case of new multiple document upload
         	fileChooserPanel.add(btnMultipleUpload,"align");
         	btnMultipleUpload.setAction(new AbstractAction() {
    			@Override
    			public void actionPerformed(ActionEvent actionEvent) {
    				if(fileChooser == null){
    					fileChooser = new JFileChooser();
    					fileChooser.setAcceptAllFileFilterUsed(true);
    					fileChooser.setMultiSelectionEnabled(true);
    				}
    		        int returnVal = fileChooser.showOpenDialog(fileChooserPanel);
    		        if (returnVal == JFileChooser.APPROVE_OPTION) {
    		            File[] files = fileChooser.getSelectedFiles();
    		            DefaultListModel<String> model = new DefaultListModel<String>();
     		            if(files != null){
    		            	selectedFileList = Arrays.asList(files);
    		            	for(File file  : selectedFileList){
    		            		//selectedFileNames.add(CommonFileUtil.getFileName(file));
    		            		model.addElement(CommonFileUtil.getFileName(file));
    		            	}
    		            }
    		            liSelectionFileList.setModel(model);
    		            txtDirPath.setDataValue(fileChooser.getCurrentDirectory().getAbsolutePath());
    		        }
    		        fileChooser.setSelectedFile(null);
    			}
    		});
            btnMultipleUpload.setText("Browse");
          }
        
        
        firstInnerPanel.add(new JLabel(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.uploadFile")), "gap para");
        firstInnerPanel.add(outerFileChooserPanel.add(fileChooserPanel), "span");
    	
        if(!isNewModel){
        	
        	/**
        	 *  Creates a temporary file in the project location and opens the file
        	 * 
        	 */
            btnOpenDocment.setAction(new AbstractAction() {
            	
				@Override
				public void actionPerformed(ActionEvent arg0) {
					String fileNameWithOutExt = CommonFileUtil.getFileNameWithoutExtention(docContent.getDocFileName());
					
					if("N".equals(docHolder.getDocPersist())){
					    // Open the file from the location
					    String filePathName = docContent.getDocDirPath() + java.io.File.separator + docContent.getDocFileName();  
					    try {
						UIFileUtil.openFile(filePathName);
					    } catch (IOException e) {
						e.printStackTrace();
					    }
					}else{

						if(contentBlob == null){
							for(DocContentBlob c : docContent.getDocContentBlobs()){
				    			contentBlob = c;
							}
						}
						
						//byte[] blobData = CommonFileUtil.getByteArrayFromBlob(contentBlob.getDocContentBlob());
	        				byte[] blobData = contentBlob.getDocContentBlob();
	        	        		UIFileUtil.openByteDataAsFile(blobData, fileNameWithOutExt, docContent.getDocFileExtension());
	        	        		contentBlob = null;
						
					}
				}
				});
        	
	        firstInnerPanel.add(new JLabel("Open Document"), "gap para");
	        btnOpenDocment.setText("Open");
	        firstInnerPanel.add(btnOpenDocment, "span");
        }
        
        
        
        outerPanel.add(firstInnerPanel,"align,wrap");

        addPageToForm("", outerPanel);

        
        popFields();
        pack();
        
      
        setSize(430, 330);
    }
    
    /**
    Write a byte array to the given file. 
    Writing binary data is significantly simpler than reading it. 
   */
   void write(byte[] aInput, String aOutputFileName){
     try {
       OutputStream output = null;
       try {
         output = new BufferedOutputStream(new FileOutputStream(aOutputFileName));
         output.write(aInput);
       }
       finally {
         output.close();
       }
     }
     catch(FileNotFoundException ex){
     }
     catch(IOException ex){
     }
   }
    
    /**
     * fire for populating the filename and file extention
     * 
     * @param filePath
     */
    private void fireFilePathChanged(String filePath){
    	
    	if(!isNewModel){
	    	if(selectedFileList != null && selectedFileList.size() > 0){
	    		for(File file : selectedFileList){
	    			if(filePath != null && filePath.length() > 0){
	    	    		Path p = Paths.get(filePath);
	    		        txtFileName.setDataValue(p.getFileName().toString());
	    		        String ext = FilenameUtils.getExtension(p.getFileName().toString());
	    		        txtFileExtension.setDataValue(ext);
	    	    	}
	    		}
	    	}
    	}
    }
    

    @Override
    public DocContent getEntity() {
        return docContent;
    }

    @Override
    public void popFields() {
    	
    	txtDocContentName.setDataValue(docContent.getDocContentName());
    	txtFileExtension.setDataValue(docContent.getDocFileExtension());
    	
    	if(docContent.getDocLastModifiedTime() != null){
    	    dtLastModifiedDate.setDataValue(docContent.getDocLastModifiedTime().toString());
    	}
    	
    	txtFileName.setDataValue(docContent.getDocFileName());
    	txtDirPath.setDataValue(docContent.getDocDirPath());
    	tpSearchKeywords.setText(docContent.getDocContentKeywords());
    	
    }

    @Override
    public void pushFields() {
    	
	
	if("N".equals(docHolder.getDocPersist())){
	
	    	// New Document can add multiple documents
	    
	    if(!isNewModel){ // in case of Edit
		
		docContent.setDocHolder(this.docHolder);
		File selectedFile = null;
        	
           	for(File file : selectedFileList){
	       		selectedFile = file;
	       	}
           	
        	if(selectedFile != null){ // If any file is selected for upload
       		
       		Path selectedFilePath = Paths.get(selectedFile.getAbsolutePath());
    		String fileName = selectedFilePath.getFileName().toString();
    		String fileExt = FilenameUtils.getExtension(selectedFilePath.getFileName().toString());
    		//String fileNameWithOutExt = FilenameUtils.removeExtension(fileName);
    		docContent.setDocFileName(fileName);
    		docContent.setDocFileExtension(fileExt);
    		docContent.setDocDirPath(txtDirPath.getDataValue());
			
    		// If any file is selected for upload
    		try {
	    			try {
	    				Path p = Paths.get(selectedFile.getAbsolutePath());
	    				//byte[] data = Files.readAllBytes(p);
	    				//contentBlob.setDocContentBlob(CommonFileUtil.getBlobFromByteArray(data));
	    				//contentBlob.setDocContentBlob(data);
	    				docContent.setDocLastModifiedTime(new Date(Files.getLastModifiedTime(p).toMillis()));
	    	               } catch (IOException e) {
	    				e.printStackTrace();
	    			}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
    	  
        	}
           	
        	docContent.setDocContentName(txtDocContentName.getDataValue());
        	docContent.setDocContentKeywords(tpSearchKeywords.getDataValue());
           	
        	docContent.setUpdatedBy("Admin User");
          	
	    }else{
	    
		for(File selectedFile : selectedFileList){
	    		
	    		DocContent docContent = new DocContent();
	    		//DocContentBlob contentBlob = new DocContentBlob();
	    	
	    		Path selectedFilePath = Paths.get(selectedFile.getAbsolutePath());
	    		String fileName = selectedFilePath.getFileName().toString();
	    		String fileExt = FilenameUtils.getExtension(selectedFilePath.getFileName().toString());
	    		String fileNameWithOutExt = FilenameUtils.removeExtension(fileName);
	    		
	    		
	        	docContent.setDocHolder(this.docHolder);
	        	
	        	docContent.setDocContentName(fileNameWithOutExt);
	        	
	        	docContent.setDocFileName(fileName);
	    	   	docContent.setDocFileExtension(fileExt);
	        	docContent.setDocDirPath(txtDirPath.getDataValue());
	        	docContent.setDocContentKeywords(tpSearchKeywords.getDataValue());
			try {
			    docContent.setDocLastModifiedTime(new Date(Files.getLastModifiedTime(selectedFilePath).toMillis()));
			} catch (IOException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
	    		
	        	docContent.setCreatedBy(AppView.getLoginSession().getUserName());
	        		
	        	docContentList.add(docContent);
	    	}
	    }
	    
	}else{
	    // If the persistence is selected 
	    if(!isNewModel){ // in case of Edit
    		
    		for(DocContentBlob c : docContent.getDocContentBlobs()){
        			contentBlob = c;
        	}
        	
        	docContent.setDocHolder(this.docHolder);
        	
        	File selectedFile = null;
        	
           	for(File file : selectedFileList){
	        		selectedFile = file;
	       	}
           
           	if(selectedFile != null){ // If any file is selected for upload
           		
           		Path selectedFilePath = Paths.get(selectedFile.getAbsolutePath());
        		String fileName = selectedFilePath.getFileName().toString();
        		String fileExt = FilenameUtils.getExtension(selectedFilePath.getFileName().toString());
        		//String fileNameWithOutExt = FilenameUtils.removeExtension(fileName);
        		docContent.setDocFileName(fileName);
        		docContent.setDocFileExtension(fileExt);
        		docContent.setDocDirPath(txtDirPath.getDataValue());
			
        		// If any file is selected for upload
        		try {
	    			try {
	    				Path p = Paths.get(selectedFile.getAbsolutePath());
	    				byte[] data = Files.readAllBytes(p);
	    				//contentBlob.setDocContentBlob(CommonFileUtil.getBlobFromByteArray(data));
	    				contentBlob.setDocContentBlob(data);
	    				docContent.setDocLastModifiedTime(new Date(Files.getLastModifiedTime(p).toMillis()));
	    	               } catch (IOException e) {
	    				e.printStackTrace();
	    			}
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
        		
        		
              
           	}
        	
        	docContent.setDocContentName(txtDocContentName.getDataValue());
        	docContent.setDocContentKeywords(tpSearchKeywords.getDataValue());
        	
        	
        	/**
        	 * Content Blob
        	 */
        	contentBlob.setDocContent(docContent);
        
            docContent.setUpdatedBy("Admin User");
        	List<DocContentBlob> docContentBlob = new ArrayList<DocContentBlob>();
        	docContentBlob.add(contentBlob);
        	
        	docContent.setDocContentBlobs(docContentBlob);
        	
    		
    	}else{
    	
    		// New Document can add multiple documents
    		for(File selectedFile : selectedFileList){
	    		
	    		DocContent docContent = new DocContent();
	    		DocContentBlob contentBlob = new DocContentBlob();
	    	
	    		Path selectedFilePath = Paths.get(selectedFile.getAbsolutePath());
	    		String fileName = selectedFilePath.getFileName().toString();
	    		String fileExt = FilenameUtils.getExtension(selectedFilePath.getFileName().toString());
	    		String fileNameWithOutExt = FilenameUtils.removeExtension(fileName);
	    		
	    		
	        	docContent.setDocHolder(this.docHolder);
	        	
	        	docContent.setDocContentName(fileNameWithOutExt);
	        	
	        	docContent.setDocFileName(fileName);
	    	   	docContent.setDocFileExtension(fileExt);
	        	docContent.setDocDirPath(txtDirPath.getDataValue());
	        	docContent.setDocContentKeywords(tpSearchKeywords.getDataValue());
	        	/**
	        	 * Content Blob
	        	 */
	        	try {
	    			try {
	    				byte[] data = Files.readAllBytes(selectedFilePath);
	    				//contentBlob.setDocContentBlob(CommonFileUtil.getBlobFromByteArray(data));
	    				contentBlob.setDocContentBlob(data);
	    				docContent.setDocLastModifiedTime(new Date(Files.getLastModifiedTime(selectedFilePath).toMillis()));
	    			} catch (IOException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}
	    		} catch (Exception e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	        	
	        	docContent.setCreatedBy(AppView.getLoginSession().getUserName());
	        	
	        	contentBlob.setDocContent(docContent);
		        
		        List<DocContentBlob> docContentBlob = new ArrayList<DocContentBlob>();
	        	docContentBlob.add(contentBlob);
	        	
	        	
	        	docContent.setDocContentBlobs(docContentBlob);
	        		
	        	docContentList.add(docContent);
	    	}
    	}
	
	}
    }

    
    
    
    /**
     * Validate form fields
     *
     * @return true if no validate errors
     */
    @Override
    public boolean validateForm() {
    	/*
        if (tfxCode.getDataValue().equals("")) {
            MessageBox.showWarning(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.CodeError"));
            tfxCode.requestFocus();
            return false;
        }
        if (tfxName.getDataValue().equals("")) {
            MessageBox.showWarning(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.NameError"));
            tfxName.requestFocus();
            return false;
        }
        */
        return true;
    }

    @Override
    public String getFormIconPath() {
        return ViewHelpers.ICONS16 + "document.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return I18n.DOCUMENTHOLDER.getString("DocContent.Form.NewTitle");
        } else {
            return I18n.DOCUMENTHOLDER.getString("DocContent.Form.EditTitle");
        }
    }

    @Override
    public void onHelp() {
        MessageBox.showInfo("Help not implemented yet!");
    }

	@Override
	public void disableFields() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSave() {
		// TODO Auto-generated method stub
		
		if(!isNewModel){
		
			super.onSave();
	
		}else{
			
	       if (!validateForm()) {
	            return false;
	        }

	        pushFields();
	        
	        controller.onSave(docContentList);

	        return true;
	    
		}
		
		
		
		return super.onSave();
	}
	
	
	@Override
	public String getAccessFunctionName() {
	    // TODO Auto-generated method stub
	    return FunctionAccessConstants.FN_DOCCONTENT;
	}

	@Override
	public void destroy() {
	    	super.destroy();
	    
	    	selectedFileList =null;
	        liSelectionFileList =null;
	        docContent = null;
	        docHolder = null;
	        contentBlob = null;
	        // form fields
	        txtDocContentSeqNo=null;
	        txtDocContentName=null;
	        tpSearchKeywords=null;
	        txtDirPath=null;
	        btnMultipleUpload=null;
	        btnSingleUpload=null;
	        fileChooser=null;
	        txtFileName=null;
	        txtFileExtension=null;
	        btnOpenDocment=null;
	        liSelectionFileList=null;
	        dtLastModifiedDate=null;
	        
	    
	}
	
	
	
}
