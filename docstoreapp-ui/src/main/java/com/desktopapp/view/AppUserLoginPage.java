package com.desktopapp.view;

import com.desktopapp.component.constants.DocStoreConstants;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.framework.AbstractDataPageView;
import com.desktopapp.framework.EntityTableColumn;
import com.desktopapp.framework.TableColumnCodeDecode;
import com.desktopapp.model.AppUserLogin;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * App User Login Page
 *
 * @author Ratnala Diwakar Choudhury
 */
public class AppUserLoginPage extends AbstractDataPageView<AppUserLogin> {
	
	
	 public AppUserLoginPage() {
			super();
	 
	 }

    public AppUserLoginPage(String searchFilter) {
		super();
		
		this.searchFilter = searchFilter;
	}


	@Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                I18n.APPUSER.getString("AppUser.Page.username"), "userName", String.class, 200));
        
        getTableModel().addColumn(new EntityTableColumn(
                I18n.APPUSER.getString("AppUser.Page.accesslevel"), "fnAccessLevel", String.class, 200,new TableColumnCodeDecode(DocStoreConstants.CD_ACCESSLEVEL)));
    }
    
	
   	@Override
	public void onMouseDoubleClick() {
		controller.onMouseDoubleClickOnTable();
	}


	@Override
    public String getIconPath() {
        return ViewHelpers.ICONS16 + "userlogin.png";
    }

    @Override
    public String getTitle() {
        return I18n.APPUSER.getString("AppUser.Page.AppUserCredentialsTitle");
    }

    @Override
    public String getAccessFunctionName() {
	return FunctionAccessConstants.FN_APPUSER_LOGIN;
    }

}
