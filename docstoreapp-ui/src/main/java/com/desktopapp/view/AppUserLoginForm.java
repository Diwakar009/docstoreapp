package com.desktopapp.view;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.springframework.util.StringUtils;

import net.miginfocom.swing.MigLayout;

import com.desktopapp.component.JCodeDecodeField;
import com.desktopapp.component.JComboBoxExt;
import com.desktopapp.component.JPasswordFieldExt;
import com.desktopapp.component.JTextFieldExt;
import com.desktopapp.component.MessageBox;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.contoller.AppController;
import com.desktopapp.framework.AbstractFormView;
import com.desktopapp.framework.DataPageController;
import com.desktopapp.model.AppUser;
import com.desktopapp.model.AppUserLogin;
import com.desktopapp.service.util.PasswordHash;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * App user login form
 *
 * @author Ratnala Diwakar Choudhury
 */
public class AppUserLoginForm extends AbstractFormView<AppUserLogin> {
	
    private AppUserLogin appUserLogin = null;
    private AppUser appUser = null;
    private boolean isNewModel = false;
    
    
    private JTextFieldExt txtUserName;
    private JPasswordFieldExt txtPassword;
    private JPasswordFieldExt txtCnfrmPassword;
    
    private JTextFieldExt txtSecQuestion;
    private JComboBoxExt cbSecQuestion;
    private JTextFieldExt txtSecQuestionAns;
    private JTextFieldExt txtFncAccessLevel;
    private JComboBoxExt cbFncAccessLevel;
    
    private JCodeDecodeField secQueCodeDeode;
    private JCodeDecodeField fncAccessLevelCodeDeode;
     
    
    /**
     * Gets new instance of country form
     *
     * @param controller country controller
     * @param appUserLogin model
     */
    public AppUserLoginForm(DataPageController<AppUserLogin> controller, AppUserLogin appUserLogin) {
        super(AppController.get().getAppView(), controller);
        this.appUserLogin = appUserLogin;
        if (appUserLogin.getId() == null) {
            isNewModel = true;
        }
    }
    
    /**
     * Gets new instance of country form
     *
     * @param controller country controller
     * @param appUserLogin model
     */
    public AppUserLoginForm(DataPageController<AppUserLogin> controller,AppUser appUser, AppUserLogin appUserLogin) {
        super(AppController.get().getAppView(), controller);
        this.appUserLogin = appUserLogin;
        this.appUser = appUser;
        if (appUserLogin.getId() == null) {
            isNewModel = true;
        }
    }
    

    @Override
    public void buildUI() {
        initComponents();
        
        
        txtUserName = new JTextFieldExt(100);
        txtPassword = new JPasswordFieldExt(50);
        txtSecQuestionAns = new JTextFieldExt(50);
        txtCnfrmPassword= new JPasswordFieldExt(50);
        txtSecQuestion = new JTextFieldExt(50);
        cbSecQuestion = new JComboBoxExt();
        txtFncAccessLevel = new JTextFieldExt(3);
        cbFncAccessLevel = new JComboBoxExt();
        secQueCodeDeode = new JCodeDecodeField(txtSecQuestion,cbSecQuestion,"CD_SECQUES");
        fncAccessLevelCodeDeode = new JCodeDecodeField(txtFncAccessLevel,cbFncAccessLevel,"CD_ACCESSLEVEL");
        
        JPanel outerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
        JPanel firstInnerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_VAR, "[][fill,grow][fill,grow]", ""));
        
        firstInnerPanel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.username")), "gap para");
        firstInnerPanel.add(txtUserName,"span 2, wrap"); // span covers 2 cells and then wraps
        
        firstInnerPanel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.password")), "gap para");
        firstInnerPanel.add(txtPassword,"span 2, wrap"); // span covers 2 cells and then wraps
        
        firstInnerPanel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.cnfrmpassword")), "gap para");
        firstInnerPanel.add(txtCnfrmPassword,"span 2, wrap"); // span covers 2 cells and then wraps
        
        firstInnerPanel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.secQuection")), "gap para");
        firstInnerPanel.add(secQueCodeDeode.getCodeDecodeUIPanel(),"span 2, wrap"); // span covers 2 cells and then wraps
        
        firstInnerPanel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.secAnswer")), "gap para");
        firstInnerPanel.add(txtSecQuestionAns,"span 2, wrap"); // span covers 2 cells and then wraps
        
        firstInnerPanel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.accesslevel")), "gap para");
        firstInnerPanel.add(fncAccessLevelCodeDeode.getCodeDecodeUIPanel(),"span 2, wrap"); // span covers 2 cells and then wraps
        
        outerPanel.add(firstInnerPanel,"align,wrap");

        addPageToForm("", outerPanel);

        
        popFields();
        pack();
        
      
        setSize(430, 300);
    }
    
    /**
    Write a byte array to the given file. 
    Writing binary data is significantly simpler than reading it. 
   */
   void write(byte[] aInput, String aOutputFileName){
     try {
       OutputStream output = null;
       try {
         output = new BufferedOutputStream(new FileOutputStream(aOutputFileName));
         output.write(aInput);
       }
       finally {
         output.close();
       }
     }
     catch(FileNotFoundException ex){
     }
     catch(IOException ex){
     }
   }
    
   

    @Override
    public AppUserLogin getEntity() {
        return appUserLogin;
    }

    @Override
    public void popFields() {
   	this.txtUserName.setDataValue(this.appUserLogin.getUserName());
   	this.txtSecQuestion.setDataValue(this.appUserLogin.getSecQuestion());
	this.txtSecQuestionAns.setDataValue(this.appUserLogin.getSecQuestionAns());
	this.txtFncAccessLevel.setDataValue(this.appUserLogin.getFnAccessLevel());
    }

    @Override
    public void pushFields() {
	this.appUserLogin.setUserName(txtUserName.getDataValue());
	try {
	    if(txtPassword.getPassword() != null && txtPassword.getPassword().length != 0){
		this.appUserLogin.setPassword(PasswordHash.createHash(txtPassword.getPassword()));
	    }
	} catch (NoSuchAlgorithmException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (InvalidKeySpecException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	this.appUserLogin.setSecQuestion(txtSecQuestion.getDataValue());
	this.appUserLogin.setSecQuestionAns(txtSecQuestionAns.getDataValue());
	this.appUserLogin.setFnAccessLevel(txtFncAccessLevel.getDataValue());
	
	appUserLogin.setAppUser(appUser);
    }
    
    
    /**
     * Validate form fields
     *
     * @return true if no validate errors
     */
    @Override
    public boolean validateForm() {
    	/*
        if (tfxCode.getText().equals("")) {
            MessageBox.showWarning(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.CodeError"));
            tfxCode.requestFocus();
            return false;
        }
        if (tfxName.getText().equals("")) {
            MessageBox.showWarning(I18n.DOCUMENTHOLDER.getString("DocHolder.Form.NameError"));
            tfxName.requestFocus();
            return false;
        }
        */
	
	char[] password = txtPassword.getPassword();
	char[] password1 = txtCnfrmPassword.getPassword();
	
	if(!Arrays.equals(password,password1)){
	    MessageBox.showError("Password should be same as confirm password");;
	    txtPassword.requestFocus();
            return false;
	}
	
        return true;
    }

    @Override
    public String getFormIconPath() {
        return ViewHelpers.ICONS16 + "document.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return I18n.APPUSER.getString("AppUser.Form.NewTitle");
        } else {
            return I18n.APPUSER.getString("AppUser.Form.EditTitle");
        }
    }

    @Override
    public void onHelp() {
        MessageBox.showInfo("Help not implemented yet!");
    }

	@Override
	public void disableFields() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getAccessFunctionName() {
	    // TODO Auto-generated method stub
	    return FunctionAccessConstants.FN_APPUSER_LOGIN;
	}

}
