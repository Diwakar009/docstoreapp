package com.desktopapp.view;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

import com.desktopapp.component.JToolBarExt;
import com.desktopapp.component.constants.DocStoreConstants;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.contoller.DocumentContentController;
import com.desktopapp.contoller.DocumentHolderController;
import com.desktopapp.framework.AbstractDataPageView;
import com.desktopapp.framework.EntityTableColumn;
import com.desktopapp.framework.TableColumnCodeDecode;
import com.desktopapp.model.DocHolder;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * Document Holder Page View
 *
 * @author Ratnala Diwakar Choudhury
 */
public class DocumentHolderPage extends AbstractDataPageView<DocHolder> {
	
	private Action acUpload;
	private Action acBackup;
	private Action onRestore;
	private Action acDownload;
	
	
	
	public DocumentHolderPage() {
		super();
		
		this.multipleSelApplicable = true;
		
	}

	@Override
	public void addTableColumns() {
       
        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docName"), "docName", String.class, 200));
      
        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docType"), "docType", String.class, 200
                ,new TableColumnCodeDecode(DocStoreConstants.CD_DOCTYPE)));

        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docTypeOthers"), "docTypeOthers", String.class, 200));
        
        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docOwner"), "docOwner", String.class, 200));
        
        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docCoowner"), "docCoowner", String.class, 200));
        
        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docKeywords"), "docKeywords", String.class, 200));
    }
	
	
	
	@Override
	protected void buildHeaderBarActions() {
		// TODO Auto-generated method stub
		super.buildHeaderBarActions();
		
		acUpload = new AbstractAction(I18n.COMMON.getString("Action.Upload"), 
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "upload.png"))) {
                @Override
                public void actionPerformed(ActionEvent e) {
                	((DocumentHolderController)getController()).onUpload();      	
                }
            };
       acUpload.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Upload"));
       
       acBackup = new AbstractAction(I18n.COMMON.getString("Action.BackUp"), 
               new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "backup.png"))) {
               @Override
               public void actionPerformed(ActionEvent e) {
                   getController().onBackup();
               }
           };
      acBackup.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.BackUp"));
           
   	
      onRestore = new AbstractAction(I18n.COMMON.getString("Action.Restore"), 
              new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "restore.png"))) {
              @Override
              public void actionPerformed(ActionEvent e) {
                  getController().onRestore();
              }
          };
    onRestore.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Restore"));
    
    
    acDownload = new AbstractAction(I18n.COMMON.getString("Action.Download"), 
            new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "download.png"))) {
            @Override
            public void actionPerformed(ActionEvent e) {
                getController().onDownload();
            }
        };
        acDownload.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Download"));
		
		
	}

	
	  @Override
	    public void tableModelChanged() {
		    super.tableModelChanged();
		    
		    if(xtable.getRowCount() > 0){
			acDownload.setEnabled(true);
		    }else{
			acDownload.setEnabled(false);
		    }
		    
		    //((DocumentContentController)getController()).tableModelChanged(xtable);
	    }
	@Override
	protected void addAdditionalTblHeaderButtons(JToolBarExt tbHeader) {
		// TODO Auto-generated method stub
		super.addAdditionalTblHeaderButtons(tbHeader);
		
		tbHeader.addToolButton(ViewHelpers.createToolButton(acBackup, false, false), getAccessFunctionName() + FunctionAccessConstants.BACKUP);
		tbHeader.addToolButton(ViewHelpers.createToolButton(onRestore, false, false),getAccessFunctionName() + FunctionAccessConstants.RESTORE);
		tbHeader.addToolButton(ViewHelpers.createToolButton(acDownload, false, false),getAccessFunctionName() + FunctionAccessConstants.DOWNLOAD);
		tbHeader.addToolButton(ViewHelpers.createToolButton(acUpload, false, false),getAccessFunctionName() + FunctionAccessConstants.UPLOAD);
	}

	@Override
    public String getIconPath() {
        return ViewHelpers.ICONS16 + "folder.png";
    }

    @Override
    public String getTitle() {
        return I18n.DOCUMENTHOLDER.getString("DocHolder.Page.Title");
    }

    @Override
    public String getAccessFunctionName() {
	return FunctionAccessConstants.FN_DOCHOLDER;
    }
    
    

}
