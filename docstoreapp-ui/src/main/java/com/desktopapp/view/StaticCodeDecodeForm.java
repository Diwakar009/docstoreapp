/*
 * Customers Java Swing Application Demo
 *
 * Copyright(c) 2013, devsniper.com
 */
package com.desktopapp.view;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import net.miginfocom.swing.MigLayout;

import com.desktopapp.component.JCodeDecodeField;
import com.desktopapp.component.JComboBoxExt;
import com.desktopapp.component.JTextAreaExt;
import com.desktopapp.component.JTextFieldExt;
import com.desktopapp.component.MessageBox;
import com.desktopapp.component.constants.DocStoreConstants;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.contoller.AppController;
import com.desktopapp.framework.AbstractFormView;
import com.desktopapp.framework.DataPageController;
import com.desktopapp.model.StaticCodeDecode;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * Static Code Decode form view
 *
 * @author Ratnala Diwakar Choudhury
 */
public class StaticCodeDecodeForm extends AbstractFormView<StaticCodeDecode> {

    private StaticCodeDecode staticCodeDecode = null;
    private boolean isNewModel = false;

    // form fields
    
    /*
    private JTextFieldExt tfxCode;
    private JTextFieldExt tfxName;
  
    */
    private JTextAreaExt txtCodeDesc;
    private JTextFieldExt txtCodeName;
    private JTextFieldExt txtCodeValue;
    private JTextFieldExt txtLanguage;
    private JComboBoxExt cbLanguage;
    private JCodeDecodeField cdLanguage;

    /**
     * Gets new instance of country form
     *
     * @param controller country controller
     * @param staticCodeDecode model
     */
    public StaticCodeDecodeForm(DataPageController<StaticCodeDecode> controller, StaticCodeDecode staticCodeDecode) {
        super(AppController.get().getAppView(), controller);
        this.staticCodeDecode = staticCodeDecode;
        if (staticCodeDecode.getId() == null) {
            isNewModel = true;
        }
    }

    @Override
    public void buildUI() {
        initComponents();
        /*
        tfxCode = new JTextFieldExt(10);
        tfxCode.requestFocus();
        tfxName = new JTextFieldExt(50);
        tpNotes = new JTextPane();
        tpNotes.setPreferredSize(new Dimension(50, 100));
        tpNotes.setMargin(new Insets(0, 0, 0, 0));
		*/
        
        txtCodeDesc = new JTextAreaExt(100);
        txtCodeDesc.setPreferredSize(new Dimension(50, 100));
        txtCodeDesc.setMargin(new Insets(0, 0, 0, 0));
        
        txtCodeName = new JTextFieldExt(20);
        
        txtCodeName.setPreferredSize(new Dimension(150, 0));
        txtCodeValue = new JTextFieldExt(20);
        txtLanguage= new JTextFieldExt(5);
        cbLanguage = new JComboBoxExt();
        cdLanguage = new JCodeDecodeField(txtLanguage, cbLanguage, DocStoreConstants.CD_LANGUAGE);
   
        JPanel panel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_VAR, "[][50:100,fill][fill,grow]", ""));

        panel.add(new JLabel(I18n.STATICCODEDECODE.getString("StaticCodeDecode.Form.CodeName")), "gap para");
        panel.add(txtCodeName, "span");

        panel.add(new JLabel(I18n.STATICCODEDECODE.getString("StaticCodeDecode.Form.CodeValue")), "gap para");
        panel.add(txtCodeValue, "span");

        panel.add(new JLabel(I18n.STATICCODEDECODE.getString("StaticCodeDecode.Form.Langauage")), "gap para");
        panel.add(cdLanguage.getCodeDecodeUIPanel(), "span");
        
        panel.add(new JLabel(I18n.STATICCODEDECODE.getString("StaticCodeDecode.Form.CodeDesc")), "gap para");
        panel.add(new JScrollPane(txtCodeDesc,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), "span");
        
        addPageToForm("", panel);

        popFields();
        
        pack();
        setSize(430, 330);
    }

    @Override
    public StaticCodeDecode getEntity() {
        return staticCodeDecode;
    }
    
    public void disableFields(){
    	
    	/*
    	if (isNewModel) {
    		txtCodeName.setAsReadOnly(false);
    		txtCodeValue.setAsReadOnly(false);
    		txtLanguage.setAsReadOnly(false);
    		cdLanguage.setAsReadOnly(false);
    	}else{
    		
    		txtCodeName.setAsReadOnly(true);
    		txtCodeValue.setAsReadOnly(true);
    		txtLanguage.setAsReadOnly(true);
    		cdLanguage.setAsReadOnly(true);
    	}
    	*/
    		
    }

    @Override
    public void popFields() {
    	txtCodeName.setDataValue(staticCodeDecode.getCodeName());
    	txtCodeValue.setDataValue(staticCodeDecode.getCodeValue());
    	txtLanguage.setDataValue(staticCodeDecode.getLanguage());
    	txtCodeDesc.setDataValue(staticCodeDecode.getCodeDesc());
    }

    @Override
    public void pushFields() {
    	
    	staticCodeDecode= new StaticCodeDecode();
    	
    	staticCodeDecode.setCodeName(txtCodeName.getDataValue());
    	staticCodeDecode.setCodeValue(txtCodeValue.getDataValue());
    	staticCodeDecode.setLanguage(txtLanguage.getDataValue());
    	staticCodeDecode.setCodeDesc(txtCodeDesc.getDataValue());
    	
        if (isNewModel) {
            // AppContoller.getAppContoller.getLoggedUser().getName();
            staticCodeDecode.setCreatedBy(AppView.getLoginSession().getUserName());
        } else {
            staticCodeDecode.setUpdatedBy(AppView.getLoginSession().getUserName());
        }
        
  }

    /**
     * Validate form fields
     *
     * @return true if no validate errors
     */
    @Override
    public boolean validateForm() {
    	
    	/*
        if (tfxCode.getDataValue().equals("")) {
            MessageBox.showWarning(I18n.STATICCODEDECODE.getString("StaticCodeDecode.Form.CodeError"));
            tfxCode.requestFocus();
            return false;
        }
        if (tfxName.getDataValue().equals("")) {
            MessageBox.showWarning(I18n.STATICCODEDECODE.getString("StaticCodeDecode.Form.NameError"));
            tfxName.requestFocus();
            return false;
        }
        */
        return true;
    }

    @Override
    public String getFormIconPath() {
        return ViewHelpers.ICONS16 + "code.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return I18n.STATICCODEDECODE.getString("StaticCodeDecode.Form.NewTitle");
        } else {
            return I18n.STATICCODEDECODE.getString("StaticCodeDecode.Form.EditTitle");
        }
    }

    @Override
    public void onHelp() {
        MessageBox.showInfo("Help not implemented yet!");
    }
    
    @Override
	public String getAccessFunctionName() {
	    // TODO Auto-generated method stub
	    return FunctionAccessConstants.FN_STATICCODEDECODE;
	}

}
