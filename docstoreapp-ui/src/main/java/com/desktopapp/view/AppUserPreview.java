package com.desktopapp.view;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import com.desktopapp.component.IconLabel;
import com.desktopapp.framework.AbstractPreviewPanel;
import com.desktopapp.model.AppUser;
import com.desktopapp.util.ViewHelpers;

/**
 * application preview panel
 *
 * @author Ratnala Diwakar Choudhury
 */
public class AppUserPreview extends AbstractPreviewPanel<AppUser> {

    private AppUser appuser = null;

   // private JLabel lblCompanyName;
    private JLabel lblPerson;
    private IconLabel lblLocation;
    private IconLabel lblPhone;
    private IconLabel lblMobile;
    private IconLabel lblEmail;
    private IconLabel lblHomepage;

    public AppUserPreview() {
        super();
    }

    @Override
    public void buildUI() {
        //lblCompanyName = new JLabel();
        lblPerson = new JLabel();
        lblLocation = new IconLabel(new ImageIcon(getClass().getResource(
                ViewHelpers.ICONS16 + "location.png")));

        lblPhone = new IconLabel(new ImageIcon(getClass().getResource(
                ViewHelpers.ICONS16 + "phone.png")));
        lblMobile = new IconLabel(new ImageIcon(getClass().getResource(
                ViewHelpers.ICONS16 + "mobile.png")));

        lblEmail = new IconLabel(new ImageIcon(getClass().getResource(
                ViewHelpers.ICONS16 + "email.png")));
        lblHomepage = new IconLabel(new ImageIcon(getClass().getResource(
                ViewHelpers.ICONS16 + "homepage.png")));

        JPanel panel = new JPanel(new MigLayout("insets 10 20 10 20"));

       // panel.add(lblCompanyName, "wrap");
        panel.add(lblPerson, "wrap");
        panel.add(lblLocation, "wrap");
        panel.add(lblPhone, "wrap");
        panel.add(lblMobile, "wrap");
        panel.add(lblEmail, "wrap");
        panel.add(lblHomepage, "wrap");

        addCenterComponent(panel);
    }

    @Override
    public void popFields() {
        appuser = (AppUser) getEntity();

     //   lblCompanyName.setText(appuser.getCompanyName());
        lblPerson.setText(appuser.getTitle() + " "
                + appuser.getFirstName() + " "
                + appuser.getLastName());

        String location = appuser.getCity();
        
        /*
        if (appuser.getCountry() != null) {
            location = location + ", " + appuser.getCountry().getName();
        }
        */

        lblLocation.setText(location);
        lblPhone.setText(appuser.getPhone());
        lblMobile.setText(appuser.getMobile());
        lblEmail.setText(appuser.getEmail());
        lblHomepage.setText(appuser.getHomepage());
    }

}
