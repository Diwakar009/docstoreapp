package com.desktopapp.view;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import net.miginfocom.swing.MigLayout;

import org.jdesktop.swingx.JXList;

import com.desktopapp.bean.SendEmailBean;
import com.desktopapp.component.JEmailTextField;
import com.desktopapp.component.JMenuExt;
import com.desktopapp.component.JTextAreaExt;
import com.desktopapp.component.JTextFieldExt;
import com.desktopapp.component.JToolBarExt;
import com.desktopapp.component.MessageBox;
import com.desktopapp.component.common.IGenericComponent;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.contoller.AppController;
import com.desktopapp.contoller.worker.BackgroundWorker;
import com.desktopapp.contoller.worker.executor.BackgroundWorkerExecutor;
import com.desktopapp.framework.DataPageController;
import com.desktopapp.framework.GenericFormView;
import com.desktopapp.model.DocContent;
import com.desktopapp.model.DocHolder;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.service.DocumentContentService;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;
import com.desktopapp.view.varifier.EmailAddressInputVerifier;

/**
 * Send Email Form
 *
 * @author Ratnala Diwakar Choudhury
 */
public class SendEmailForm extends GenericFormView {

	
	  protected Action acSendEmail;
	  private JEmailTextField to;
	  private JEmailTextField cc;
	  private JEmailTextField bcc;
	  private JTextFieldExt subject;
	  private JTextAreaExt content;
	  private ArrayList<DocContent> docContents = null;
	  private DocHolder docHolder = null;
	  private DataPageController controller = null;
	  private DocumentContentService service = null;
	  private JXList liAtachmentList;
	  
	  
	  private List<String> toList = null;
	  private List<String> ccList = null;
	  private List<String> bccList = null;
	  private String subjectStr = null;
	  private String contentStr = null;
	  
	    
	/**
     * Gets new instance of country form
     *
     * @param controller country controller
     * @param docContent model
     */
    public SendEmailForm(DataPageController controller,DocHolder holder,ArrayList<DocContent> docContents) {
        super(AppController.get().getAppView(), controller);
        this.controller = controller;
        this.docContents = docContents;
    }
    
    /**
     * Gets new instance of country form
     *
     *
     * @param controller country controller
     * @param docContent model
     */
    public SendEmailForm(DataPageController controller,DocHolder holder) {
        super(AppController.get().getAppView(),controller);
        this.docHolder = docHolder;
        this.controller = controller;
    }
    

    @Override
    public void buildUI() {
    	
    	
        initComponents();
        service = (DocumentContentService)ObjectFactory.getServiceObject(ObjectFactory.DOC_CONTENT_SERVICE);
        
        to = new JEmailTextField("To");
        cc = new JEmailTextField("Cc");
        bcc = new JEmailTextField("Bcc");
        
        subject =new JTextFieldExt("Subject",400);
        content= new JTextAreaExt(8000);
        
        liAtachmentList = new JXList();
        
        to.setMandatory(true);
        subject.setMandatory(true);
        
        content.setPreferredSize(new Dimension(100, 200));
        content.setMargin(new Insets(0, 0, 0, 0));
        
        genericComponentList.add(to);
        genericComponentList.add(cc);
        genericComponentList.add(bcc);
        genericComponentList.add(subject);
        genericComponentList.add(content);
        
        JPanel outerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
        JPanel firstInnerPanel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_VAR, "[][fill,grow][fill,grow]", ""));
        
        // add all elements add in first inner panel
        firstInnerPanel.add(new JLabel("To"), "gap para");
        firstInnerPanel.add(to,"span 2, wrap"); // span covers 2 cells and then wraps 
        
        firstInnerPanel.add(new JLabel("Cc"), "gap para");
        firstInnerPanel.add(cc,"span 2, wrap"); // span covers 2 cells and then wraps
        
        firstInnerPanel.add(new JLabel("Bcc"), "gap para");
        firstInnerPanel.add(bcc,"span 2, wrap"); // span covers 2 cells and then wraps
        
        firstInnerPanel.add(new JLabel("Subject"), "gap para");
        firstInnerPanel.add(subject,"span 2, wrap"); // span covers 2 cells and then wraps
        
        firstInnerPanel.add(new JLabel("Attachments"), "gap para");
        firstInnerPanel.add(new JScrollPane(liAtachmentList), "span");
        
        firstInnerPanel.add(new JLabel("Note"), "gap para");
        firstInnerPanel.add(new JScrollPane(content), "span");
         
        outerPanel.add(firstInnerPanel,"align,wrap");

        addPageToForm("", outerPanel);
        
        popFields();
        pack();
      
        setSize(500, 500);
    }
    

    @Override
	protected void buildFormActions() {
		// TODO Auto-generated method stub
				super.buildFormActions();
		
				acSendEmail = new AbstractAction(I18n.COMMON.getString("Action.Email"),
                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "send-file.png"))) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                       // controller.onRefresh();
                        onSendEmail();
                    }
                };
                acSendEmail.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Close"));
                acSendEmail.putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_S));
                acSendEmail.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F2, InputEvent.ALT_MASK));
		
	}

    protected boolean onSendEmail() {
    	
    	if (!validateForm()) {
            return false;
        }

        pushFields();
        
    	if (MessageBox.showAskYesNo(I18n.COMMON.getString(
                "MessageBox.Confirm.Email")) == MessageBox.YES_OPTION) {
          
      	  try{
      		  
      	  	 Map<String,Object> dataMap = new HashMap<String, Object>();
      	     dataMap.put(BackgroundWorker.WORKER_SERVICE_OBJECT, (DocumentContentService)service);
      	     
      	     dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_CLASS_TYPES, new Class[]{SendEmailBean.class});
      	     dataMap.put(BackgroundWorker.WORKER_SERVICE_PARAM_OBJECTS, new Object[]{new SendEmailBean(toList,ccList,bccList,subjectStr,contentStr,this.docContents)});
      	     dataMap.put(BackgroundWorker.WORKER_SERVICE_METHOD_NAME, "emailAttachments");
      	     dataMap.put(BackgroundWorker.REFRESH_NEEDED, "N");
      	     dataMap.put(BackgroundWorker.SUCCESS_MESSAGE, "Email Successfully Sent");
      	     dataMap.put(BackgroundWorker.ERROR_MESSAGE, "Error In Sending Email");
            	       	  
      	     BackgroundWorkerExecutor taskExecutor = new BackgroundWorkerExecutor(new BackgroundWorker(dataMap,this.controller));
      	     
      	     taskExecutor.executeTask();
      	     return true;
      	      	  
        	}catch(Exception anyException){
        		MessageBox.showError("Error in sending email ",anyException);
        		return false;
        	}
      	  
      	    
        }
    	
		return true;
	}

	@Override
    public void popFields() {
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(DocContent content  : docContents){
    		model.addElement(content.getDocFileName());
    	} 
		liAtachmentList.setModel(model);
	}

    @Override
    public void pushFields() {
    	
    	toList = new ArrayList<String>();
    	ccList = new ArrayList<String>();
    	bccList = new ArrayList<String>();
    	
    	subjectStr = subject.getDataValue();
    	contentStr = content.getDataValue();
    	
    	if(to.getDataValue() != null && !to.getDataValue().isEmpty()){
    		String[] toStringArr = to.getDataValue().split(";");
    		toList = Arrays.asList(toStringArr);
    	}
    	
    	if(cc.getDataValue() != null && !cc.getDataValue().isEmpty()){
    		String[] ccStringArr = cc.getDataValue().split(";");
    		ccList = Arrays.asList(ccStringArr);
    	}
    	
    	if(bcc.getDataValue() != null && !bcc.getDataValue().isEmpty()){
    		String[] bccStringArr = bcc.getDataValue().split(";");
    		bccList = Arrays.asList(bccStringArr);
    	}
    	
    	
    	
    }
    
    @Override
	protected void addToolBarButton(JToolBarExt toolBar2) {
        toolBar.add(ViewHelpers.createToolButton(acSendEmail, true, true));
    }

	@Override
	protected void addMenuAction(JMenuExt mnuFile) {
        mnuFile.add(acSendEmail);
    }

	/**
     * Validate form fields
     *
     * @return true if no validate errors
     */
    @Override
    public boolean validateForm() {
    
    	 for(IGenericComponent iGenericComponent : genericComponentList) {
    			if(iGenericComponent != null){
    			    if(!iGenericComponent.validateInput()){
    			    	return false;
    			    }
    			}
    	 }
    	
        return true;
    }

    @Override
    public String getFormIconPath() {
        return ViewHelpers.ICONS16 + "email.png";
    }

    @Override
    public String getFormTitle() {
           return I18n.COMMON.getString("SendMail.Form.Send");
     }

    @Override
	public void disableFields() {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public String getAccessFunctionName() {
	    // TODO Auto-generated method stub
	    return FunctionAccessConstants.FN_SENDEMAIL;
	}

	@Override
	public void destroy() {
	    	super.destroy();
	    
	}
	
	
	
}
