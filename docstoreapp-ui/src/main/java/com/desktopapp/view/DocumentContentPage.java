package com.desktopapp.view;

import java.awt.event.ActionEvent;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

import org.jdesktop.swingx.JXTable;

import com.desktopapp.component.JToolBarExt;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.contoller.DocumentContentController;
import com.desktopapp.framework.AbstractDataPageView;
import com.desktopapp.framework.EntityTableColumn;
import com.desktopapp.model.DocContent;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * Document Content Page View
 *
 * @author Ratnala Diwakar Choudhury
 */
public class DocumentContentPage extends AbstractDataPageView<DocContent> {
    
    	private Action acDownload;
    	private Action acUpload;
    	
    	private Action acEmail;
    	private Action acPrint;
	
	 public DocumentContentPage() {
			super();
	 
	 }

    public DocumentContentPage(String searchFilter) {
		super();
		
		this.searchFilter = searchFilter;
		this.multipleSelApplicable = true;
	}


	@Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docContentName"), "docContentName", String.class, 200));
        
        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docFileName"), "docFileName", String.class, 200));
        
        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docExtension"), "docFileExtension", String.class, 200));
      
        getTableModel().addColumn(new EntityTableColumn(
                I18n.DOCUMENTHOLDER.getString("DocHolder.Page.docLastModifiedDate"), "docLastModifiedTime", Date.class, 200));
    }
    
	
   	@Override
	protected void buildHeaderBarActions() {
	    // TODO Auto-generated method stub
	    super.buildHeaderBarActions();
	    
	    acUpload = new AbstractAction(I18n.COMMON.getString("Action.Upload"), 
	                new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "upload.png"))) {
	                @Override
	                public void actionPerformed(ActionEvent e) {
	                	((DocumentContentController)getController()).onUpload();      	
	                }
	            };
	    acUpload.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Upload"));
	    
	    
	    acDownload = new AbstractAction(I18n.COMMON.getString("Action.Download"), 
	            new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "download.png"))) {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	                getController().onDownload();
	            }
	        };
	    acDownload.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Download"));
	    
	    
	    acEmail = new AbstractAction(I18n.COMMON.getString("Action.Email"), 
	            new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "email.png"))) {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	                getController().sendEmail();
	            }
	        };
	    acEmail.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Email"));
	    
	    
	    acPrint = new AbstractAction(I18n.COMMON.getString("Action.Print"), 
	            new ImageIcon(getClass().getResource(ViewHelpers.ICONS16 + "print.png"))) {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	                getController().print();
	            }
	        };
	   
	   acPrint.putValue(Action.SHORT_DESCRIPTION, I18n.COMMON.getString("Action.Hint.Print"));
	    
	   
	    
			
	    
	}
   	
   	@Override
	protected void addAdditionalTblHeaderButtons(JToolBarExt tbHeader) {
		super.addAdditionalTblHeaderButtons(tbHeader);
		tbHeader.add(ViewHelpers.createToolButton(acDownload, false, false));
		tbHeader.add(ViewHelpers.createToolButton(acUpload, false, false));
		tbHeader.add(ViewHelpers.createToolButton(acEmail, false, false), getAccessFunctionName() + FunctionAccessConstants.EMAIL);
		tbHeader.add(ViewHelpers.createToolButton(acPrint, false, false), getAccessFunctionName() + FunctionAccessConstants.PRINT);
		
	}

	@Override
	public void onMouseDoubleClick() {
		controller.onMouseDoubleClickOnTable();
	}


	@Override
    public String getIconPath() {
        return ViewHelpers.ICONS16 + "folderopen.png";
    }

    @Override
    public void tableModelChanged() {
	    super.tableModelChanged();
	    
	    if(xtable.getRowCount() > 0){
	    	acDownload.setEnabled(true);
	    	acEmail.setEnabled(true);
	    	acPrint.setEnabled(true);
	    }else{
	    	acDownload.setEnabled(false);
	    	acEmail.setEnabled(false);
	    	acPrint.setEnabled(false);
	    }
	    
	    ((DocumentContentController)getController()).tableModelChanged(xtable);
    }

    @Override
    public String getTitle() {
        return I18n.DOCUMENTHOLDER.getString("DocHolder.Page.DocContentTitle");
    }

    @Override
    public String getAccessFunctionName() {
	return FunctionAccessConstants.FN_DOCCONTENT;
    }
    
 

}
