package com.desktopapp.view.varifier;

import java.awt.Color;

import javax.swing.InputVerifier;
import javax.swing.JComponent;

import com.desktopapp.component.JTextFieldExt;
import com.desktopapp.util.ValidationUtil;

public class EmailAddressInputVerifier extends InputVerifier{
	
	@Override
	public boolean verify(JComponent component) {
		JTextFieldExt textField = (JTextFieldExt)component;
		textField.setText(new String (textField.getDataValue().replaceAll(",", ";")));
		
		if(ValidationUtil.isEmailFormatValid(textField.getDataValue())){
			textField.setBackground(Color.WHITE);	
			return true;
		}
		textField.setBackground(Color.RED);
		return false;
	}

}
