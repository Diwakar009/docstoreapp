package com.desktopapp.view;

import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.framework.AbstractDataPageView;
import com.desktopapp.framework.AbstractPreviewPanel;
import com.desktopapp.framework.EntityTableColumn;
import com.desktopapp.model.AppUser;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * App User Page View
 *
 * @author Ratnala Diwakar CHoudhury
 */
public class AppUserPage extends AbstractDataPageView<AppUser> {

    @Override
    public void addTableColumns() {
     
        getTableModel().addColumn(new EntityTableColumn(
                I18n.APPUSER.getString("AppUser.Page.FirstName"),
                "firstName", String.class, 200));

        getTableModel().addColumn(new EntityTableColumn(
                I18n.APPUSER.getString("AppUser.Page.LastName"),
                "lastName", String.class, 200));
       
        getTableModel().addColumn(new EntityTableColumn(
                I18n.APPUSER.getString("AppUser.Page.ShortName"),
                "shortName", String.class, 200));

        getTableModel().addColumn(new EntityTableColumn(
                I18n.APPUSER.getString("AppUser.Page.Mobile"),
                "mobile", String.class, 150, false, false));

        getTableModel().addColumn(new EntityTableColumn(
                I18n.APPUSER.getString("AppUser.Page.Email"),
                "email", String.class, 150));

       getTableModel().addColumn(new EntityTableColumn(
                I18n.APPUSER.getString("AppUser.Page.Active"),
                "active", Boolean.class, 50, false, false));
    }

    @Override
    public String getIconPath() {
        return ViewHelpers.ICONS16 + "customer.png";
    }

    @Override
    public String getTitle() {
        return I18n.APPUSER.getString("AppUser.Page.Title");
    }

    @Override
    public AbstractPreviewPanel getPreviewPanel() {
        return new AppUserPreview();
    }

    @Override
    public String getAccessFunctionName() {
	return FunctionAccessConstants.FN_APPUSER;
    }

}
