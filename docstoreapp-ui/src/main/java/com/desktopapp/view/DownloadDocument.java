package com.desktopapp.view;

import java.io.IOException;

import com.desktopapp.framework.DataPageController;
import com.desktopapp.model.DocContent;
import com.desktopapp.model.DocContentBlob;
import com.desktopapp.model.DocHolder;
import com.desktopapp.util.CommonFileUtil;
import com.desktopapp.util.UIFileUtil;
import com.dropbox.core.DbxEntry.File;


/**
 * 
 * Download dowument when double click the table row.
 * 
 * @author "Diwakar Choudhury"
 *
 */
public class DownloadDocument {
	
	private DocContent docContent = null;
    private DocHolder docHolder = null;
    private DocContentBlob contentBlob = null;
    private boolean isNewModel = false;
	
	  /**
     * 
     *
     * @param controller country controller
     * @param docContent model
     */
    public DownloadDocument(DataPageController<DocContent> controller,DocHolder docHolder, DocContent docContent) {
        this.docContent = docContent;
        this.docHolder = docHolder;
        
        if (docContent.getId() == null) {
            isNewModel = true;
        }
        
        if(!isNewModel){
	        for(DocContentBlob c : docContent.getDocContentBlobs()){
	        	contentBlob = c;
	        }
        }
        
     
    }

    /**
     * Open document
     * 
     * @return
     */
	public boolean openDocument() {
		
		if(!isNewModel){
			
        		
			if("N".equals(docHolder.getDocPersist())){
			    // Open the file from the location
			    
			    String filePathName = docContent.getDocDirPath() + java.io.File.separator + docContent.getDocFileName();  
			    try {
				UIFileUtil.openFile(filePathName);
				return true;
			    } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			    }
			    
			}else{
			       String fileNameWithOutExt = CommonFileUtil.getFileNameWithoutExtention(docContent.getDocFileName());
			       byte[] blobData = contentBlob.getDocContentBlob();
        		       contentBlob = null;
        		       return UIFileUtil.openByteDataAsFile(blobData, fileNameWithOutExt, docContent.getDocFileExtension());
			}
		}
		
		return false;
	}
   
}
