package com.desktopapp.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.miginfocom.swing.MigLayout;

import com.desktopapp.component.JCodeDecodeField;
import com.desktopapp.component.JComboBoxExt;
import com.desktopapp.component.JTextAreaExt;
import com.desktopapp.component.JTextFieldExt;
import com.desktopapp.component.MessageBox;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.component.constants.UIConstants;
import com.desktopapp.contoller.AppController;
import com.desktopapp.contoller.AppUserLoginController;
import com.desktopapp.framework.AbstractFormView;
import com.desktopapp.framework.DataPageController;
import com.desktopapp.model.AppUser;
import com.desktopapp.objectfactory.ObjectFactory;
import com.desktopapp.spring.util.ApplicationContextUtils;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * App user form view
 *
 * @author Ratnala Diwakar Choudhury
 */
public class AppUserForm extends AbstractFormView<AppUser> {

    private AppUser appUser = null;
    private boolean isNewModel = false;

    // General page
    private JTextFieldExt tfxTitle;
    private JComboBoxExt cbTitle;
    private JCodeDecodeField cdfTitle;
    
    private JTextFieldExt tfxFirstName;
    private JTextFieldExt tfxLastName;
    private JTextFieldExt tfxShortName;
    private JCheckBox chbActive;
    
    // Address page
    private JTextAreaExt tpAddress;
    private JTextFieldExt tfxCity;
    private JTextFieldExt tfxRegion;
    private JTextFieldExt tfxPostalCode;
    
    // Communication page
    private JTextFieldExt tfxPhone;
    private JTextFieldExt tfxMobile;
    private JTextFieldExt tfxFax;
    private JTextFieldExt tfxEmail;
    private JTextFieldExt tfxHomepage;
    private JTextFieldExt tfxSkype;
    
    // Notes page
    private JTextAreaExt tpNotes;
    
    private JPanel appUserLoginPanel;
    
    AppUserLoginController appUserLoginController = null;
    private String searchFilter = "";

    /**
     * Gets new instance of customer form
     *
     * @param controller customer controller
     * @param appUser model
     */
    public AppUserForm(DataPageController<AppUser> controller, AppUser appUser) {
        super(AppController.get().getAppView(), controller);
        this.appUser = appUser;
        if (appUser.getId() == null) {
            isNewModel = true;
        }
    }
    
   
    @Override
    public void buildUI() {
        initComponents();
        // add pages
        addPageToForm(I18n.APPUSER.getString("AppUser.Form.GeneralPage.Title"),
                buildGeneralPage());
        addPageToForm(I18n.APPUSER.getString("AppUser.Form.AddressPage.Title"),
                buildAddressPage());
        addPageToForm(I18n.APPUSER.getString("AppUser.Form.CommunicationPage.Title"),
                buildCommunicationPage());
        addPageToForm(I18n.APPUSER.getString("AppUser.Form.NotesPage.Title"),
                buildNotesPage());
        
        if(!isNewModel){
        	addAppUserLoginPageToForm();
        }
        
        popFields();
        pack();
        
        if(!isNewModel){
        	setSize(675, 600);
        }else{
        	setSize(500, 370);
        }
    }

    
    /**
     * 
     */
    private void addAppUserLoginPageToForm() {
		appUserLoginController = (AppUserLoginController)ApplicationContextUtils.getBean(ObjectFactory.APP_USER_LOGIN_CTRL);
    	appUserLoginController.setAppUser(appUser);
    	appUserLoginController.setSearchFilter(searchFilter);
    	
    	JPanel panel = new JPanel(new MigLayout(UIConstants.MIGLAYOUT_PARAM_1_INSETS_0, "[fill,grow]", ""));
	    panel.add(appUserLoginController.getDataPageView().asComponent(),"span");
	    
	    appUserLoginPanel.add(panel);
   }


	/**
     * General page UI
     *
     * @return general page panel
     */
    private JPanel buildGeneralPage() {
        tfxTitle = new JTextFieldExt(5);
        cbTitle = new JComboBoxExt();
        cdfTitle = new JCodeDecodeField(tfxTitle, cbTitle, "CD_USERTITLE");
        
        tfxFirstName = new JTextFieldExt(50);
        tfxLastName = new JTextFieldExt(50);
        tfxShortName = new JTextFieldExt(50);
        
        genericComponentList.add(tfxTitle);
        genericComponentList.add(cbTitle);
        genericComponentList.add(cdfTitle);
        genericComponentList.add(tfxFirstName);
        genericComponentList.add(tfxLastName);
        genericComponentList.add(tfxShortName);
        
        
        chbActive = new JCheckBox(I18n.APPUSER.getString("AppUser.Form.Active"));

        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][50:100,fill][fill,grow]", ""));

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Title")), "gap para");
        //panel.add(tfxTitle, "span");
        panel.add(cdfTitle.getCodeDecodeUIPanel(), "span");
        
        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.FirstName")), "gap para");
        panel.add(tfxFirstName, "span");

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.LastName")), "gap para");
        panel.add(tfxLastName, "span");
        
        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.ShortName")), "gap para");
        panel.add(tfxShortName, "span");

        panel.add(ViewHelpers.createBoldTitledSeperator(
                I18n.APPUSER.getString("AppUser.Form.ActiveTitle")), "span,growx");

        panel.add(new JLabel(), "gap para");
        panel.add(chbActive, "span");

        return panel;
    }

    /**
     * Address page UI
     *
     * @return addres page panel
     */
    private JPanel buildAddressPage() {
        tpAddress = new JTextAreaExt(100);
        tpAddress.setPreferredSize(new Dimension(20, 80));
        tpAddress.setMargin(new Insets(0, 0, 0, 0));
        tfxCity = new JTextFieldExt(50);
        tfxRegion = new JTextFieldExt(50);
        tfxPostalCode = new JTextFieldExt(50);
        
        genericComponentList.add(tpAddress);
        genericComponentList.add(tfxCity);
        genericComponentList.add(tfxRegion);
        genericComponentList.add(tfxPostalCode);
     
        
      
        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][fill,grow]"));

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Address")), "gap para");
        panel.add(new JScrollPane(tpAddress), "span");

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.City")), "gap para");
        panel.add(tfxCity, "span");

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Region")), "gap para");
        panel.add(tfxRegion, "span");

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.PostalCode")), "gap para");
        panel.add(tfxPostalCode, "span");

        return panel;
    }

    /**
     * Communication page UI
     *
     * @return communication page panel
     */
    private JPanel buildCommunicationPage() {
        tfxPhone = new JTextFieldExt(50);
        tfxMobile = new JTextFieldExt(50);
        tfxFax = new JTextFieldExt(50);
        tfxEmail = new JTextFieldExt(50);
        tfxHomepage = new JTextFieldExt(50);
        tfxSkype = new JTextFieldExt(50);
        
        genericComponentList.add(tfxPhone);
        genericComponentList.add(tfxMobile);
        genericComponentList.add(tfxFax);
        genericComponentList.add(tfxEmail);
        genericComponentList.add(tfxHomepage);
        genericComponentList.add(tfxSkype);
     
        

        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][fill,grow]"));

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Phone")), "gap para");
        panel.add(tfxPhone, "span");

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Mobile")), "gap para");
        panel.add(tfxMobile, "span");

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Fax")), "gap para");
        panel.add(tfxFax, "span");

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Email")), "gap para");
        panel.add(tfxEmail, "span");

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Homepage")), "gap para");
        panel.add(tfxHomepage, "span");

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Skype")), "gap para");
        panel.add(tfxSkype, "span");

        return panel;
    }

    /**
     * Notes page UI
     *
     * @return notes page panel
     */
    private JPanel buildNotesPage() {
        tpNotes = new JTextAreaExt(200);
        tpNotes.setPreferredSize(new Dimension(50, 200));
        tpNotes.setMargin(new Insets(0, 0, 0, 0));
        
        genericComponentList.add(tpNotes);
  
        JPanel panel = new JPanel(new MigLayout("insets 20 10 10 10", "[][fill,grow]"));

        panel.add(new JLabel(I18n.APPUSER.getString("AppUser.Form.Notes")), "gap para");
        panel.add(new JScrollPane(tpNotes), "span");

        return panel;
    }

    @Override
    public AppUser getEntity() {
        return appUser;
    }

    @Override
    public void popFields() {
        // general page
        tfxTitle.setDataValue(appUser.getTitle());
        tfxFirstName.setDataValue(appUser.getFirstName());
        tfxLastName.setDataValue(appUser.getLastName());
        tfxShortName.setDataValue(appUser.getShortName());
        chbActive.setSelected(appUser.getActive());
        // address page
        tpAddress.setDataValue(appUser.getAddress());
        tfxCity.setDataValue(appUser.getCity());
        tfxRegion.setDataValue(appUser.getRegion());
        tfxPostalCode.setDataValue(appUser.getPostalCode());
        // communication page
        tfxPhone.setDataValue(appUser.getPhone());
        tfxMobile.setDataValue(appUser.getMobile());
        tfxFax.setDataValue(appUser.getFax());
        tfxEmail.setDataValue(appUser.getEmail());
        tfxHomepage.setDataValue(appUser.getHomepage());
        tfxSkype.setDataValue(appUser.getSkype());
        // notes page
        tpNotes.setDataValue(appUser.getNotes());
    }

    @Override
    public void pushFields() {
        // general page
    	appUser.setTitle(tfxTitle.getDataValue());
    	appUser.setFirstName(tfxFirstName.getDataValue());
    	appUser.setLastName(tfxLastName.getDataValue());
    	appUser.setShortName(tfxShortName.getDataValue());
        appUser.setActive(chbActive.isSelected());
        // address page
        appUser.setAddress(tpAddress.getText());
        appUser.setCity(tfxCity.getDataValue());
        appUser.setRegion(tfxRegion.getDataValue());
        appUser.setPostalCode(tfxPostalCode.getDataValue());
        // communication page
        appUser.setPhone(tfxPhone.getDataValue());
        appUser.setMobile(tfxMobile.getDataValue());
        appUser.setFax(tfxFax.getDataValue());
        appUser.setEmail(tfxEmail.getDataValue());
        appUser.setHomepage(tfxHomepage.getDataValue());
        appUser.setSkype(tfxSkype.getDataValue());
        // notes page
        appUser.setNotes(tpNotes.getText());

        if (isNewModel) {
            // AppContoller.getAppContoller.getLoggedUser().getName();
            appUser.setCreatedBy(AppView.getLoginSession().getUserName());
        } else {
            appUser.setUpdatedBy(AppView.getLoginSession().getUserName());
        }
    }
    
    
	/*
	 * (non-Javadoc)
	 * @see com.desktopapp.framework.AbstractFormView#addMorePanels()
	 */
    @Override
	protected void addMorePanels() {
		super.addMorePanels();
		appUserLoginPanel = new JPanel(new BorderLayout());
		getCanvasMainPanel().add(appUserLoginPanel);
	}


	/**
     * Form validate
     *
     * @return true if no validate errors
     */
    @Override
    public boolean validateForm() {
       
        if (tfxFirstName.getDataValue().equals("")) {
            MessageBox.showWarning(I18n.APPUSER.getString("AppUser.Form.FirstNameError"));
            tfxFirstName.requestFocus();
            return false;
        }

        if (tfxLastName.getDataValue().equals("")) {
            MessageBox.showWarning(I18n.APPUSER.getString("AppUser.Form.LastNameError"));
            tfxLastName.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public String getFormIconPath() {
        return ViewHelpers.ICONS16 + "customer.png";
    }

    @Override
    public String getFormTitle() {
        if (isNewModel) {
            return I18n.APPUSER.getString("AppUser.Form.NewTitle");
        } else {
            return I18n.APPUSER.getString("AppUser.Form.EditTitle");
        }
    }
    
    @Override
    public boolean isMultiPageForm() {
        return true;
    }

    @Override
    public boolean isPrintable() {
        return true;
    }
    
    @Override
    public void onPrintPreview() {
        MessageBox.showInfo("Print preview not implemented yet!");
    }

    @Override
    public void onPrint() {
        MessageBox.showInfo("Print not implemented yet!");
    }

    @Override
    public void onHelp() {
        MessageBox.showInfo("Help not implemented yet!");
    }

	@Override
	public void disableFields() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String getAccessFunctionName() {
	    // TODO Auto-generated method stub
	    return FunctionAccessConstants.FN_APPUSER;
	}
	
	@Override
	public void destroy() {
	    super.destroy();
	       
	    tfxTitle = null;
	    cbTitle= null;
	    cdfTitle= null;
	    
	    tfxFirstName= null;
	    tfxLastName= null;
	    tfxShortName= null;
	    chbActive = null;
	    
	    // Address page
	    tpAddress= null;
	    tfxCity= null;
	    tfxRegion= null;
	    tfxPostalCode= null;
	    
	    // Communication page
	    tfxPhone= null;
	    tfxMobile= null;
	    tfxFax= null;
	    tfxEmail= null;
	    tfxHomepage= null;
	    tfxSkype= null;
	    
	    // Notes page
	    tpNotes= null;
	    
	    
	    appUserLoginPanel = null;
	    
	}
	

}
