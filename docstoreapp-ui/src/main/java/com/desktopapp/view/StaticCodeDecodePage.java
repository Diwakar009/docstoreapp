package com.desktopapp.view;

import com.desktopapp.component.constants.DocStoreConstants;
import com.desktopapp.component.constants.FunctionAccessConstants;
import com.desktopapp.framework.AbstractDataPageView;
import com.desktopapp.framework.EntityTableColumn;
import com.desktopapp.framework.TableColumnCodeDecode;
import com.desktopapp.model.StaticCodeDecode;
import com.desktopapp.util.I18n;
import com.desktopapp.util.ViewHelpers;

/**
 * Static Code Decode Page View
 *
 * @author Ratnala Diwakar Choudhury
 */
public class StaticCodeDecodePage extends AbstractDataPageView<StaticCodeDecode> {

    @Override
    public void addTableColumns() {
        getTableModel().addColumn(new EntityTableColumn(
                I18n.STATICCODEDECODE.getString("StaticCodeDecode.Page.CodeName"), "id.codeName", String.class, 100));

        getTableModel().addColumn(new EntityTableColumn(
                I18n.STATICCODEDECODE.getString("StaticCodeDecode.Page.Language"), "id.language", String.class, 200,new TableColumnCodeDecode(DocStoreConstants.CD_LANGUAGE)));

        getTableModel().addColumn(new EntityTableColumn(
                I18n.STATICCODEDECODE.getString("StaticCodeDecode.Page.CodeValue"), "id.codeValue", String.class, 200));
        
        getTableModel().addColumn(new EntityTableColumn(
                I18n.STATICCODEDECODE.getString("StaticCodeDecode.Page.CodeDesc"), "codeDesc", String.class, 200));
    }

    @Override
    public String getIconPath() {
        return ViewHelpers.ICONS16 + "code.png";
    }

    @Override
    public String getTitle() {
        return I18n.STATICCODEDECODE.getString("StaticCodeDecode.Page.Title");
    }

    @Override
    public String getAccessFunctionName() {
	return FunctionAccessConstants.FN_STATICCODEDECODE;
    }

}
