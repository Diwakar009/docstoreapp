
 

select
        docholder0_.DOC_ID as DOC1_3_,
        docholder0_.created_at as created2_3_,
        docholder0_.created_by as created3_3_,
        docholder0_.updated_at as updated4_3_,
        docholder0_.updated_by as updated5_3_,
        docholder0_.DOC_COOWNER as DOC6_3_,
        docholder0_.DOC_KEYWORDS as DOC7_3_,
        docholder0_.DOC_NAME as DOC8_3_,
        docholder0_.DOC_OWNER as DOC9_3_,
        docholder0_.DOC_TYPE as DOC10_3_,
        docholder0_.DOC_TYPE_OTHERS as DOC11_3_ 
    from
        doc_holder docholder0_ 
    where
        exists (
            select
                docholder1_.DOC_ID 
            from
                doc_holder docholder1_ cross 
            join
                doc_content doccontent2_ 
            where
                docholder1_.DOC_ID=doccontent2_.DOC_ID 
                and docholder1_.DOC_ID=docholder0_.DOC_ID 
                and (
                    docholder1_.DOC_OWNER like '%Varsha Dependent Pass%'
                ) 
                or docholder1_.DOC_COOWNER like '%Varsha Dependent Pass%' 
                or docholder1_.DOC_TYPE like '%Varsha Dependent Pass%' 
                or docholder1_.DOC_TYPE_OTHERS like '%Varsha Dependent Pass%' 
                or docholder1_.DOC_NAME like '%Varsha Dependent Pass%' 
                or (
                    docholder1_.DOC_KEYWORDS like '%Varsha Dependent Pass%'
                ) 
                or (
                    doccontent2_.DOC_CONTENT_NAME like '%Varsha Dependent Pass%'
                ) 
                or doccontent2_.DOC_CONTENT_KEYWORDS like '%Varsha Dependent Pass%'
        ) 
    order by
        docholder0_.DOC_NAME
        
        
        
         select
        distinct docholder0_.DOC_ID as DOC1_3_,
        docholder0_.created_at as created2_3_,
        docholder0_.created_by as created3_3_,
        docholder0_.updated_at as updated4_3_,
        docholder0_.updated_by as updated5_3_,
        docholder0_.DOC_COOWNER as DOC6_3_,
        docholder0_.DOC_KEYWORDS as DOC7_3_,
        docholder0_.DOC_NAME as DOC8_3_,
        docholder0_.DOC_OWNER as DOC9_3_,
        docholder0_.DOC_TYPE as DOC10_3_,
        docholder0_.DOC_TYPE_OTHERS as DOC11_3_ 
    from
        doc_holder docholder0_ 
    left outer join
        doc_content doccontent1_ 
            on docholder0_.DOC_ID=doccontent1_.DOC_ID 
    where
        docholder0_.DOC_OWNER like '%Fi%' 
        or docholder0_.DOC_COOWNER like '%Fi%' 
        or docholder0_.DOC_TYPE like '%Fi%'
        or docholder0_.DOC_TYPE_OTHERS like '%Fi%' 
        or docholder0_.DOC_NAME like '%Fi%' 
        or (
            docholder0_.DOC_KEYWORDS like '%Fi%'
        ) 
        and (
            doccontent1_.DOC_CONTENT_NAME like '%Fi%'
        ) 
        or doccontent1_.DOC_CONTENT_KEYWORDS like '%Fi%'
    order by
        docholder0_.DOC_NAME
        
        
        
       select
        docholder0_.DOC_ID as DOC1_3_,
        docholder0_.created_at as created2_3_,
        docholder0_.created_by as created3_3_,
        docholder0_.updated_at as updated4_3_,
        docholder0_.updated_by as updated5_3_,
        docholder0_.DOC_COOWNER as DOC6_3_,
        docholder0_.DOC_KEYWORDS as DOC7_3_,
        docholder0_.DOC_NAME as DOC8_3_,
        docholder0_.DOC_OWNER as DOC9_3_,
        docholder0_.DOC_TYPE as DOC10_3_,
        docholder0_.DOC_TYPE_OTHERS as DOC11_3_ 
    from
        doc_holder docholder0_ 
    where
        exists (
            select
                docholder1_.DOC_ID 
            from
                doc_holder docholder1_ 
            left outer join
                doc_content doccontent2_ 
                    on docholder1_.DOC_ID=doccontent2_.DOC_ID 
            where
                docholder1_.DOC_ID=docholder0_.DOC_ID 
                and (
                    docholder1_.DOC_OWNER like '%Varsha Dependent Pass%'
                ) 
                or docholder1_.DOC_COOWNER like '%Varsha Dependent Pass%' 
                or docholder1_.DOC_TYPE like '%Varsha Dependent Pass%' 
                or docholder1_.DOC_TYPE_OTHERS like '%Varsha Dependent Pass%' 
                or docholder1_.DOC_NAME like '%Varsha Dependent Pass%' 
                or (
                    docholder1_.DOC_KEYWORDS like '%Varsha Dependent Pass%'
                ) 
                and (
                    doccontent2_.DOC_CONTENT_NAME like '%Varsha Dependent Pass%'
                ) 
                or doccontent2_.DOC_CONTENT_KEYWORDS like '%Varsha Dependent Pass%'
        ) 
    order by
        docholder0_.DOC_NAME
        
        select
                *
            from
                doc_holder docholder1_ 
            left outer join
                doc_content doccontent2_ 
                    on docholder1_.DOC_ID=doccontent2_.DOC_ID 
            where
                (
                    docholder1_.DOC_OWNER like '%Varsha Dependent Pass%'
                ) 
                or docholder1_.DOC_COOWNER like '%Varsha Dependent Pass%' 
                or docholder1_.DOC_TYPE like '%Varsha Dependent Pass%' 
                or docholder1_.DOC_TYPE_OTHERS like '%Varsha Dependent Pass%' 
                or docholder1_.DOC_NAME like '%Varsha Dependent Pass%' 
                or (
                    docholder1_.DOC_KEYWORDS like '%Varsha Dependent Pass%'
                ) 
                and (
                    doccontent2_.DOC_CONTENT_NAME like '%Varsha Dependent Pass%'
                ) 
                or doccontent2_.DOC_CONTENT_KEYWORDS like '%Varsha Dependent Pass%'
                
                
                
                
                
                
                
                
                
                
        select
        docholder0_.DOC_ID as DOC1_3_,
        docholder0_.created_at as created2_3_,
        docholder0_.created_by as created3_3_,
        docholder0_.updated_at as updated4_3_,
        docholder0_.updated_by as updated5_3_,
        docholder0_.DOC_COOWNER as DOC6_3_,
        docholder0_.DOC_KEYWORDS as DOC7_3_,
        docholder0_.DOC_NAME as DOC8_3_,
        docholder0_.DOC_OWNER as DOC9_3_,
        docholder0_.DOC_TYPE as DOC10_3_,
        docholder0_.DOC_TYPE_OTHERS as DOC11_3_ 
    from
        doc_holder docholder0_ 
    left outer join
        doc_content doccontent1_ 
            on docholder0_.DOC_ID=doccontent1_.DOC_ID 
    where
        docholder0_.DOC_OWNER like '%Varsha Dependent Pass%' 
        or docholder0_.DOC_COOWNER like '%Varsha Dependent Pass%' 
        or docholder0_.DOC_TYPE like '%Varsha Dependent Pass%' 
        or docholder0_.DOC_TYPE_OTHERS like '%Varsha Dependent Pass%' 
        or docholder0_.DOC_NAME like '%Varsha Dependent Pass%' 
        or (
            docholder0_.DOC_KEYWORDS like '%Varsha Dependent Pass%'
        ) 
        and (
            doccontent1_.DOC_CONTENT_NAME like '%Varsha Dependent Pass%'
        ) 
        or doccontent1_.DOC_CONTENT_KEYWORDS like '%Varsha Dependent Pass%' 
    order by
        docholder0_.DOC_NAME
        
        
        
        
        