package com.desktopapp.model;

import java.io.Serializable;

import javax.persistence.*;

import com.desktopapp.framework.IEntity;


/**
 * The persistent class for the static_code_decode database table.
 * 
 */
@Entity
@Table(name="static_code_decode")
@NamedQueries({
    @NamedQuery(name = StaticCodeDecode.FIND_ALL,
            query="SELECT s FROM StaticCodeDecode s ORDER BY s.codeDesc")
    ,
    @NamedQuery(name = StaticCodeDecode.FIND_BY_CODE_NAME_OR_CODE_VAUE_OR_CODE_DESC,
            query="SELECT s FROM StaticCodeDecode s WHERE s.id.codeName LIKE :codeName OR s.id.codeValue LIKE :codeValue OR s.codeDesc like :codeDesc ORDER BY s.codeDesc")
    ,
    @NamedQuery(name = StaticCodeDecode.FIND_BY_CODE_NAME_N_LANG,
            query="SELECT s FROM StaticCodeDecode s WHERE s.id.codeName = :codeName AND s.id.language=:language ORDER BY s.codeDesc")
})
public class StaticCodeDecode extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_ALL = "StaticCodeDecode.findAll";
	public static final String FIND_BY_CODE_NAME_OR_CODE_VAUE_OR_CODE_DESC = "StaticCodeDecode.findByCodeNameORDesc";
	public static final String FIND_BY_CODE_NAME_N_LANG = "StaticCodeDecode.findByCodeNameNLang";
	
	  
	
	@EmbeddedId
	private StaticCodeDecodePK id;

	@Column(name="CODE_DESC", length = 1000)
	private String codeDesc;

	
	public StaticCodeDecode() {
	}
	
	public String getCodeName() {
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		return this.id.getCodeName();
	}
	
	public void setCodeName(String codeName) {
		
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		this.id.setCodeName(codeName);
	}
	public String getLanguage() {
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		return this.id.getLanguage();
	}
	public void setLanguage(String language) {
		
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		this.id.setLanguage(language);
	}
	public String getCodeValue() {
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		return this.id.getCodeValue();
	}
	public void setCodeValue(String codeValue) {
		
		if(getId() == null){
			this.id = new StaticCodeDecodePK();
		}
		
		this.id.setCodeValue(codeValue);
	}
	
	public StaticCodeDecodePK getId() {
		return this.id;
	}

	public void setId(StaticCodeDecodePK id) {
		this.id = id;
	}

	public String getCodeDesc() {
		return this.codeDesc;
	}

	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}

	@Override
	public Object getEntityPK() {
		return getId();
	}

}