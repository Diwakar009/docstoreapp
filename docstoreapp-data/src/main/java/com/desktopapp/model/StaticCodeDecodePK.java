package com.desktopapp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.desktopapp.framework.ICompositeEntityPK;

/**
 * The primary key class for the static_code_decode database table.
 * 
 */
@Embeddable
public class StaticCodeDecodePK implements Serializable,ICompositeEntityPK {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="CODE_NAME", length = 50)
	private String codeName;
	
	@Column(name="LANGUAGE", length = 5)
	private String language;

	@Column(name="CODE_VALUE", length = 50)
	private String codeValue;

	public StaticCodeDecodePK() {
	}
	public String getCodeName() {
		return this.codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getLanguage() {
		return this.language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCodeValue() {
		return this.codeValue;
	}
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof StaticCodeDecodePK)) {
			return false;
		}
		StaticCodeDecodePK castOther = (StaticCodeDecodePK)other;
		return 
			this.codeName.equals(castOther.codeName)
			&& this.language.equals(castOther.language)
			&& this.codeValue.equals(castOther.codeValue);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.codeName.hashCode();
		hash = hash * prime + this.language.hashCode();
		hash = hash * prime + this.codeValue.hashCode();
		
		return hash;
	}
	
	@Override
	public String toString() {
		return "StaticCodeDecodePK [codeName=" + codeName + ", language="
				+ language + ", codeValue=" + codeValue + "]";
	}
	
	
}