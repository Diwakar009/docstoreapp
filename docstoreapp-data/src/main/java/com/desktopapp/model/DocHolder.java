package com.desktopapp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;


/**
 * The persistent class for the doc_holder database table.
 * 
 */
@Entity
@Table(name="doc_holder")
@NamedQueries({
    @NamedQuery(name = DocHolder.FIND_ALL,
            query="SELECT d FROM DocHolder d"),
    /*        
    @NamedQuery(name = DocHolder.FIND_BY_DOC_LIKE_ALL,
            query="SELECT d FROM DocHolder d WHERE d.docOwner LIKE :docOwner OR d.docCoowner LIKE :docCoowner OR d.docType like :docType OR d.docTypeOthers like :docTypeOthers OR d.docName like :docName OR d.docKeywords like :docKeywords ORDER BY d.docName")
     ,*/
   @NamedQuery(name = DocHolder.FIND_BY_DOC_LIKE_ALL,
         query="select distinct d from DocHolder as d left outer join d.docContents as c where d.docOwner LIKE :docOwner OR d.docCoowner LIKE :docCoowner OR d.docType like :docType OR d.docTypeOthers like :docTypeOthers OR d.docName like :docName OR d.docKeywords like :docKeywords or c.docContentName LIKE :docKeywords OR c.docContentKeywords LIKE :docKeywords ORDER BY d.docName")
})
public class DocHolder extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "DocHolder.findAll";
	public static final String FIND_BY_DOC_LIKE_ALL = "DocHolder.findByDocLikeAll";
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Basic(optional = false)
	 @Column(name = "DOC_ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	 private Long id;

	
	@Column(name="DOC_COOWNER", length = 1000)
	private String docCoowner;

	@Column(name="DOC_KEYWORDS", length = 1000)
	private String docKeywords;

	@Column(name="DOC_NAME", length = 100)
	private String docName;

	@Column(name="DOC_OWNER", length = 1000)
	private String docOwner;

	@Column(name="DOC_TYPE", length = 50)
	private String docType;

	@Column(name="DOC_TYPE_OTHERS", length = 100)
	private String docTypeOthers;
	
	@Column(name="DOC_ACCESS", length = 50)
	private String docAccess;
	
	@Column(name="DOC_PERSIST", length = 1)
	private String docPersist;
		

	//bi-directional many-to-one association to DocContent
	@OneToMany(mappedBy="docHolder",fetch=FetchType.LAZY)
	@Cascade(value = org.hibernate.annotations.CascadeType.ALL)
	private List<DocContent> docContents;

	public DocHolder() {
		docContents = new ArrayList<DocContent>();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocCoowner() {
		return this.docCoowner;
	}

	public void setDocCoowner(String docCoowner) {
		this.docCoowner = docCoowner;
	}

	public String getDocKeywords() {
		return this.docKeywords;
	}

	public void setDocKeywords(String docKeywords) {
		this.docKeywords = docKeywords;
	}

	public String getDocName() {
		return this.docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDocOwner() {
		return this.docOwner;
	}

	public void setDocOwner(String docOwner) {
		this.docOwner = docOwner;
	}

	public String getDocType() {
		return this.docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getDocTypeOthers() {
		return this.docTypeOthers;
	}

	public void setDocTypeOthers(String docTypeOthers) {
		this.docTypeOthers = docTypeOthers;
	}

	public List<DocContent> getDocContents() {
		return this.docContents;
	}

	public void setDocContents(List<DocContent> docContents) {
		this.docContents = docContents;
	}

	public DocContent addDocContent(DocContent docContent) {
		getDocContents().add(docContent);
		docContent.setDocHolder(this);

		return docContent;
	}

	public DocContent removeDocContent(DocContent docContent) {
		getDocContents().remove(docContent);
		docContent.setDocHolder(null);

		return docContent;
	}

	@Override
	public Object getEntityPK() {
		return getId();
	}

	public String getDocAccess() {
	    return docAccess;
	}

	public void setDocAccess(String docAccess) {
	    this.docAccess = docAccess;
	}
	
	

	public String getDocPersist() {
	    return docPersist;
	}

	public void setDocPersist(String docPersist) {
	    this.docPersist = docPersist;
	}

	@Override
	public String toString() {
	    return "DocHolder [id=" + id + ", docCoowner=" + docCoowner
		    + ", docKeywords=" + docKeywords + ", docName=" + docName
		    + ", docOwner=" + docOwner + ", docType=" + docType
		    + ", docTypeOthers=" + docTypeOthers + ", docAccess="
		    + docAccess + ", docPersist=" + docPersist
		    + ", docContents=" + docContents + "]";
	}

	

	
	
}