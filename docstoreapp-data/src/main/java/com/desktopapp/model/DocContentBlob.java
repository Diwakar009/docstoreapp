package com.desktopapp.model;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;

import com.desktopapp.framework.IEntity;


/**
 * The persistent class for the doc_content_blob database table.
 * 
 */
@Entity
@Table(name="doc_content_blob")
@NamedQuery(name="DocContentBlob.findAll", query="SELECT d FROM DocContentBlob d")
public class DocContentBlob implements Serializable,IEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "DOC_CONTENT_BLOB_ID", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;
	
	@Lob
	@Column(name="DOC_CONTENT")
	private byte[] docContentBlob;
	//private Blob docContentBlob;

	//bi-directional many-to-one association to DocContent
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DOC_CONTENT_ID")
	private DocContent docContent;
	

	public DocContentBlob() {
	}

	public Long getId() {
		return this.id;
	}

	public void setid(Long id) {
		this.id = id;
	}

	public byte[] getDocContentBlob() {
		return this.docContentBlob;
	}

	public void setDocContentBlob(byte[] docContent) {
		this.docContentBlob = docContent;
	}
	
	public DocContent getDocContent() {
		return this.docContent;
	}

	/*
	public Blob getDocContentBlob() {
	    return docContentBlob;
	}

	public void setDocContentBlob(Blob docContentBlob) {
	    this.docContentBlob = docContentBlob;
	}
	*/

	public void setDocContent(DocContent docContent) {
		this.docContent = docContent;
	}

	@Override
	public Object getEntityPK() {
		// TODO Auto-generated method stub
		return getId();
	}


	

}