package com.desktopapp.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the app_user database table.
 * 
 */
@Entity
@Table(name = "app_user")
@NamedQueries({
		@NamedQuery(name = AppUser.FIND_ALL, query = "SELECT a FROM AppUser a"),
		/*
		 * @NamedQuery(name = DocHolder.FIND_BY_DOC_LIKE_ALL, query=
		 * "SELECT d FROM DocHolder d WHERE d.docOwner LIKE :docOwner OR d.docCoowner LIKE :docCoowner OR d.docType like :docType OR d.docTypeOthers like :docTypeOthers OR d.docName like :docName OR d.docKeywords like :docKeywords ORDER BY d.docName"
		 * ) ,
		 */
		@NamedQuery(name = AppUser.FIND_BY_USER_LIKE_ALL, query = "select distinct d from AppUser as d left outer join d.appUserLogins as c where d.firstName LIKE :firstName OR d.lastName LIKE :lastName OR d.shortName like :shortName OR d.phone like :phone OR d.email like :email or c.userName LIKE :userName ORDER BY d.shortName") })
public class AppUser extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "AppUser.findAll";
	public static final String FIND_BY_USER_LIKE_ALL = "AppUser.findByUserLikeAll";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "appuser_id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	@Basic(optional = false)
	@Column(name = "active", nullable = false)
	private boolean active;

	@Lob
	@Column(name = "address", length = 65535)
	private String address;

	@Column(name = "city", length = 50)
	private String city;

	@Column(name = "first_name", length = 50)
	private String firstName;

	@Column(name = "last_name", length = 50)
	private String lastName;

	@Column(name = "title", length = 50)
	private String title;

	@Column(name = "fax", length = 50)
	private String fax;

	@Column(name = "mobile", length = 50)
	private String mobile;

	@Column(name = "email", length = 50)
	private String email;

	@Column(name = "homepage", length = 50)
	private String homepage;

	@Lob
	@Column(name = "notes", length = 65535)
	private String notes;

	@Column(name = "phone", length = 50)
	private String phone;

	@Column(name = "postal_code", length = 50)
	private String postalCode;

	@Column(name = "region", length = 50)
	private String region;

	@Column(name = "short_name", length = 100)
	private String shortName;

	@Column(name = "skype", length = 50)
	private String skype;

	// bi-directional many-to-one association to AppUserLogin
	@OneToMany(mappedBy = "appUser")
	private List<AppUserLogin> appUserLogins;

	public AppUser() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getHomepage() {
		return this.homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getSkype() {
		return this.skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<AppUserLogin> getAppUserLogins() {
		return this.appUserLogins;
	}

	public void setAppUserLogins(List<AppUserLogin> appUserLogins) {
		this.appUserLogins = appUserLogins;
	}

	public AppUserLogin addAppUserLogin(AppUserLogin appUserLogin) {
		getAppUserLogins().add(appUserLogin);
		appUserLogin.setAppUser(this);

		return appUserLogin;
	}

	public AppUserLogin removeAppUserLogin(AppUserLogin appUserLogin) {
		getAppUserLogins().remove(appUserLogin);
		appUserLogin.setAppUser(null);

		return appUserLogin;
	}

	@Override
	public Object getEntityPK() {
		// TODO Auto-generated method stub
		return getId();
	}

}