

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.PreencodedMimeBodyPart;

import com.desktopapp.component.common.Attachment;
import com.desktopapp.service.common.IEmailGatewayService;

public class EmailGatewayAdapterImpl {

	public void send(String from, String to, List<String> ccList,
			List<String> bccList, String subject, String body,
			Collection<Attachment> attachments) {

		
		try {
			
				InternetAddress sender = from != null ? new InternetAddress(from):null;
			
				List<InternetAddress> allReceipents = new ArrayList<InternetAddress>();
				
				InternetAddress toRecipient = new InternetAddress(to);
				allReceipents.add(toRecipient);
				
				List<InternetAddress> ccAddressList = new ArrayList<InternetAddress>();
				List<InternetAddress> bccAddressList = new ArrayList<InternetAddress>();
				
				if(ccList != null && !ccList.isEmpty()){
					for(String cc: ccList){
						ccAddressList.add(new InternetAddress(cc));
					}
					
					allReceipents.addAll(ccAddressList);
				}
				
				if(bccList != null && !bccList.isEmpty()){
					for(String bcc: ccList){
						bccAddressList.add(new InternetAddress(bcc));
					}
					
					allReceipents.addAll(bccAddressList);
				}
			
				//Session session = Session.getInstance(props); // This should not be null
				
				// Gmail via TLS
				
				// Yahoo Session
				final String username = "diwakarchoudhury";
				final String password = "Machli";
				
				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", "smtp.mail.yahoo.com");
				props.put("mail.smtp.reportsuccess","true");
				props.put("mail.debug", "false");
				props.put("mail.smtp.port", "587");

				Session session = Session.getInstance(props,
						new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			    });
				
				/*
				final String username = "diwakar009";
				final String password = "machli";
				
				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", "smtp.gmail.com");
				props.put("mail.smtp.reportsuccess","true");
				props.put("mail.debug", "false");
				props.put("mail.smtp.port", "587");

				Session session = Session.getInstance(props,
						new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			    });
				*/
			
				// Gmail via SSL
				/*
				Properties props = new Properties();
				props.put("mail.smtp.host", "smtp.gmail.com");
				props.put("mail.smtp.socketFactory.port", "465");
				props.put("mail.smtp.socketFactory.class",
						"javax.net.ssl.SSLSocketFactory");
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.port", "465");

				Session session = Session.getDefaultInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username,password);
						}
					});
				
				*/
				
				Iterator<Attachment> iter = attachments.iterator();
				Collection<Attachment> embeddedObjects = new ArrayList<>();
				Collection<Attachment> properAttachments = new ArrayList<>();
			
				while(iter.hasNext()){
					Attachment attachment = iter.next();
					if(attachment.isInline() && attachment.getContentId() != null && !attachment.getContentId().isEmpty()){
						embeddedObjects.add(attachment);
					}else{
						properAttachments.add(attachment);
					}
				}
				
				MimeMessage mail = buildMail(session,sender,ccAddressList.toArray(new InternetAddress[ccAddressList.size()]),bccAddressList.toArray(new InternetAddress[bccAddressList.size()]),toRecipient,subject,body,embeddedObjects,properAttachments);
			
				
				Transport.send(mail,allReceipents.toArray(new InternetAddress[allReceipents.size()]));
			
		}catch(SendFailedException fe){
			fe.fillInStackTrace();
			Address[] invalidAddress = fe.getInvalidAddresses();
			for(Address a : invalidAddress){	
				System.out.println("Invalid Address " + a.toString());
			}
			
		}catch (MessagingException e) {
			throw new RuntimeException(e); 
		}
	}
	
	private static MimeMessage buildMail(Session session, InternetAddress sender, InternetAddress[] ccAddress,InternetAddress[] bccAddress,InternetAddress toRecipient,String subject,String htmlBody,Collection<Attachment> embededObjects,Collection<Attachment> attachments) throws MessagingException{
		
		MimeMessage message = new MimeMessage(session);
		
		message.setSubject(subject);
		
		if(sender != null){
			message.setFrom(sender);
			message.setSender(sender);
		}else if(session.getProperties() != null && session.getProperties().containsKey("mail.from")){
			sender = new InternetAddress((String)session.getProperties().get("mail.from"));
			message.setFrom(sender);
			message.setSender(sender);
		}
		
		message.setRecipient(RecipientType.TO,toRecipient);
		
		if(ccAddress.length > 0){
			message.setRecipients(RecipientType.CC,ccAddress);
		}
		
		if(bccAddress.length > 0){
			message.setRecipients(RecipientType.BCC,bccAddress);
		}
		
		message.setSentDate(new Date());
		
		/**
		 * Fill in the body
		 * 
		 */
		
		if(embededObjects != null && embededObjects.size() > 0){ // HTML with embeded objects
			
			MimeMultipart multipart = createdRelaed("gmail.com",htmlBody,embededObjects);
			
			if(attachments != null && attachments.size() > 0){
				multipart = createMixed(asBody(multipart),attachments);
			}
			
			message.setContent(multipart);
			
		} else if(attachments != null && attachments.size() > 0){
			
			MimeBodyPart bodyPart = new MimeBodyPart();
			bodyPart.setText(htmlBody,"utf-8","html");
			bodyPart.setDisposition(Part.INLINE);
			message.setContent(createMixed(bodyPart,attachments));
		}else{
			message.setText(htmlBody,"utf-8","html");
		}
		
		message.saveChanges();

		return message;
		
	}

	private static MimeMultipart createMixed(MimeBodyPart firstBodyPart,
			Collection<Attachment> attachments) throws MessagingException {
		MimeMultipart mixed = new MimeMultipart();
		mixed.addBodyPart(firstBodyPart);
		
		for(Attachment part : attachments){
			mixed.addBodyPart(makeBody(part.getMimeType(),Part.ATTACHMENT,wrap72(part.getContent()),UUID.randomUUID().toString(),part.getFilename()));
		}
		return mixed;
	}

	/**
	 * 
	 * Create a body part
	 * 
	 * @param mimeType
	 * @param attachment
	 * @param content base 64 encoded
	 * @param string
	 * @param filename
	 * @return
	 * @throws MessagingException 
	 */
	private static BodyPart makeBody(String mimeType, String disposition,
			String content, String cid, String filename) throws MessagingException {
		
		PreencodedMimeBodyPart preencodedMimeBodyPart = new PreencodedMimeBodyPart("base64");
		
		preencodedMimeBodyPart.setText(content);
		preencodedMimeBodyPart.setContentID(cid);
		preencodedMimeBodyPart.setDisposition(disposition);
		
		if(filename != null){
			preencodedMimeBodyPart.setFileName(filename);
		}
		
		preencodedMimeBodyPart.setHeader("Content-Type", mimeType);
		return preencodedMimeBodyPart;
	}

	private static MimeBodyPart asBody(MimeMultipart multipart) throws MessagingException {
		MimeBodyPart wrap = new MimeBodyPart();
		wrap.setContent(multipart);
		return wrap;
	}

	private static MimeMultipart createdRelaed(String senderHost, String body,
			Collection<Attachment> embededObjects) throws MessagingException {
		
		String htmlCID = UUID.randomUUID().toString() + "@" + senderHost;
		MimeBodyPart htmlPart = new MimeBodyPart();
		htmlPart.setText(body,"utf-8","html");
		htmlPart.setContentID("<" + htmlCID+ ">");
		htmlPart.setDisposition(Part.INLINE);
		MimeMultipart related = new MimeMultipart("related; type=\"text/html\"; start= \"<" +htmlCID+ ">\"");
		related.addBodyPart(htmlPart);
		for(Attachment part: embededObjects){
			related.addBodyPart(makeBody(part.getMimeType(),Part.INLINE,wrap72(part.getContentId()),part.getContentId(),null));
		}
		
		return related;
		
	}
	
	
	/**
	 * Format string to conform to 72 character width
	 * 
	 * @param in 
	 * @return string hard warraped at 72 characters
	 */
	private static String wrap72(String in){
		
		final int length = 72;
		int i =0;
		int j=length;
		
		StringBuilder out = new StringBuilder(in.length() * 74/72);
		
		while ( j < in.length()){
			out.append(in.substring(i,j)).append("\r\n");
			i=j;
			j+=length;
		}
		
		if(i < in.length() - 1){
			out.append(in.substring(i));
		}
		
		return out.toString();
		
	}
	

}
