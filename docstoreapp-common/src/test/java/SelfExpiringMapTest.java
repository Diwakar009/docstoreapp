import org.junit.Test;

import com.desktopapp.util.SelfExpiringHashMap;


public class SelfExpiringMapTest {
	
	@Test
	public void testSelfExpiringMap(){
	
		SelfExpiringHashMap<String, String> cacheMap = new SelfExpiringHashMap<String, String>(3000, 10);
		
		cacheMap.put("key1", "value1");
		cacheMap.put("key2", "value2");
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		cacheMap.put("key3", "value3");
		
		System.out.println("key1 :" + cacheMap.get("key1"));
		System.out.println("key2 :" + cacheMap.get("key2"));
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("cacheMap size :" + cacheMap.size());
		
		System.out.println("key3 :" + cacheMap.get("key3"));
		System.out.println("key1 :" + cacheMap.get("key1"));
		
		
		
		
	}

}
