import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ValidateEmailAddress {

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("(([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)(\\s*;\\s*|\\s*$))*");

	public static boolean validate(String emailStr) {
		        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
		        return matcher.matches();
	}
	
	public static void main(String args[]){
		String emailAddress = "diwakar009@gmail.com;diwakarchoudhury@yahoo.com;anshu@gmail.com";
		System.out.println("Validate Email Address" + validate(emailAddress));
		
	}
}
