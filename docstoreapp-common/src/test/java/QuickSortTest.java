
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import com.desktopapp.util.CommonUtil;
import com.desktopapp.util.QuickSort;
 
public class QuickSortTest {
    public static void main(String[] args) {
        final int SIZE = 10;
 
        List<String> myList = new ArrayList<String>(SIZE);
 
        for (int i=0; i<SIZE; i++){
            int value = (int) (Math.random() * 100);
            myList.add(String.valueOf(value));
        }
 
        CommonUtil.quickSort(myList);
        
        for (String i : myList) {
	    System.out.println(i);
        }
        
    }
}