package com.destopapp.datamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Group Data model
 * 
 * @author "Diwakar Choudhury"
 *
 * @param <K1>
 * @param <K2>
 * @param <DATA>
 */
public class GroupDataModel<K1,K2,DATA> extends HashMap<K1,HashMap<K2,List<DATA>>>{

    
    /**
     * Put the data in the group model
     * 
     * @param k1
     * @param k2
     * @param data
     */
    public void put(K1 k1,K2 k2,DATA data){
	
	HashMap<K2,List<DATA>> map = null;
	List<DATA> list = null;
	
	if(containsKey(k1)){
	    map = get(k1);
	}else{
	    map = new HashMap<K2,List<DATA>>();
	}
	    
	
	if(map.containsKey(k2)){
	    list = map.get(k2);
	    list.add(data);
	}else{
	    list = new ArrayList<DATA>();
	    list.add(data);
	}
	
	map.put(k2, list);
	put(k1,map);
    }
    
    public List<DATA> getStringList(K1 k1,K2 k2){
	
	if(containsKey(k1)){
	    HashMap<K2,List<DATA>> map = get(k1);
	    if(map.containsKey(k2)){
		return map.get(k2);
	    }
	}
	
	return null;
    }
    
    public boolean containsKey(K1 k1,K2 k2){
	
	if(containsKey(k1)){
	    HashMap<K2,List<DATA>> map = get(k1);
	    if(map.containsKey(k2)){
		return true;
	    }
	}
	
	return false;
    }
    
}
