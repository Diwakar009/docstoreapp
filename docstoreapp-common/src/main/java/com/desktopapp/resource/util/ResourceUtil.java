package com.desktopapp.resource.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.desktopapp.template.helper.TemplateHelper;

public class ResourceUtil {
	
	public static final String PROPERTY_FILE = "desktopapp.properties";
	
	private static Properties properties = null;
	
	private static Properties getProperties() {
		
		if(properties == null){
			
		properties = new Properties();
			
    		InputStream  inputStream = ResourceUtil.class.getClassLoader().getResourceAsStream(PROPERTY_FILE);
    		try{
	    		if (inputStream != null) {
	    			properties.load(inputStream);
	    		} else {
	    			throw new FileNotFoundException("property file '" + PROPERTY_FILE + "' not found in the classpath");
	    		}
	    		
	    	}catch(Exception anyException){
    			anyException.printStackTrace();
    		}
		}
		
		return properties;
	}
	
	
	/**
	 * gets the Property value
	 * 
	 * @param property
	 * @return
	 */
	public static String getPropertyValue(String property){
		Properties properties = getProperties();
		
		if(properties != null){
			return (String)getProperties().get(property);	
		}else{
			return null;
		}
		
	}
	
	/**
	 * Get resource content
	 * 
	 * Make sure that u add the extension file in pom.xml or the file will not be visible in classpath
	 * 
	 * @param resourceName
	 * @return
	 */
	public static String getResourceContent(String resourceName){
		
		String resourceContent = "";
		
		InputStream  inputStream = ResourceUtil.class.getClassLoader().getResourceAsStream(resourceName);
		try{
    		if (inputStream != null) {
    			resourceContent = IOUtils.toString(inputStream, "UTF-8"); 
    		} else {
    			throw new FileNotFoundException("property file '" + resourceName + "' not found in the classpath");
    		}
    		
    	}catch(Exception anyException){
			anyException.printStackTrace();
		}finally{
			if(inputStream != null){
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return resourceContent;
	
		
	}
		

}
