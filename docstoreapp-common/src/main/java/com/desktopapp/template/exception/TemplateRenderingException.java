package com.desktopapp.template.exception;

public class TemplateRenderingException extends TemplateException{

	public TemplateRenderingException(String exceptionMessage) {
		super(exceptionMessage);
		// TODO Auto-generated constructor stub
	}

	public TemplateRenderingException(String exceptionMessage, Throwable cause) {
		super(exceptionMessage, cause);
		// TODO Auto-generated constructor stub
	}
	

}
