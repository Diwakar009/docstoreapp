package com.desktopapp.template;

import java.util.Locale;

import org.apache.velocity.Template;

import com.desktopapp.component.common.ContentType;

public final class ParsedTemplate {
	
	private final Template templateObject;
	private final ContentType templateContentType;
	private final Locale tempalteLocale;
	public ParsedTemplate(Template templateObject,
			ContentType templateContentType, Locale tempalteLocale) {
		super();
		this.templateObject = templateObject;
		this.templateContentType = templateContentType;
		this.tempalteLocale = tempalteLocale;
	}
	public String getTemplateName(){
		return templateObject.getName();
	}
	public Template getTemplateObject() {
		return templateObject;
	}
	public ContentType getTemplateContentType() {
		return templateContentType;
	}
	public Locale getTempalteLocale() {
		return tempalteLocale;
	}
	public boolean isHtml(){
		return ContentType.HTML.equals(templateContentType);
	}
	@Override
	public String toString() {
		return "ParsedTemplate [templateObject=" + templateObject
				+ ", templateContentType=" + templateContentType
				+ ", tempalteLocale=" + tempalteLocale + "]";
	}
	
	
	
	
	
	

}
