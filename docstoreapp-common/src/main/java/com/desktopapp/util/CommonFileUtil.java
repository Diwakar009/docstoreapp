package com.desktopapp.util;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.UserPrincipal;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;

public class CommonFileUtil {
	
	
	public static final String FILETYPEEXT_PDF = ".pdf";
	
	
	/**
	 * 
	 * get byte array from blob
	 * 
	 * @param blob
	 * @return
	 */
	public static byte[] getByteArrayFromBlob(Blob blob){
	    int blobLength;
	    try {
		blobLength = (int) blob.length();
		byte[] blobAsBytes = blob.getBytes(1, blobLength);
		//blob.free(); //release the blob and free up memory. (since JDBC 4.0)
		return blobAsBytes;
	    } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    } 
	    
	    return null;
	}
	
	
	/**
	 * Get Blob from byte array
	 * 
	 * @param byteArray
	 * @return
	 */
	public static Blob getBlobFromByteArray(byte[] byteArray){
	  
	    try {
		return new javax.sql.rowset.serial.SerialBlob(byteArray);
	    } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	    
	    return null;
	    
	}
	
	
	/**
	 * Returns File Name
	 * 
	 * @param file
	 * @return
	 */
	public static String getFileName(File file){
		Path path = Paths.get(file.getAbsolutePath());
		return path.getFileName().toString();
	}
	
	/**
	 * Returns filename with out extention.
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getFileNameWithoutExtention(String fileName){
		return FilenameUtils.removeExtension(fileName);
	}
	
	
	/**
	 * get file owner name
	 * 
	 * @param file
	 * @return
	 */
	public static String getFileOwner(File file){
		
		try {
			UserPrincipal userPrincipal = Files.getOwner(file.toPath());
			
			return userPrincipal.getName();
		} catch (Exception e) {
			return null;
		}
		
	}
	
	
	/**
	 * Returns map.
	 * 
	 * folder path as key and list of files as values 
	 * 
	 * Directory Name --> List of Files
	 * 
	 * .filter((p -> !p.toString().equals(directoryPath))) provides you addtional exceptional criteria.
	 * 
	 * @param directoryPath
	 * @return
	 * @throws Exception
	 */
	public static Map<String,List<Path>> getDirectoryFileMap(String directoryPath) throws Exception {
		
		Map<String,List<Path>> fileMap = new LinkedHashMap<String, List<Path>>();
		
		fileMap.put(directoryPath, new ArrayList<Path>()); // For parent directory
	
		Files.walk(Paths.get(directoryPath)) 
			 .forEach(filePath -> {
		
				if(Files.isDirectory(filePath)){
					fileMap.put(filePath.toString(), new ArrayList<Path>());
				}
			
				if(Files.isRegularFile(filePath)) {
					ArrayList<Path> list = (ArrayList<Path>)fileMap.get(filePath.getParent().toString());
					list.add(filePath);
					fileMap.put(filePath.getParent().toString(), list);
				}
		});
	
		
		return fileMap;
	}
	
	

}
