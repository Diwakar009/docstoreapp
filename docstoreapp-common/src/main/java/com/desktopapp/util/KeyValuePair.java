package com.desktopapp.util;

import java.util.Map;
import java.util.Objects;

import scala.collection.mutable.StringBuilder;

public final class KeyValuePair implements Map.Entry<String, Object>{
	
	private final String key;
	private final Object value;
	
	public KeyValuePair(String key, Object value) {
		super();
		this.key = Objects.requireNonNull(key, "Null 'key' argument passed to keyValuePair constructor");
		this.value = value;
	}
	@Override
	public String getKey() {
		return key;
	}
	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return value;
	}
	@Override
	@Deprecated
	public Object setValue(Object value) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Cannot update the value of a KeyValuePair instance");
	}
	@Override
	public int hashCode() {
		return key.hashCode() ^ Objects.hashCode(value);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyValuePair other = (KeyValuePair) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return new StringBuilder(100).append(key).append('=').append(value).toString();
	}

}
