package com.desktopapp.objectfactory;

import com.desktopapp.spring.util.ApplicationContextUtils;

public class ObjectFactory {
	
	/**
	 * Controller Object
	 */
	public static final String STATIC_CODE_DECODE_CTRL = "staticCodeDecodeController";
	public static final String DOC_HOLDER_CTRL = "documentHolderController";
	public static final String DOC_CONTENT_CTRL = "documentContentController";
	public static final String APP_USER_CTRL ="appUserController";
	public static final String APP_USER_LOGIN_CTRL ="appUserLoginController";
	
	/**
	 * Service Object
	 */
	public static final String DOC_HOLDER_SERVICE=" documentHolderService";
	public static final String DOC_CONTENT_SERVICE=" documentContentService";
	public static final String STATIC_CODE_DECODE_SERVICE=" staticCodeDecodeService";
	public static final String APP_USER_SERVICE ="appUserService";
	public static final String APP_USER_LOGIN_SERVICE ="appUserLoginService";
	public static final String APP_USER_FUNCTION_ACC_SERVICE ="appUserFunctionAccessService";
	public static final String APP_USER_UI_SETTINGS_SERVICE = "appUserUISettingsService";
	public static final String EMAIL_GATEWAY_SERVICE=" emailGatewayService";
	
	/**
	 * Return controller object
	 * 
	 * @param objectName
	 * @return
	 */
	public static Object getControllerObject(String objectName){
		if(STATIC_CODE_DECODE_CTRL.equals(objectName)){
			return ApplicationContextUtils.getBean("staticCodeDecodeController");
		}else if(DOC_HOLDER_CTRL.equals(objectName)){
			return ApplicationContextUtils.getBean("documentHolderController");
		}else if(DOC_CONTENT_CTRL.equals(objectName)){
			return ApplicationContextUtils.getBean("documentContentController");
		}else if(APP_USER_CTRL.equals(objectName)){
			return ApplicationContextUtils.getBean("appUserController");
		}else if(APP_USER_LOGIN_CTRL.equals(objectName)){
			return ApplicationContextUtils.getBean("appUserLoginController");
		}else{
			return null;
		}
	}
	
	
	/**
	 * Return service object
	 * 
	 * @param objectName
	 * @return
	 */
	public static Object getServiceObject(String objectName){
		if(DOC_HOLDER_SERVICE.equals(objectName)){
			return ApplicationContextUtils.getBean("documentHolderService");
		}else if(DOC_CONTENT_SERVICE.equals(objectName)){
			return ApplicationContextUtils.getBean("documentContentService");
		}else if(STATIC_CODE_DECODE_SERVICE.equals(objectName)){
			return ApplicationContextUtils.getBean("staticCodeDecodeService");
		}else if(APP_USER_SERVICE.equals(objectName)){
			return ApplicationContextUtils.getBean("appUserService");
		}else if(APP_USER_LOGIN_SERVICE.equals(objectName)){
			return ApplicationContextUtils.getBean("appUserLoginService");
		}else if(APP_USER_FUNCTION_ACC_SERVICE.equals(objectName)){
			return ApplicationContextUtils.getBean("appUserFunctionAccessService");
		}else if(APP_USER_UI_SETTINGS_SERVICE.equals(objectName)){
			return ApplicationContextUtils.getBean("appUserUISettingsService");
		}else if(EMAIL_GATEWAY_SERVICE.equals(objectName)){
			return ApplicationContextUtils.getBean("emailGatewayService");
		}else{
			return null;
		}
	}
	
	
	

}
