package com.desktopapp.spring.util;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextUtils {
	
	public static ApplicationContext ctx;
	
	static{
		ctx = new ClassPathXmlApplicationContext(
                "classpath:applicationContext.xml"); //get Spring context 
	}
	
	public static Object getBean(String beanName){
		return ((BeanFactory)ctx).getBean(beanName);
	}

}
