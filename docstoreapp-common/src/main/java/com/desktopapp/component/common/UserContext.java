package com.desktopapp.component.common;

public class UserContext {
    
    private String userName;
    private String userAccessType;
    
    public UserContext() {
	super();
    }
    public UserContext(String userName, String userAccessType) {
	super();
	this.userName = userName;
	this.userAccessType = userAccessType;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserAccessType() {
        return userAccessType;
    }
    public void setUserAccessType(String userAccessType) {
        this.userAccessType = userAccessType;
    }
    

}
