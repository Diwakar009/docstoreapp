package com.desktopapp.component.common;

public class CodeDataItem {
	
	private String codeName;
	private String codeValue;
	private String codeDescription;
	
	public CodeDataItem(String codeName, String codeValue,
			String codeDescription) {
		super();
		this.codeName = codeName;
		this.codeValue = codeValue;
		this.codeDescription = codeDescription;
	}
	
	
	
	public CodeDataItem(String codeName, String codeValue) {
		super();
		this.codeName = codeName;
		this.codeValue = codeValue;
	}

	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	public String getCodeValue() {
		return codeValue;
	}
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}
	public String getCodeDescription() {
		return codeDescription;
	}
	public void setCodeDescription(String codeDescription) {
		this.codeDescription = codeDescription;
	}



	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codeDescription == null) ? 0 : codeDescription.hashCode());
		result = prime * result
				+ ((codeName == null) ? 0 : codeName.hashCode());
		result = prime * result
				+ ((codeValue == null) ? 0 : codeValue.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodeDataItem other = (CodeDataItem) obj;
		if (codeDescription == null) {
			if (other.codeDescription != null)
				return false;
		} else if (!codeDescription.equals(other.codeDescription))
			return false;
		if (codeName == null) {
			if (other.codeName != null)
				return false;
		} else if (!codeName.equals(other.codeName))
			return false;
		if (codeValue == null) {
			if (other.codeValue != null)
				return false;
		} else if (!codeValue.equals(other.codeValue))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return codeDescription;
	}

}
