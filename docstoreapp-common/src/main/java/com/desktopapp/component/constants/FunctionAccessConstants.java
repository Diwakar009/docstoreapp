package com.desktopapp.component.constants;

public class FunctionAccessConstants {

    public static final String FN_CODEDECODE = "FN_CODEDECODE";
    public static final String FN_APPUSER = "FN_APPUSER";
    public static final String FN_DOCHOLDER = "FN_DOCHOLDER";
    public static final String FN_DOCCONTENT = "FN_DOCCONTENT";
    public static final String FN_STATICCODEDECODE = "FN_STATICCODEDECODE";
    public static final String FN_APPUSER_LOGIN = "FN_APPUSER_LOGIN";
    public static final String FN_SENDEMAIL= "FN_SENDEMAIL";
    
    
    public static final String ADD = "_ADD";
    public static final String EDIT = "_EDIT";
    public static final String DELETE = "_DELETE";
    public static final String REFRESH = "_REFRESH";
    public static final String SAVE = "_SAVE";
    
    public static final String BACKUP = "_BACKUP";
    public static final String RESTORE = "_RESTORE";
    public static final String DOWNLOAD = "_DOWNLOAD";
    public static final String UPLOAD = "_UPLOAD";
    
    public static final String EMAIL = "_EMAIL";
    public static final String PRINT = "_PRINT";
    
    
    
    
}
